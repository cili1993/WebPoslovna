namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class STAVKA_PROMETNOG_DOKUMENTA
    {
        [Key]
        public int ID_STAV__PROM__DOK_ { get; set; }

        public int ID_PROM__DOK_ { get; set; }

        public int ID_ARTIKAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal REDNI_BROJ_STAV__PROM__DOK { get; set; }

        public int KOLICINA_STAV__PROM__DOK { get; set; }

        public double CENA_STAVKA_PROM__DOK_ { get; set; }

        public double? VREDNOST_STAV__PROM__DOK { get; set; }

        public virtual PROMETNI_DOKUMENT PROMETNI_DOKUMENT { get; set; }

        public virtual SIFRARNIK_ARTIKALA SIFRARNIK_ARTIKALA { get; set; }
    }
}
