namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MAGACIN")]
    public partial class MAGACIN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MAGACIN()
        {
            MAGACINSKA_KARTICA = new HashSet<MAGACINSKA_KARTICA>();
            POPISNI_DOKUMENT = new HashSet<POPISNI_DOKUMENT>();
            PROMETNI_DOKUMENT = new HashSet<PROMETNI_DOKUMENT>();
            PROMETNI_DOKUMENT1 = new HashSet<PROMETNI_DOKUMENT>();
        }

        [Key]
        public int ID_MAGACINA { get; set; }

        public int ID_PREDUZECA { get; set; }

        public int ID_SEKTORA { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_MAGACINA { get; set; }

        public virtual SEKTOR SEKTOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAGACINSKA_KARTICA> MAGACINSKA_KARTICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POPISNI_DOKUMENT> POPISNI_DOKUMENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROMETNI_DOKUMENT> PROMETNI_DOKUMENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROMETNI_DOKUMENT> PROMETNI_DOKUMENT1 { get; set; }
    }
}
