namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JEDINICA_MERE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JEDINICA_MERE()
        {
            SIFRARNIK_ARTIKALA = new HashSet<SIFRARNIK_ARTIKALA>();
        }

        [Key]
        public int ID_JEDINICE { get; set; }

        [StringLength(10)]
        public string NAZIV_JEDINICE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SIFRARNIK_ARTIKALA> SIFRARNIK_ARTIKALA { get; set; }
    }
}
