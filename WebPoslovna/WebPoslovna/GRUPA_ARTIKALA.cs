namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GRUPA_ARTIKALA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GRUPA_ARTIKALA()
        {
            SIFRARNIK_ARTIKALA = new HashSet<SIFRARNIK_ARTIKALA>();
        }

        [Key]
        public int ID_GRUPE { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_GRUPE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SIFRARNIK_ARTIKALA> SIFRARNIK_ARTIKALA { get; set; }
    }
}
