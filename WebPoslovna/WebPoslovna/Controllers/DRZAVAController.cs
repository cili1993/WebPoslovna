﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class DRZAVAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/DRZAVA
        public IQueryable<DRZAVA> GetDRZAVA()
        {
            return db.DRZAVA;
        }


        // PUT: api/DRZAVA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDRZAVA(int id, DRZAVA dRZAVA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dRZAVA.ID_DRZAVA)
            {
                return BadRequest();
            }

            DRZAVA dr = db.DRZAVA.SingleOrDefault(m => m.OZNAKA_DRZAVA.Equals(dRZAVA.OZNAKA_DRZAVA));

            if (dr != null)
            {
                return Conflict();
            }

            db.Entry(dRZAVA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DRZAVAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DRZAVA
        [ResponseType(typeof(DRZAVA))]
        public IHttpActionResult PostDRZAVA(DRZAVA dRZAVA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DRZAVA dr = db.DRZAVA.SingleOrDefault(m => m.OZNAKA_DRZAVA.Equals(dRZAVA.OZNAKA_DRZAVA));

            if (dr != null)
            {
                return Conflict();
            }

            db.DRZAVA.Add(dRZAVA);
            db.SaveChanges();
            
            return CreatedAtRoute("DefaultApi", new { id = dRZAVA.ID_DRZAVA }, dRZAVA);
        }

        // DELETE: api/DRZAVA/5
        [ResponseType(typeof(DRZAVA))]
        public IHttpActionResult DeleteDRZAVA(int id)
        {
            DRZAVA dRZAVA = db.DRZAVA.SingleOrDefault(m => m.ID_DRZAVA == id);
            if (dRZAVA == null)
            {
                return NotFound();
            }

            db.DRZAVA.Remove(dRZAVA);
            db.SaveChanges();

            return Ok(dRZAVA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DRZAVAExists(int id)
        {
            return db.DRZAVA.Count(e => e.ID_DRZAVA == id) > 0;
        }

        // GET: api/DRZAVANext/1
        [Route("api/DRZAVANext/{id}")]
        [HttpGet]
        [ResponseType(typeof(MESTO))]
        public HttpResponseMessage GetDrzavaNext(int id)
        {

            IQueryable<MESTO> promdom = db.MESTO;
            var list = promdom.Where(p => p.ID_DRZAVA == id)
                .Select(p => new { p.OZNAKA_MESTA, p.NAZIV_MESTA, p.POST_BR });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

       
        // GET: api/DRZAVA/5
        [ResponseType(typeof(DRZAVA))]
        public IHttpActionResult GetDRZAVA(int id)
        {
            DRZAVA dRZAVA = db.DRZAVA.Find(id);
            if (dRZAVA == null)
            {
                return NotFound();
            }

            return Ok(dRZAVA);
        }
    }
}