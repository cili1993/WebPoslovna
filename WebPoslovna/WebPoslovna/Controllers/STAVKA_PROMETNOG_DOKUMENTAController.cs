﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class STAVKA_PROMETNOG_DOKUMENTAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/STAVKA_PROMETNOG_DOKUMENTA
        public IQueryable<STAVKA_PROMETNOG_DOKUMENTA> GetSTAVKA_PROMETNOG_DOKUMENTA()
        {
            return db.STAVKA_PROMETNOG_DOKUMENTA;
        }

        // GET: api/STAVKA_PROMETNOG_DOKUMENTA/5
        [ResponseType(typeof(STAVKA_PROMETNOG_DOKUMENTA))]
        public IHttpActionResult GetSTAVKA_PROMETNOG_DOKUMENTA(int id)
        {
            STAVKA_PROMETNOG_DOKUMENTA sTAVKA_PROMETNOG_DOKUMENTA = db.STAVKA_PROMETNOG_DOKUMENTA.Find(id);
            if (sTAVKA_PROMETNOG_DOKUMENTA == null)
            {
                return NotFound();
            }

            return Ok(sTAVKA_PROMETNOG_DOKUMENTA);
        }

        // PUT: api/STAVKA_PROMETNOG_DOKUMENTA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSTAVKA_PROMETNOG_DOKUMENTA(int id, STAVKA_PROMETNOG_DOKUMENTA sTAVKA_PROMETNOG_DOKUMENTA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sTAVKA_PROMETNOG_DOKUMENTA.ID_STAV__PROM__DOK_)
            {
                return BadRequest();
            }

            db.Entry(sTAVKA_PROMETNOG_DOKUMENTA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!STAVKA_PROMETNOG_DOKUMENTAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/STAVKA_PROMETNOG_DOKUMENTA
        [ResponseType(typeof(STAVKA_PROMETNOG_DOKUMENTA))]
        public IHttpActionResult PostSTAVKA_PROMETNOG_DOKUMENTA(STAVKA_PROMETNOG_DOKUMENTA sTAVKA_PROMETNOG_DOKUMENTA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.STAVKA_PROMETNOG_DOKUMENTA.Add(sTAVKA_PROMETNOG_DOKUMENTA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (STAVKA_PROMETNOG_DOKUMENTAExists(sTAVKA_PROMETNOG_DOKUMENTA.ID_STAV__PROM__DOK_))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sTAVKA_PROMETNOG_DOKUMENTA.ID_STAV__PROM__DOK_ }, sTAVKA_PROMETNOG_DOKUMENTA);
        }

        // DELETE: api/STAVKA_PROMETNOG_DOKUMENTA/5
        [ResponseType(typeof(STAVKA_PROMETNOG_DOKUMENTA))]
        public IHttpActionResult DeleteSTAVKA_PROMETNOG_DOKUMENTA(int id)
        {
            STAVKA_PROMETNOG_DOKUMENTA sTAVKA_PROMETNOG_DOKUMENTA = db.STAVKA_PROMETNOG_DOKUMENTA.SingleOrDefault(m => m.ID_STAV__PROM__DOK_ == id);
            if (sTAVKA_PROMETNOG_DOKUMENTA == null)
            {
                return NotFound();
            }

            db.STAVKA_PROMETNOG_DOKUMENTA.Remove(sTAVKA_PROMETNOG_DOKUMENTA);
            db.SaveChanges();

            return Ok(sTAVKA_PROMETNOG_DOKUMENTA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool STAVKA_PROMETNOG_DOKUMENTAExists(int id)
        {
            return db.STAVKA_PROMETNOG_DOKUMENTA.Count(e => e.ID_STAV__PROM__DOK_ == id) > 0;
        }
    }
}