﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace WebPoslovna.Controllers
{
    public class BussinesActionController : ApiController
    {

        private Model1 db = new Model1();

        [Route("api/pdf/LagerLista/{idGodine}/{idMagacina}")]
        [HttpPost]
        public HttpResponseMessage PdfLagerLista(int idGodine,int idMagacina)
        {
            var poslovnaGodina = db.POSLOVNA_GODINA.Find(idGodine);
            var magacin = db.MAGACIN.Find(idMagacina);
            var preduzece = db.PREDUZECE.Find(magacin.ID_PREDUZECA);

            PdfDocument document = new PdfDocument();
            //document.Info.Title = "Created with PDFsharp";
            
            // Create an empty page
            PdfPage page = document.AddPage();
            //page.Orientation = PdfSharp.PageOrientation.Landscape;

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Create a font
            XFont font = new XFont("Verdana", 20, XFontStyle.Bold);

            // Draw the text 
            gfx.DrawString("Lager lista", font, XBrushes.Black,
              new XRect(10, 40, page.Width, page.Height / 8),
              XStringFormats.TopCenter);

            font = new XFont("Verdana", 16, XFontStyle.Bold);

            gfx.DrawString("Poslovna godina: " + poslovnaGodina.GODINA, font, XBrushes.Black,
              new XRect(10, 60, page.Width, page.Height / 8),
              XStringFormats.TopLeft);

            font = new XFont("Verdana", 14, XFontStyle.Bold);

            gfx.DrawString("Preduzece: " + preduzece.NAZIV_PREDUZECA, font, XBrushes.Black,
             new XRect(10, 100, page.Width, page.Height),
             XStringFormats.TopLeft);

            gfx.DrawString("Magacin: " + magacin.NAZIV_MAGACINA, font, XBrushes.Black,
             new XRect(300, 100, page.Width, page.Height),
             XStringFormats.TopLeft);

            font = new XFont("Verdana", 11, XFontStyle.Bold);

            gfx.DrawString("Sifra", font, XBrushes.Black,
              new XRect(0, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("Roba", font, XBrushes.Black,
              new XRect(60, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("M.j.", font, XBrushes.Black,
              new XRect(160, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("Kolicina", font, XBrushes.Black,
                       new XRect(200, 120, page.Width, page.Height),
                       XStringFormats.TopLeft);

            gfx.DrawString("Vrednost", font, XBrushes.Black,
                  new XRect(310, 120, page.Width, page.Height),
                  XStringFormats.TopLeft);

            /*     gfx.DrawString("Ulazna kolicina", font, XBrushes.Black,
                  new XRect(200, 120, page.Width, page.Height),
                  XStringFormats.TopLeft);

                 gfx.DrawString("Ulzna vrednost", font, XBrushes.Black,
                   new XRect(310, 120, page.Width, page.Height),
                   XStringFormats.TopLeft);

                 gfx.DrawString("Izlazna kolicina", font, XBrushes.Black,
                   new XRect(420, 120, page.Width, page.Height),
                   XStringFormats.TopLeft);

                 gfx.DrawString("Izlazna vrednsot", font, XBrushes.Black,
                   new XRect(530, 120, page.Width, page.Height),
                   XStringFormats.TopLeft);

                 gfx.DrawString("Trenutno stanje", font, XBrushes.Black,
                   new XRect(640, 120, page.Width, page.Height),
                   XStringFormats.TopLeft);
     */
            font = new XFont("Verdana", 10, XFontStyle.Regular);
            int razmak = 20;
            var kartice = (from p in db.MAGACINSKA_KARTICA where p.ID_GODINE == poslovnaGodina.ID_GODINE && p.ID_MAGACINA == magacin.ID_MAGACINA select p);


            foreach (SIFRARNIK_ARTIKALA artikl in db.SIFRARNIK_ARTIKALA)
            {
                var magacinskeKartice = (from p in kartice where p.ID_ARTIKAL == artikl.ID_ARTIKAL && p.ID_MAGACINA == magacin.ID_MAGACINA select p);
                var kolicina = 0;
                decimal vrednost = 0;

                foreach (MAGACINSKA_KARTICA pd in magacinskeKartice)
                {
                    kolicina += (int)pd.UK__KOL__MAG__KAR_;
                    vrednost += (decimal)pd.UK_VR__MAG__KAR;
                    
                }

                if (magacinskeKartice.Count() > 0)
                {
                    var jm = db.JEDINICA_MERE.Find(artikl.ID_JEDINICE);


                    gfx.DrawString(artikl.ID_ARTIKAL.ToString(), font, XBrushes.Black,
                      new XRect(10, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(artikl.NAZIV_ARTIKAL, font, XBrushes.Black,
                      new XRect(60, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(jm.NAZIV_JEDINICE, font, XBrushes.Black,
                      new XRect(160, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(kolicina.ToString(), font, XBrushes.Black,
                         new XRect(200, 120 + razmak, page.Width, page.Height),
                         XStringFormats.TopLeft);

                    gfx.DrawString(vrednost.ToString(), font, XBrushes.Black,
                          new XRect(310, 120 + razmak, page.Width, page.Height),
                          XStringFormats.TopLeft);

                    /*    gfx.DrawString(pd.KOLICINA__ULAZA.ToString(), font, XBrushes.Black,
                         new XRect(200, 120 + razmak, page.Width, page.Height),
                         XStringFormats.TopLeft);

                        gfx.DrawString(pd.VREDNOST_ULAZA.ToString(), font, XBrushes.Black,
                          new XRect(310, 120 + razmak, page.Width, page.Height),
                          XStringFormats.TopLeft);

                        gfx.DrawString(pd.KOLICINA_IZLAZA.ToString(), font, XBrushes.Black,
                          new XRect(420, 120 + razmak, page.Width, page.Height),
                          XStringFormats.TopLeft);

                        gfx.DrawString(pd.VREDNOST_IZLAZA.ToString(), font, XBrushes.Black,
                          new XRect(530, 120 + razmak, page.Width, page.Height),
                          XStringFormats.TopLeft);

                        gfx.DrawString((pd.KOLICINA__ULAZA - pd.KOLICINA_IZLAZA).ToString(), font, XBrushes.Black,
                          new XRect(640, 120 + razmak, page.Width, page.Height),
                          XStringFormats.TopLeft);

        */          razmak += 15;
                }
                    
            }

            // Save the document...

            MemoryStream stream = new MemoryStream();
            document.Save(stream, false);

            byte[] bytes = stream.ToArray();
            var res = new HttpResponseMessage();
            res.Content = new ByteArrayContent(bytes);
            res.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            stream.Close();
            return res;

        }


        [Route("api/pdf/Analitika/{idMagacina}")]
        [HttpPost]
        public HttpResponseMessage PdfAnalitickaKartica(int idMagacina)
        {
            //var analitika = db.MAGACINSKA_KARTICA.Find(idKartice);
            var magacin = db.MAGACIN.Find(idMagacina);
            var preduzece = db.PREDUZECE.Find(magacin.ID_PREDUZECA);

            PdfDocument document = new PdfDocument();
            //document.Info.Title = "Created with PDFsharp";

            // Create an empty page
            PdfPage page = document.AddPage();
            //page.Orientation = PdfSharp.PageOrientation.Landscape;

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Create a font
            XFont font = new XFont("Verdana", 20, XFontStyle.Bold);

            // Draw the text 
            gfx.DrawString("Analitika magacinske kartice", font, XBrushes.Black,
              new XRect(10, 40, page.Width, page.Height / 8),
              XStringFormats.TopCenter);


            font = new XFont("Verdana", 14, XFontStyle.Bold);

            gfx.DrawString("Preduzece: " + preduzece.NAZIV_PREDUZECA, font, XBrushes.Black,
             new XRect(10, 100, page.Width, page.Height),
             XStringFormats.TopLeft);

            gfx.DrawString("Magacin: " + magacin.NAZIV_MAGACINA, font, XBrushes.Black,
             new XRect(300, 100, page.Width, page.Height),
             XStringFormats.TopLeft);

            font = new XFont("Verdana", 11, XFontStyle.Bold);

            gfx.DrawString("Datum", font, XBrushes.Black,
              new XRect(10, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("Vrsta dok.", font, XBrushes.Black,
              new XRect(100, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("Smer", font, XBrushes.Black,
              new XRect(180, 120, page.Width, page.Height),
              XStringFormats.TopLeft);

            gfx.DrawString("Pocetno stanje", font, XBrushes.Black,
                new XRect(230, 120, page.Width, page.Height),
                XStringFormats.TopLeft);

            gfx.DrawString("Ulaz", font, XBrushes.Black,
                  new XRect(350, 120, page.Width, page.Height),
                  XStringFormats.TopLeft);

            gfx.DrawString("Izlaz", font, XBrushes.Black,
                new XRect(420, 120, page.Width, page.Height),
                XStringFormats.TopLeft);

           gfx.DrawString("Stanje", font, XBrushes.Black,
                new XRect(490, 120, page.Width, page.Height),
                XStringFormats.TopLeft);
            

            font = new XFont("Verdana", 10, XFontStyle.Regular);
            int razmak = 20;


            foreach (ANALITIKA_MAGACINSKE_KARTICE anal in db.ANALITIKA_MAGACINSKE_KARTICE)
            {
                var magKartica = db.MAGACINSKA_KARTICA.Find(anal.ID_MAG__KART_);

                    gfx.DrawString(anal.DATUM_PROMENE, font, XBrushes.Black,
                      new XRect(10, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(anal.VRSTA_PROMETA, font, XBrushes.Black,
                      new XRect(100, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(anal.SMER, font, XBrushes.Black,
                      new XRect(180, 120 + razmak, page.Width, page.Height),
                      XStringFormats.TopLeft);

                    gfx.DrawString(magKartica.POCETNO_STANJE_VREDNOST_MAG__KAR.ToString(), font, XBrushes.Black,
                         new XRect(230, 120 + razmak, page.Width, page.Height),
                         XStringFormats.TopLeft);

                    gfx.DrawString(magKartica.VREDNOST_ULAZA.ToString(), font, XBrushes.Black,
                        new XRect(350, 120 + razmak, page.Width, page.Height),
                        XStringFormats.TopLeft);

                    gfx.DrawString(magKartica.VREDNOST_IZLAZA.ToString(), font, XBrushes.Black,
                       new XRect(420, 120 + razmak, page.Width, page.Height),
                       XStringFormats.TopLeft);

                    gfx.DrawString(magKartica.UK_VR__MAG__KAR.ToString(), font, XBrushes.Black,
                       new XRect(490, 120 + razmak, page.Width, page.Height),
                       XStringFormats.TopLeft);

                razmak += 15;
                
            }

            // Save the document...

            MemoryStream stream = new MemoryStream();
            document.Save(stream, false);

            byte[] bytes = stream.ToArray();
            var res = new HttpResponseMessage();
            res.Content = new ByteArrayContent(bytes);
            res.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            stream.Close();
            return res;

        }

        [Route("api/knjizenje/{idprometniDokument}")]
        [HttpPost]
        public void knjizenje(int idprometniDokument)
        {
            PROMETNI_DOKUMENT prometniDokument = db.PROMETNI_DOKUMENT.Find(idprometniDokument);
            var stavkePrometnogDokumenta = (from p in db.STAVKA_PROMETNOG_DOKUMENTA where p.ID_PROM__DOK_ == prometniDokument.ID_PROM__DOK_ select p);

            MAGACINSKA_KARTICA novaMagacinskaKartica;
            ANALITIKA_MAGACINSKE_KARTICE novaAnalitika;
            IList<MAGACINSKA_KARTICA> listaNovihKartica = new List<MAGACINSKA_KARTICA>();
            IList<ANALITIKA_MAGACINSKE_KARTICE> listaNovihAnalitika = new List<ANALITIKA_MAGACINSKE_KARTICE>();

            foreach (STAVKA_PROMETNOG_DOKUMENTA stavka in stavkePrometnogDokumenta)
            {
                novaMagacinskaKartica = new MAGACINSKA_KARTICA();
               
                novaMagacinskaKartica.ID_GODINE = prometniDokument.ID_GODINE;
                novaMagacinskaKartica.ID_MAGACINA = prometniDokument.ID_MAGACINA;
                novaMagacinskaKartica.ID_ARTIKAL = stavka.ID_ARTIKAL;
                novaMagacinskaKartica.PROSECNA_CENA = (decimal)stavka.CENA_STAVKA_PROM__DOK_;

                if (prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("PR") || prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("MM"))
                {
                    novaMagacinskaKartica.KOLICINA__ULAZA = stavka.KOLICINA_STAV__PROM__DOK;
                    novaMagacinskaKartica.VREDNOST_ULAZA = (decimal)stavka.VREDNOST_STAV__PROM__DOK;
                    novaMagacinskaKartica.KOLICINA_IZLAZA = 0;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = 0;
                }
                else
                {
                    novaMagacinskaKartica.KOLICINA_IZLAZA = stavka.KOLICINA_STAV__PROM__DOK;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = (decimal)stavka.VREDNOST_STAV__PROM__DOK;
                    novaMagacinskaKartica.KOLICINA__ULAZA = 0;
                    novaMagacinskaKartica.VREDNOST_ULAZA = 0;
                }

                novaMagacinskaKartica.UK__KOL__MAG__KAR_ = novaMagacinskaKartica.KOLICINA__ULAZA - novaMagacinskaKartica.KOLICINA_IZLAZA;
                novaMagacinskaKartica.UK_VR__MAG__KAR = novaMagacinskaKartica.VREDNOST_ULAZA - novaMagacinskaKartica.VREDNOST_IZLAZA;

                novaMagacinskaKartica.POCETNO_STANJE_KOLICINA_MAG__KAR_ = 0;
                novaMagacinskaKartica.POCETNO_STANJE_VREDNOST_MAG__KAR = 0;

                listaNovihKartica.Add(novaMagacinskaKartica);

            }


            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                db.MAGACINSKA_KARTICA.Add(mg);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (MAGACINSKA_KARTICAExists(mg.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                novaAnalitika = new ANALITIKA_MAGACINSKE_KARTICE();
                //Nova analiticka kartica na osnovu magacinskee
                var pomocnaAnalitika = db.ANALITIKA_MAGACINSKE_KARTICE.OrderByDescending(s => s.REDNI_BROJ_MAG__ANAL_).First();
                var redniBroj = pomocnaAnalitika.REDNI_BROJ_MAG__ANAL_++;

                novaAnalitika.ID_MAG__KART_ = mg.ID_MAG__KART_;
                novaAnalitika.REDNI_BROJ_MAG__ANAL_ = redniBroj;
                novaAnalitika.VRSTA_PROMETA = prometniDokument.VRSTA_PROMETNOG_DOKUMENTA;
                novaAnalitika.SIFRA_DOKUMENTA = prometniDokument.ID_PROM__DOK_;
                novaAnalitika.CENA_ANAL__MAG__KART_ = (double)mg.PROSECNA_CENA;
                novaAnalitika.DATUM_PROMENE = prometniDokument.DATUM_KNJIZENJA;

                if (prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("PR") || prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("MM"))
                {
                    novaAnalitika.SMER = "U";
                }
                else
                {
                    novaAnalitika.SMER = "I";
                }

                listaNovihAnalitika.Add(novaAnalitika);
            }


           
        foreach(ANALITIKA_MAGACINSKE_KARTICE an in listaNovihAnalitika)
            {
                db.ANALITIKA_MAGACINSKE_KARTICE.Add(an);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (ANALITIKA_MAGACINSKE_KARTICEExists(an.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            
        }

        private bool MAGACINSKA_KARTICAExists(int id)
        {
            return db.MAGACINSKA_KARTICA.Count(e => e.ID_MAG__KART_ == id) > 0;
        }

        private bool ANALITIKA_MAGACINSKE_KARTICEExists(int id)
        {
            return db.ANALITIKA_MAGACINSKE_KARTICE.Count(e => e.ID_MAG__KART_ == id) > 0;
        }

        [Route("api/storniranje/{idprometniDokument}")]
        [HttpPost]
        public void storniranje(int idprometniDokument)
        {
            PROMETNI_DOKUMENT prometniDokument = db.PROMETNI_DOKUMENT.Find(idprometniDokument);
            var stavkePrometnogDokumenta = (from p in db.STAVKA_PROMETNOG_DOKUMENTA where p.ID_PROM__DOK_ == prometniDokument.ID_PROM__DOK_ select p);

            MAGACINSKA_KARTICA novaMagacinskaKartica;
            ANALITIKA_MAGACINSKE_KARTICE novaAnalitika;
            IList<MAGACINSKA_KARTICA> listaNovihKartica = new List<MAGACINSKA_KARTICA>();
            IList<ANALITIKA_MAGACINSKE_KARTICE> listaNovihAnalitika = new List<ANALITIKA_MAGACINSKE_KARTICE>();


            foreach (STAVKA_PROMETNOG_DOKUMENTA stavka in stavkePrometnogDokumenta)
            {
                novaMagacinskaKartica = new MAGACINSKA_KARTICA();

                novaMagacinskaKartica.ID_GODINE = prometniDokument.ID_GODINE;
                novaMagacinskaKartica.ID_MAGACINA = prometniDokument.ID_MAGACINA;
                novaMagacinskaKartica.ID_ARTIKAL = stavka.ID_ARTIKAL;
                novaMagacinskaKartica.PROSECNA_CENA = (decimal)stavka.CENA_STAVKA_PROM__DOK_;

                if (prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("PR") || prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("MM"))
                {
                    novaMagacinskaKartica.KOLICINA_IZLAZA = stavka.KOLICINA_STAV__PROM__DOK;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = (decimal)stavka.VREDNOST_STAV__PROM__DOK;
                    novaMagacinskaKartica.KOLICINA__ULAZA = 0;
                    novaMagacinskaKartica.VREDNOST_ULAZA = 0;
                }
                else
                {
                    novaMagacinskaKartica.KOLICINA__ULAZA = stavka.KOLICINA_STAV__PROM__DOK;
                    novaMagacinskaKartica.VREDNOST_ULAZA = (decimal)stavka.VREDNOST_STAV__PROM__DOK;
                    novaMagacinskaKartica.KOLICINA_IZLAZA = 0;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = 0;
                }

                novaMagacinskaKartica.UK__KOL__MAG__KAR_ = novaMagacinskaKartica.KOLICINA__ULAZA - novaMagacinskaKartica.KOLICINA_IZLAZA;
                novaMagacinskaKartica.UK_VR__MAG__KAR = novaMagacinskaKartica.VREDNOST_ULAZA - novaMagacinskaKartica.VREDNOST_IZLAZA;

                novaMagacinskaKartica.POCETNO_STANJE_KOLICINA_MAG__KAR_ = 0;
                novaMagacinskaKartica.POCETNO_STANJE_VREDNOST_MAG__KAR = 0;

                listaNovihKartica.Add(novaMagacinskaKartica);

            }


            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                db.MAGACINSKA_KARTICA.Add(mg);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (MAGACINSKA_KARTICAExists(mg.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                novaAnalitika = new ANALITIKA_MAGACINSKE_KARTICE();
                //Nova analiticka kartica na osnovu magacinskee
                var pomocnaAnalitika = db.ANALITIKA_MAGACINSKE_KARTICE.OrderByDescending(s => s.REDNI_BROJ_MAG__ANAL_).First();
                var redniBroj = pomocnaAnalitika.REDNI_BROJ_MAG__ANAL_++;

                novaAnalitika.ID_MAG__KART_ = mg.ID_MAG__KART_;
                novaAnalitika.REDNI_BROJ_MAG__ANAL_ = redniBroj;
                novaAnalitika.VRSTA_PROMETA = prometniDokument.VRSTA_PROMETNOG_DOKUMENTA;
                novaAnalitika.SIFRA_DOKUMENTA = prometniDokument.ID_PROM__DOK_;
                novaAnalitika.CENA_ANAL__MAG__KART_ = (double)mg.PROSECNA_CENA;
                novaAnalitika.DATUM_PROMENE = prometniDokument.DATUM_KNJIZENJA;

                if (prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("PR") || prometniDokument.VRSTA_PROMETNOG_DOKUMENTA.Equals("MM"))
                {
                    novaAnalitika.SMER = "U";
                }
                else
                {
                    novaAnalitika.SMER = "I";
                }

                listaNovihAnalitika.Add(novaAnalitika);
            }



            foreach (ANALITIKA_MAGACINSKE_KARTICE an in listaNovihAnalitika)
            {
                db.ANALITIKA_MAGACINSKE_KARTICE.Add(an);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (ANALITIKA_MAGACINSKE_KARTICEExists(an.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        [Route("api/knjizenjePopisa/{idpopisniDokument}")]
        [HttpPost]
        public void knjizenjePopisa(int idpopisniDokument)
        {
            POPISNI_DOKUMENT popisniDok = db.POPISNI_DOKUMENT.Find(idpopisniDokument);
            var stavkePopisnogDokumenta = (from p in db.STAVKE_POPISA where p.ID_POP__DOK__ == popisniDok.ID_POP__DOK__ select p);

            MAGACINSKA_KARTICA novaMagacinskaKartica;
            ANALITIKA_MAGACINSKE_KARTICE novaAnalitika;
            IList<MAGACINSKA_KARTICA> listaNovihKartica = new List<MAGACINSKA_KARTICA>();
            IList<ANALITIKA_MAGACINSKE_KARTICE> listaNovihAnalitika = new List<ANALITIKA_MAGACINSKE_KARTICE>();


            foreach (STAVKE_POPISA stavka in stavkePopisnogDokumenta)
            {
                novaMagacinskaKartica = new MAGACINSKA_KARTICA();

                novaMagacinskaKartica.ID_GODINE = popisniDok.ID_GODINE;
                novaMagacinskaKartica.ID_MAGACINA = popisniDok.ID_MAGACINA;
                novaMagacinskaKartica.ID_ARTIKAL = stavka.ID_ARTIKAL;
                novaMagacinskaKartica.PROSECNA_CENA = 0;

                var vrednostNakonPopisa = stavka.KOLICINA_PO_POPISU - stavka.KOLICINA_U_KARTICI;

                if (vrednostNakonPopisa > 0)
                {
                    novaMagacinskaKartica.KOLICINA_IZLAZA = 0;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = 0;
                    novaMagacinskaKartica.KOLICINA__ULAZA = (int)vrednostNakonPopisa;
                    novaMagacinskaKartica.VREDNOST_ULAZA = 0;
                }
                else
                {
                    novaMagacinskaKartica.KOLICINA__ULAZA = 0;
                    novaMagacinskaKartica.VREDNOST_ULAZA = 0;
                    novaMagacinskaKartica.KOLICINA_IZLAZA = (int)vrednostNakonPopisa;
                    novaMagacinskaKartica.VREDNOST_IZLAZA = 0;
                }

                novaMagacinskaKartica.UK__KOL__MAG__KAR_ = novaMagacinskaKartica.KOLICINA__ULAZA - novaMagacinskaKartica.KOLICINA_IZLAZA;
                novaMagacinskaKartica.UK_VR__MAG__KAR = novaMagacinskaKartica.VREDNOST_ULAZA - novaMagacinskaKartica.VREDNOST_IZLAZA;

                novaMagacinskaKartica.POCETNO_STANJE_KOLICINA_MAG__KAR_ = 0;
                novaMagacinskaKartica.POCETNO_STANJE_VREDNOST_MAG__KAR = 0;

                listaNovihKartica.Add(novaMagacinskaKartica);

            }

            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                db.MAGACINSKA_KARTICA.Add(mg);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (MAGACINSKA_KARTICAExists(mg.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            foreach (MAGACINSKA_KARTICA mg in listaNovihKartica)
            {
                novaAnalitika = new ANALITIKA_MAGACINSKE_KARTICE();
                //Nova analiticka kartica na osnovu magacinskee
                var pomocnaAnalitika = db.ANALITIKA_MAGACINSKE_KARTICE.OrderByDescending(s => s.REDNI_BROJ_MAG__ANAL_).First();
                var redniBroj = pomocnaAnalitika.REDNI_BROJ_MAG__ANAL_++;

                novaAnalitika.ID_MAG__KART_ = mg.ID_MAG__KART_;
                novaAnalitika.REDNI_BROJ_MAG__ANAL_ = redniBroj;
                novaAnalitika.VRSTA_PROMETA = "KO";
                novaAnalitika.SIFRA_DOKUMENTA = popisniDok.ID_POP__DOK__;
                novaAnalitika.CENA_ANAL__MAG__KART_ = (double)mg.PROSECNA_CENA;
                novaAnalitika.DATUM_PROMENE = popisniDok.DATUM_POPISA;


                if (mg.KOLICINA__ULAZA > 0)
                {
                    novaAnalitika.SMER = "U";
                }
                else
                {
                    novaAnalitika.SMER = "I";
                }

                listaNovihAnalitika.Add(novaAnalitika);
            }



            foreach (ANALITIKA_MAGACINSKE_KARTICE an in listaNovihAnalitika)
            {
                db.ANALITIKA_MAGACINSKE_KARTICE.Add(an);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (ANALITIKA_MAGACINSKE_KARTICEExists(an.ID_MAG__KART_))
                    {
                        System.Console.Out.Write("Error");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
       }
   }
}
