﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class STAVKE_POPISAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/STAVKE_POPISA
        public IQueryable<STAVKE_POPISA> GetSTAVKE_POPISA()
        {
            return db.STAVKE_POPISA;
        }

        // GET: api/STAVKE_POPISA/5
        [ResponseType(typeof(STAVKE_POPISA))]
        public IHttpActionResult GetSTAVKE_POPISA(int id)
        {
            STAVKE_POPISA sTAVKE_POPISA = db.STAVKE_POPISA.Find(id);
            if (sTAVKE_POPISA == null)
            {
                return NotFound();
            }

            return Ok(sTAVKE_POPISA);
        }

        // PUT: api/STAVKE_POPISA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSTAVKE_POPISA(int id, STAVKE_POPISA sTAVKE_POPISA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sTAVKE_POPISA.ID_PREDUZECA)
            {
                return BadRequest();
            }

            db.Entry(sTAVKE_POPISA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!STAVKE_POPISAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/STAVKE_POPISA
        [ResponseType(typeof(STAVKE_POPISA))]
        public IHttpActionResult PostSTAVKE_POPISA(STAVKE_POPISA sTAVKE_POPISA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.STAVKE_POPISA.Add(sTAVKE_POPISA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (STAVKE_POPISAExists(sTAVKE_POPISA.ID_PREDUZECA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sTAVKE_POPISA.ID_PREDUZECA }, sTAVKE_POPISA);
        }

        // DELETE: api/STAVKE_POPISA/5
        [ResponseType(typeof(STAVKE_POPISA))]
        public IHttpActionResult DeleteSTAVKE_POPISA(int id)
        {
            STAVKE_POPISA sTAVKE_POPISA = db.STAVKE_POPISA.SingleOrDefault(m => m.ID_STAVKE_POPISA == id);
            if (sTAVKE_POPISA == null)
            {
                return NotFound();
            }

            db.STAVKE_POPISA.Remove(sTAVKE_POPISA);
            db.SaveChanges();

            return Ok(sTAVKE_POPISA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool STAVKE_POPISAExists(int id)
        {
            return db.STAVKE_POPISA.Count(e => e.ID_PREDUZECA == id) > 0;
        }
    }
}