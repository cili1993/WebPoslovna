﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebPoslovna.Controllers
{
    public class ExampleController : ApiController
    {
        private Model1 db = new Model1();
        private System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=PoslovnaInformatikaDB;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework");

        // GET: api/Example
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Example/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Example
        [Route("api/login/{user}/{pass}")]
        [HttpPost]
        public void Post(string user, string pass)
        {
            System.Console.WriteLine(user);
        }


        [Route("api/tables")]
        [HttpGet]
        public HttpResponseMessage getTables()
        {
            conn.Open();
            List<string> tables = new List<string>();
            DataTable dt = conn.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tables.Add(tablename);
            }
            conn.Close();

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(tables, new System.Net.Http.Formatting.JsonMediaTypeFormatter());


            return msg;
        }

        // PUT: api/Example/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Example/5
        public void Delete(int id)
        {
        }
    }
}
