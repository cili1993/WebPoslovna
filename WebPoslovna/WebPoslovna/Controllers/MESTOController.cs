﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class MESTOController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/MESTO
        public IQueryable<MESTO> GetMESTO()
        {
            return db.MESTO;
        }

        // GET: api/MESTO/5
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult GetMESTO(int id)
        {
            MESTO mESTO = db.MESTO.Find(id);
            if (mESTO == null)
            {
                return NotFound();
            }

            return Ok(mESTO);
        }

       
        // PUT: api/MESTO/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMESTO(int id, MESTO mESTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mESTO.ID_MESTO)
            {
                return BadRequest();
            }

            MESTO mst = db.MESTO.SingleOrDefault(m => m.OZNAKA_MESTA.Equals(mESTO.OZNAKA_MESTA));

            if (mst != null)
            {
                return Conflict();
            }

            db.Entry(mESTO).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MESTOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MESTO
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult PostMESTO(MESTO mESTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MESTO mst = db.MESTO.SingleOrDefault(m => m.OZNAKA_MESTA.Equals(mESTO.OZNAKA_MESTA));

            if(mst != null)
            {
                return Conflict();
            }

            db.MESTO.Add(mESTO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MESTOExists(mESTO.ID_MESTO))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mESTO.ID_MESTO }, mESTO);
        }

        // DELETE: api/MESTO/5
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult DeleteMESTO(int id)
        {
            MESTO mESTO = db.MESTO.SingleOrDefault(m => m.ID_MESTO == id);
            if (mESTO == null)
            {
                return NotFound();
            }

            db.MESTO.Remove(mESTO);
            db.SaveChanges();

            return Ok(mESTO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MESTOExists(int id)
        {
            return db.MESTO.Count(e => e.ID_MESTO == id) > 0;
        }

        // GET: api/MESTONext/POSLOVNI_PARTNER/1
        [Route("api/MESTONext/POSLOVNI_PARTNER/{id}")]
        [HttpGet]
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public HttpResponseMessage GetMestoNext_PoslovniPartner(int id)
        {

            IQueryable<POSLOVNI_PARTNER> promdom = db.POSLOVNI_PARTNER;
            var list = promdom.Where(p => p.ID_MESTO == id)
                .Select(p => new {
                    p.NAZIV_PARTNERA,
                    p.TIP_PARTNERA,
                    p.PIB_PARTNERA,
                    p.ADRESA_PARTNERA,
                    p.TELEFON_PARTNERA
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/MESTONext/PREDUZECE/1
        [Route("api/MESTONext/PREDUZECE/{id}")]
        [HttpGet]
        [ResponseType(typeof(PREDUZECE))]
        public HttpResponseMessage GetMestoNext_Preduzece(int id)
        {

            IQueryable<PREDUZECE> promdom = db.PREDUZECE;
            var list = promdom.Where(p => p.ID_MESTO == id)
                .Select(p => new {
                    p.NAZIV_PREDUZECA,
                    p.PIB_PREDUZECA,
                    p.ADRESA_PREDUZECA,
                    p.TELEFON_PREDUZECA
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }


        // GET: api/MESTOFrom/5
        [Route("api/MESTOFrom/{id}")]
        [HttpGet]
        [ResponseType(typeof(DRZAVA))]
        public IHttpActionResult GetMESTOFrom(int id)
        {
            MESTO mESTO = db.MESTO.Find(id);
            DRZAVA drz = db.DRZAVA.Find(mESTO.ID_DRZAVA);
            if (drz == null)
            {
                return NotFound();
            }

            return Ok(drz);
        }


    }
}