﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class POSLOVNA_GODINAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/POSLOVNA_GODINA
        public IQueryable<POSLOVNA_GODINA> GetPOSLOVNA_GODINA()
        {
            return db.POSLOVNA_GODINA;
        }

        // GET: api/POSLOVNA_GODINA/5
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult GetPOSLOVNA_GODINA(int id)
        {
            POSLOVNA_GODINA pOSLOVNA_GODINA = db.POSLOVNA_GODINA.Find(id);
            if (pOSLOVNA_GODINA == null)
            {
                return NotFound();
            }

            return Ok(pOSLOVNA_GODINA);
        }

        // PUT: api/POSLOVNA_GODINA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPOSLOVNA_GODINA(int id, POSLOVNA_GODINA pOSLOVNA_GODINA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOSLOVNA_GODINA.ID_GODINE)
            {
                return BadRequest();
            }

            db.Entry(pOSLOVNA_GODINA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POSLOVNA_GODINAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POSLOVNA_GODINA
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult PostPOSLOVNA_GODINA(POSLOVNA_GODINA pOSLOVNA_GODINA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.POSLOVNA_GODINA.Add(pOSLOVNA_GODINA);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pOSLOVNA_GODINA.ID_GODINE }, pOSLOVNA_GODINA);
        }

        // DELETE: api/POSLOVNA_GODINA/5
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult DeletePOSLOVNA_GODINA(int id)
        {
            POSLOVNA_GODINA pOSLOVNA_GODINA = db.POSLOVNA_GODINA.SingleOrDefault(m => m.ID_GODINE == id);
            if (pOSLOVNA_GODINA == null)
            {
                return NotFound();
            }

            db.POSLOVNA_GODINA.Remove(pOSLOVNA_GODINA);
            db.SaveChanges();

            return Ok(pOSLOVNA_GODINA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POSLOVNA_GODINAExists(int id)
        {
            return db.POSLOVNA_GODINA.Count(e => e.ID_GODINE == id) > 0;
        }

        // GET: api/POSLOVNA_GODINANext/MAGACINSKA_KARTICA/1
        [Route("api/POSLOVA_GODINANext/MAGACINSKA_KARTICA/{id}")]
        [HttpGet]
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public HttpResponseMessage GetPoslovnaGodinaNext_MagacinskaKartica(int id)
        {

            IQueryable<MAGACINSKA_KARTICA> sifart = db.MAGACINSKA_KARTICA;
            var list = sifart.Where(p => p.ID_GODINE == id)
                .Select(p => new {
                    p.PROSECNA_CENA,
                    p.KOLICINA__ULAZA,
                    p.VREDNOST_ULAZA,
                    p.KOLICINA_IZLAZA,
                    p.VREDNOST_IZLAZA,
                    p.POCETNO_STANJE_KOLICINA_MAG__KAR_,
                    p.POCETNO_STANJE_VREDNOST_MAG__KAR,
                    p.UK__KOL__MAG__KAR_,
                    p.UK_VR__MAG__KAR
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/POSLOVNA_GODINANext/POPISNI_DOKUMENT/1
        [Route("api/POSLOVNA_GODINANext/POPISNI_DOKUMENT/{id}")]
        [HttpGet]
        [ResponseType(typeof(POPISNI_DOKUMENT))]
        public HttpResponseMessage GetPoslovnaGodinaNext_PopisniDokument(int id)
        {

            IQueryable<POPISNI_DOKUMENT> sifart = db.POPISNI_DOKUMENT;
            var list = sifart.Where(p => p.ID_GODINE == id)
                .Select(p => new { p.BROJ_POPISA, p.DATUM_POPISA });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/POSLOVNA_GODINANext/PROMETNI_DOKUMENT/1
        [Route("api/POSLOVNA_GODINANext/PROMETNI_DOKUMENT/{id}")]
        [HttpGet]
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public HttpResponseMessage GetPoslovnaGodinaNext_PrometniDokument(int id)
        {

            IQueryable<PROMETNI_DOKUMENT> promdom = db.PROMETNI_DOKUMENT;
            var list = promdom.Where(p => p.ID_GODINE == id)
                .Select(p => new { p.VRSTA_PROMETNOG_DOKUMENTA, p.BROJ, p.STATUS, p.DATUM_NASTANKA, p.DATUM_KNJIZENJA });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}