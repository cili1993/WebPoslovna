﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class MAGACINController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/MAGACIN
        public IQueryable<MAGACIN> GetMAGACIN()
        {
            return db.MAGACIN;
        }

        // GET: api/MAGACIN/5
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult GetMAGACIN(int id)
        {
            MAGACIN mAGACIN = db.MAGACIN.Find(id);
            if (mAGACIN == null)
            {
                return NotFound();
            }

            return Ok(mAGACIN);
        }

        // PUT: api/MAGACIN/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAGACIN(int id, MAGACIN mAGACIN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAGACIN.ID_PREDUZECA)
            {
                return BadRequest();
            }

            db.Entry(mAGACIN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAGACINExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MAGACIN
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult PostMAGACIN(MAGACIN mAGACIN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAGACIN.Add(mAGACIN);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MAGACINExists(mAGACIN.ID_PREDUZECA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mAGACIN.ID_PREDUZECA }, mAGACIN);
        }

        // DELETE: api/MAGACIN/5
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult DeleteMAGACIN(int id)
        {
            MAGACIN mAGACIN = db.MAGACIN.SingleOrDefault(m => m.ID_MAGACINA == id);
            if (mAGACIN == null)
            {
                return NotFound();
            }

            db.MAGACIN.Remove(mAGACIN);
            db.SaveChanges();

            return Ok(mAGACIN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAGACINExists(int id)
        {
            return db.MAGACIN.Count(e => e.ID_PREDUZECA == id) > 0;
        }

        // GET: api/MAGACINNext/MAGACINSKA_KARTICA/1
        [Route("api/MAGACINNext/MAGACINSKA_KARTICA/{id}")]
        [HttpGet]
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public HttpResponseMessage GetGRUPA_ASRTIKALANext(int id)
        {

            IQueryable<MAGACINSKA_KARTICA> sifart = db.MAGACINSKA_KARTICA;
            var list = sifart.Where(p => p.ID_MAGACINA == id)
                .Select(p => new { p.PROSECNA_CENA, p.KOLICINA__ULAZA, p.VREDNOST_ULAZA,p.KOLICINA_IZLAZA,p.VREDNOST_IZLAZA,
                p.POCETNO_STANJE_KOLICINA_MAG__KAR_,p.POCETNO_STANJE_VREDNOST_MAG__KAR,p.UK__KOL__MAG__KAR_,p.UK_VR__MAG__KAR});
            
            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/MAGACINNext/POPISNI_DOKUMENT/1
        [Route("api/MAGACINNext/POPISNI_DOKUMENT/{id}")]
        [HttpGet]
        [ResponseType(typeof(POPISNI_DOKUMENT))]
        public HttpResponseMessage GetMagacinANext_PopisniDokument(int id)
        {

            IQueryable<POPISNI_DOKUMENT> sifart = db.POPISNI_DOKUMENT;
            var list = sifart.Where(p => p.ID_MAGACINA == id)
                .Select(p => new { p.BROJ_POPISA, p.DATUM_POPISA });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/MAGACINNext/PROMETNI_DOKUMENT/1
        [Route("api/MAGACINNext/PROMETNI_DOKUMENT/{id}")]
        [HttpGet]
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public HttpResponseMessage GetMagacinNext_PrometniDokument(int id)
        {

            IQueryable<PROMETNI_DOKUMENT> promdom = db.PROMETNI_DOKUMENT;
            var list = promdom.Where(p => p.ID_MAGACINA == id)
                .Select(p => new { p.VRSTA_PROMETNOG_DOKUMENTA, p.BROJ, p.STATUS,p.DATUM_NASTANKA,p.DATUM_KNJIZENJA });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}