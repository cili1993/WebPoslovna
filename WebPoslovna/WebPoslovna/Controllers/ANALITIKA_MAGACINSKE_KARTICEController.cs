﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class ANALITIKA_MAGACINSKE_KARTICEController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/ANALITIKA_MAGACINSKE_KARTICE
        public IQueryable<ANALITIKA_MAGACINSKE_KARTICE> GetANALITIKA_MAGACINSKE_KARTICE()
        {
            return db.ANALITIKA_MAGACINSKE_KARTICE;
        }

        // GET: api/ANALITIKA_MAGACINSKE_KARTICE/5
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult GetANALITIKA_MAGACINSKE_KARTICE(int id)
        {
            ANALITIKA_MAGACINSKE_KARTICE aNALITIKA_MAGACINSKE_KARTICE = db.ANALITIKA_MAGACINSKE_KARTICE.Find(id);
            if (aNALITIKA_MAGACINSKE_KARTICE == null)
            {
                return NotFound();
            }

            return Ok(aNALITIKA_MAGACINSKE_KARTICE);
        }

        // PUT: api/ANALITIKA_MAGACINSKE_KARTICE/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutANALITIKA_MAGACINSKE_KARTICE(int id, ANALITIKA_MAGACINSKE_KARTICE aNALITIKA_MAGACINSKE_KARTICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aNALITIKA_MAGACINSKE_KARTICE.ID_MAG__KART_)
            {
                return BadRequest();
            }

            db.Entry(aNALITIKA_MAGACINSKE_KARTICE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ANALITIKA_MAGACINSKE_KARTICEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ANALITIKA_MAGACINSKE_KARTICE
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult PostANALITIKA_MAGACINSKE_KARTICE(ANALITIKA_MAGACINSKE_KARTICE aNALITIKA_MAGACINSKE_KARTICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ANALITIKA_MAGACINSKE_KARTICE.Add(aNALITIKA_MAGACINSKE_KARTICE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ANALITIKA_MAGACINSKE_KARTICEExists(aNALITIKA_MAGACINSKE_KARTICE.ID_MAG__KART_))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = aNALITIKA_MAGACINSKE_KARTICE.ID_MAG__KART_ }, aNALITIKA_MAGACINSKE_KARTICE);
        }

        // DELETE: api/ANALITIKA_MAGACINSKE_KARTICE/5
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult DeleteANALITIKA_MAGACINSKE_KARTICE(int id)
        {
            ANALITIKA_MAGACINSKE_KARTICE aNALITIKA_MAGACINSKE_KARTICE = db.ANALITIKA_MAGACINSKE_KARTICE.SingleOrDefault(m => m.ID_ANAL__MAG__KART_ == id);
            if (aNALITIKA_MAGACINSKE_KARTICE == null)
            {
                return NotFound();
            }

            db.ANALITIKA_MAGACINSKE_KARTICE.Remove(aNALITIKA_MAGACINSKE_KARTICE);
            db.SaveChanges();

            return Ok(aNALITIKA_MAGACINSKE_KARTICE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ANALITIKA_MAGACINSKE_KARTICEExists(int id)
        {
            return db.ANALITIKA_MAGACINSKE_KARTICE.Count(e => e.ID_MAG__KART_ == id) > 0;
        }
    }
}