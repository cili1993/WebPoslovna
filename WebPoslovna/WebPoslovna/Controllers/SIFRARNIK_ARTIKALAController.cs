﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class SIFRARNIK_ARTIKALAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/SIFRARNIK_ARTIKALA
        public IQueryable<SIFRARNIK_ARTIKALA> GetSIFRARNIK_ARTIKALA()
        {
            return db.SIFRARNIK_ARTIKALA;
        }

        // GET: api/SIFRARNIK_ARTIKALA/5
        [ResponseType(typeof(SIFRARNIK_ARTIKALA))]
        public IHttpActionResult GetSIFRARNIK_ARTIKALA(int id)
        {
            SIFRARNIK_ARTIKALA sIFRARNIK_ARTIKALA = db.SIFRARNIK_ARTIKALA.Find(id);
            if (sIFRARNIK_ARTIKALA == null)
            {
                return NotFound();
            }

            return Ok(sIFRARNIK_ARTIKALA);
        }

        // PUT: api/SIFRARNIK_ARTIKALA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSIFRARNIK_ARTIKALA(int id, SIFRARNIK_ARTIKALA sIFRARNIK_ARTIKALA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sIFRARNIK_ARTIKALA.ID_GRUPE)
            {
                return BadRequest();
            }

            db.Entry(sIFRARNIK_ARTIKALA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SIFRARNIK_ARTIKALAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SIFRARNIK_ARTIKALA
        [ResponseType(typeof(SIFRARNIK_ARTIKALA))]
        public IHttpActionResult PostSIFRARNIK_ARTIKALA(SIFRARNIK_ARTIKALA sIFRARNIK_ARTIKALA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SIFRARNIK_ARTIKALA.Add(sIFRARNIK_ARTIKALA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SIFRARNIK_ARTIKALAExists(sIFRARNIK_ARTIKALA.ID_GRUPE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sIFRARNIK_ARTIKALA.ID_GRUPE }, sIFRARNIK_ARTIKALA);
        }

        // DELETE: api/SIFRARNIK_ARTIKALA/5
        [ResponseType(typeof(SIFRARNIK_ARTIKALA))]
        public IHttpActionResult DeleteSIFRARNIK_ARTIKALA(int id)
        {
            SIFRARNIK_ARTIKALA sIFRARNIK_ARTIKALA = db.SIFRARNIK_ARTIKALA.SingleOrDefault(m => m.ID_ARTIKAL == id);
            if (sIFRARNIK_ARTIKALA == null)
            {
                return NotFound();
            }

            db.SIFRARNIK_ARTIKALA.Remove(sIFRARNIK_ARTIKALA);
            db.SaveChanges();

            return Ok(sIFRARNIK_ARTIKALA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SIFRARNIK_ARTIKALAExists(int id)
        {
            return db.SIFRARNIK_ARTIKALA.Count(e => e.ID_GRUPE == id) > 0;
        }

        // GET: api/SIFRARNIK_ARTIKALANext/STAVKE_POPISA/1
        [Route("api/SIFRARNIK_ARTIKALANext/STAVKE_POPISA/{id}")]
        [HttpGet]
        [ResponseType(typeof(STAVKE_POPISA))]
        public HttpResponseMessage GetSifrarnikArtikalaNext_StavkePopisa(int id)
        {

            IQueryable<STAVKE_POPISA> promdom = db.STAVKE_POPISA;
            var list = promdom.Where(p => p.ID_ARTIKAL == id)
                .Select(p => new {
                    p.KOLICINA_PO_POPISU,
                    p.KOLICINA_U_KARTICI
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/SIFRARNIK_ARTIKALANext/STAVKA_PROMETNOG_DOKUMENTA/1
        [Route("api/SIFRARNIK_ARTIKALANext/STAVKA_PROMETNOG_DOKUMENTA/{id}")]
        [HttpGet]
        [ResponseType(typeof(STAVKA_PROMETNOG_DOKUMENTA))]
        public HttpResponseMessage GetSifrarnikArtikalatNext_StavkaPrometnogDokumenta(int id)
        {

            IQueryable<STAVKA_PROMETNOG_DOKUMENTA> promdom = db.STAVKA_PROMETNOG_DOKUMENTA;
            var list = promdom.Where(p => p.ID_ARTIKAL == id)
                .Select(p => new {
                    p.REDNI_BROJ_STAV__PROM__DOK,
                    p.KOLICINA_STAV__PROM__DOK,
                    p.CENA_STAVKA_PROM__DOK_,
                    p.VREDNOST_STAV__PROM__DOK

                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/SIFRARNIK_ARTIKALANext/MAGACINSKA_KARTICA/1
        [Route("api/SIFRARNIK_ARTIKALANext/MAGACINSKA_KARTICA/{id}")]
        [HttpGet]
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public HttpResponseMessage GetSifrarnikArtikalaNext_MagacinskaKartica(int id)
        {

            IQueryable<MAGACINSKA_KARTICA> sifart = db.MAGACINSKA_KARTICA;
            var list = sifart.Where(p => p.ID_ARTIKAL == id)
                .Select(p => new {
                    p.PROSECNA_CENA,
                    p.KOLICINA__ULAZA,
                    p.VREDNOST_ULAZA,
                    p.KOLICINA_IZLAZA,
                    p.VREDNOST_IZLAZA,
                    p.POCETNO_STANJE_KOLICINA_MAG__KAR_,
                    p.POCETNO_STANJE_VREDNOST_MAG__KAR,
                    p.UK__KOL__MAG__KAR_,
                    p.UK_VR__MAG__KAR
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }


    }



}
