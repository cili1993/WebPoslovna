﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class JEDINICA_MEREController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/JEDINICA_MERE
        public IQueryable<JEDINICA_MERE> GetJEDINICA_MERE()
        {
            return db.JEDINICA_MERE;
        }

        // GET: api/JEDINICA_MERE/5
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult GetJEDINICA_MERE(int id)
        {
            JEDINICA_MERE jEDINICA_MERE = db.JEDINICA_MERE.Find(id);
            if (jEDINICA_MERE == null)
            {
                return NotFound();
            }

            return Ok(jEDINICA_MERE);
        }

        // PUT: api/JEDINICA_MERE/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJEDINICA_MERE(int id, JEDINICA_MERE jEDINICA_MERE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jEDINICA_MERE.ID_JEDINICE)
            {
                return BadRequest();
            }

            db.Entry(jEDINICA_MERE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JEDINICA_MEREExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JEDINICA_MERE
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult PostJEDINICA_MERE(JEDINICA_MERE jEDINICA_MERE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JEDINICA_MERE.Add(jEDINICA_MERE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = jEDINICA_MERE.ID_JEDINICE }, jEDINICA_MERE);
        }

        // DELETE: api/JEDINICA_MERE/5
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult DeleteJEDINICA_MERE(int id)
        {
            JEDINICA_MERE jEDINICA_MERE = db.JEDINICA_MERE.SingleOrDefault(m => m.ID_JEDINICE == id);
            if (jEDINICA_MERE == null)
            {
                return NotFound();
            }

            db.JEDINICA_MERE.Remove(jEDINICA_MERE);
            db.SaveChanges();

            return Ok(jEDINICA_MERE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JEDINICA_MEREExists(int id)
        {
            return db.JEDINICA_MERE.Count(e => e.ID_JEDINICE == id) > 0;
        }

        // GET: api/JEDINICA_MERENext/1
        [Route("api/JEDINICA_MERENext/{id}")]
        [HttpGet]
        [ResponseType(typeof(SIFRARNIK_ARTIKALA))]
        public HttpResponseMessage GetJEDINICA_MERENext(int id)
        {

            IQueryable<SIFRARNIK_ARTIKALA> sifart = db.SIFRARNIK_ARTIKALA;
            var list = sifart.Where(p => p.ID_JEDINICE == id)
                .Select(p => new { p.NAZIV_ARTIKAL });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}