﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class POPISNA_KOMISIJAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/POPISNA_KOMISIJA
        public IQueryable<POPISNA_KOMISIJA> GetPOPISNA_KOMISIJA()
        {
            return db.POPISNA_KOMISIJA;
        }

        // GET: api/POPISNA_KOMISIJA/5
        [ResponseType(typeof(POPISNA_KOMISIJA))]
        public IHttpActionResult GetPOPISNA_KOMISIJA(int id)
        {
            POPISNA_KOMISIJA pOPISNA_KOMISIJA = db.POPISNA_KOMISIJA.Find(id);
            if (pOPISNA_KOMISIJA == null)
            {
                return NotFound();
            }

            return Ok(pOPISNA_KOMISIJA);
        }

        // PUT: api/POPISNA_KOMISIJA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPOPISNA_KOMISIJA(int id, POPISNA_KOMISIJA pOPISNA_KOMISIJA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOPISNA_KOMISIJA.ID__POP__KOMISIJE)
            {
                return BadRequest();
            }

            db.Entry(pOPISNA_KOMISIJA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POPISNA_KOMISIJAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POPISNA_KOMISIJA
        [ResponseType(typeof(POPISNA_KOMISIJA))]
        public IHttpActionResult PostPOPISNA_KOMISIJA(POPISNA_KOMISIJA pOPISNA_KOMISIJA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.POPISNA_KOMISIJA.Add(pOPISNA_KOMISIJA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (POPISNA_KOMISIJAExists(pOPISNA_KOMISIJA.ID__POP__KOMISIJE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pOPISNA_KOMISIJA.ID__POP__KOMISIJE }, pOPISNA_KOMISIJA);
        }

        // DELETE: api/POPISNA_KOMISIJA/5
        [ResponseType(typeof(POPISNA_KOMISIJA))]
        public IHttpActionResult DeletePOPISNA_KOMISIJA(int id)
        {
            POPISNA_KOMISIJA pOPISNA_KOMISIJA = db.POPISNA_KOMISIJA.SingleOrDefault(m => m.ID__POP__KOMISIJE == id);
            if (pOPISNA_KOMISIJA == null)
            {
                return NotFound();
            }

            db.POPISNA_KOMISIJA.Remove(pOPISNA_KOMISIJA);
            db.SaveChanges();

            return Ok(pOPISNA_KOMISIJA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POPISNA_KOMISIJAExists(int id)
        {
            return db.POPISNA_KOMISIJA.Count(e => e.ID__POP__KOMISIJE == id) > 0;
        }
    }
}