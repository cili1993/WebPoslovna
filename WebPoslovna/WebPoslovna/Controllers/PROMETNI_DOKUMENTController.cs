﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class PROMETNI_DOKUMENTController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/PROMETNI_DOKUMENT
        public IQueryable<PROMETNI_DOKUMENT> GetPROMETNI_DOKUMENT()
        {
            return db.PROMETNI_DOKUMENT;
        }

        // GET: api/PROMETNI_DOKUMENT/5
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult GetPROMETNI_DOKUMENT(int id)
        {
            PROMETNI_DOKUMENT pROMETNI_DOKUMENT = db.PROMETNI_DOKUMENT.Find(id);
            if (pROMETNI_DOKUMENT == null)
            {
                return NotFound();
            }

            return Ok(pROMETNI_DOKUMENT);
        }

        // PUT: api/PROMETNI_DOKUMENT/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROMETNI_DOKUMENT(int id, PROMETNI_DOKUMENT pROMETNI_DOKUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROMETNI_DOKUMENT.ID_PROM__DOK_)
            {
                return BadRequest();
            }

            db.Entry(pROMETNI_DOKUMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROMETNI_DOKUMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PROMETNI_DOKUMENT
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult PostPROMETNI_DOKUMENT(PROMETNI_DOKUMENT pROMETNI_DOKUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROMETNI_DOKUMENT.Add(pROMETNI_DOKUMENT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PROMETNI_DOKUMENTExists(pROMETNI_DOKUMENT.ID_GODINE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pROMETNI_DOKUMENT.ID_GODINE }, pROMETNI_DOKUMENT);
        }

        // DELETE: api/PROMETNI_DOKUMENT/5
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult DeletePROMETNI_DOKUMENT(int id)
        {
            PROMETNI_DOKUMENT pROMETNI_DOKUMENT = db.PROMETNI_DOKUMENT.SingleOrDefault(m => m.ID_PROM__DOK_ == id);
            if (pROMETNI_DOKUMENT == null)
            {
                return NotFound();
            }

            db.PROMETNI_DOKUMENT.Remove(pROMETNI_DOKUMENT);
            db.SaveChanges();

            return Ok(pROMETNI_DOKUMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROMETNI_DOKUMENTExists(int id)
        {
            return db.PROMETNI_DOKUMENT.Count(e => e.ID_GODINE == id) > 0;
        }

        // GET: api/PROMETNI_DOKUMENTNext/1
        [Route("api/PROMETNI_DOKUMENTNext/{id}")]
        [HttpGet]
        [ResponseType(typeof(STAVKA_PROMETNOG_DOKUMENTA))]
        public HttpResponseMessage GetPrometniDokumentNext(int id)
        {

            IQueryable<STAVKA_PROMETNOG_DOKUMENTA> promdom = db.STAVKA_PROMETNOG_DOKUMENTA;
            var list = promdom.Where(p => p.ID_PROM__DOK_ == id)
                .Select(p => new {
                    p.REDNI_BROJ_STAV__PROM__DOK,
                    p.KOLICINA_STAV__PROM__DOK,
                    p.CENA_STAVKA_PROM__DOK_,
                    p.VREDNOST_STAV__PROM__DOK

                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}