﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class RADNIKController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/RADNIK
        public IQueryable<RADNIK> GetRADNIK()
        {
            return db.RADNIK;
        }

        // GET: api/RADNIK/5
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult GetRADNIK(int id)
        {
            RADNIK rADNIK = db.RADNIK.Find(id);
            if (rADNIK == null)
            {
                return NotFound();
            }

            return Ok(rADNIK);
        }

        // PUT: api/RADNIK/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRADNIK(int id, RADNIK rADNIK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rADNIK.ID_RADNIKA)
            {
                return BadRequest();
            }

            db.Entry(rADNIK).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RADNIKExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RADNIK
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult PostRADNIK(RADNIK rADNIK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RADNIK.Add(rADNIK);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = rADNIK.ID_RADNIKA }, rADNIK);
        }

        // DELETE: api/RADNIK/5
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult DeleteRADNIK(int id)
        {
            RADNIK rADNIK = db.RADNIK.SingleOrDefault(m => m.ID_RADNIKA == id);
            if (rADNIK == null)
            {
                return NotFound();
            }

            db.RADNIK.Remove(rADNIK);
            db.SaveChanges();

            return Ok(rADNIK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RADNIKExists(int id)
        {
            return db.RADNIK.Count(e => e.ID_RADNIKA == id) > 0;
        }

        // GET: api/RADNIKNext/1
        [Route("api/RADNIKNext/{id}")]
        [HttpGet]
        [ResponseType(typeof(POPISNA_KOMISIJA))]
        public HttpResponseMessage GetRadnikNext_PopisnaKomisija(int id)
        {

            IQueryable<POPISNA_KOMISIJA> promdom = db.POPISNA_KOMISIJA;
            var list = promdom.Where(p => p.ID_RADNIKA == id)
                .Select(p => new {
                    p.FUNKCIJA_U_KOMISIJI

                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}