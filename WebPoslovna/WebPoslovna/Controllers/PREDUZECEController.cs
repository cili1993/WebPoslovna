﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class PREDUZECEController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/PREDUZECE
        public IQueryable<PREDUZECE> GetPREDUZECE()
        {
            return db.PREDUZECE;
        }

        // GET: api/PREDUZECE/5
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult GetPREDUZECE(int id)
        {
            PREDUZECE pREDUZECE = db.PREDUZECE.Find(id);
            if (pREDUZECE == null)
            {
                return NotFound();
            }

            return Ok(pREDUZECE);
        }

        // PUT: api/PREDUZECE/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPREDUZECE(int id, PREDUZECE pREDUZECE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pREDUZECE.ID_PREDUZECA)
            {
                return BadRequest();
            }

            db.Entry(pREDUZECE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PREDUZECEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PREDUZECE
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult PostPREDUZECE(PREDUZECE pREDUZECE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PREDUZECE.Add(pREDUZECE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pREDUZECE.ID_PREDUZECA }, pREDUZECE);
        }

        // DELETE: api/PREDUZECE/5
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult DeletePREDUZECE(int id)
        {
            PREDUZECE pREDUZECE = db.PREDUZECE.SingleOrDefault(m => m.ID_PREDUZECA == id);
            if (pREDUZECE == null)
            {
                return NotFound();
            }

            db.PREDUZECE.Remove(pREDUZECE);
            db.SaveChanges();

            return Ok(pREDUZECE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PREDUZECEExists(int id)
        {
            return db.PREDUZECE.Count(e => e.ID_PREDUZECA == id) > 0;
        }

        // GET: api/PREDUZECENext/POSLOVNA_GODINA/1
        [Route("api/PREDUZECENext/POSLOVNA_GODINA/{id}")]
        [HttpGet]
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public HttpResponseMessage GetPreduzeceNext_PoslovnaGodina(int id)
        {

            IQueryable<POSLOVNA_GODINA> sifart = db.POSLOVNA_GODINA;
            var list = sifart.Where(p => p.ID_PREDUZECA == id)
                .Select(p => new {
                    p.GODINA,
                    p.ZAKLJUCENA_
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/PREDUZECENext/RADNIK/1
        [Route("api/PREDUZECENext/RADNIK/{id}")]
        [HttpGet]
        [ResponseType(typeof(RADNIK))]
        public HttpResponseMessage GetPreduzeceNext_Radnik(int id)
        {

            IQueryable<RADNIK> sifart = db.RADNIK;
            var list = sifart.Where(p => p.ID_PREDUZECA == id)
                .Select(p => new {
                    p.IME_RADNIKA,
                    p.PREZIME_RADNIKA
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/PREDUZECENext/SEKTOR/1
        [Route("api/PREDUZECENext/SEKTOR/{id}")]
        [HttpGet]
        [ResponseType(typeof(SEKTOR))]
        public HttpResponseMessage GetPreduzeceNext_Sektor(int id)
        {

            IQueryable<SEKTOR> sifart = db.SEKTOR;
            var list = sifart.Where(p => p.ID_PREDUZECA == id)
                .Select(p => new {
                    p.NAZIV_SEKTORA
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

    }
}