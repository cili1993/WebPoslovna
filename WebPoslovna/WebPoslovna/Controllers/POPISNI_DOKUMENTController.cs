﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class POPISNI_DOKUMENTController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/POPISNI_DOKUMENT
        public IQueryable<POPISNI_DOKUMENT> GetPOPISNI_DOKUMENT()
        {
            return db.POPISNI_DOKUMENT;
        }

        // GET: api/POPISNI_DOKUMENT/5
        [ResponseType(typeof(POPISNI_DOKUMENT))]
        public IHttpActionResult GetPOPISNI_DOKUMENT(int id)
        {
            POPISNI_DOKUMENT pOPISNI_DOKUMENT = db.POPISNI_DOKUMENT.Find(id);
            if (pOPISNI_DOKUMENT == null)
            {
                return NotFound();
            }

            return Ok(pOPISNI_DOKUMENT);
        }

        // PUT: api/POPISNI_DOKUMENT/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPOPISNI_DOKUMENT(int id, POPISNI_DOKUMENT pOPISNI_DOKUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOPISNI_DOKUMENT.ID_POP__DOK__)
            {
                return BadRequest();
            }

            db.Entry(pOPISNI_DOKUMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POPISNI_DOKUMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POPISNI_DOKUMENT
        [ResponseType(typeof(POPISNI_DOKUMENT))]
        public IHttpActionResult PostPOPISNI_DOKUMENT(POPISNI_DOKUMENT pOPISNI_DOKUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.POPISNI_DOKUMENT.Add(pOPISNI_DOKUMENT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (POPISNI_DOKUMENTExists(pOPISNI_DOKUMENT.ID_POP__DOK__))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pOPISNI_DOKUMENT.ID_POP__DOK__ }, pOPISNI_DOKUMENT);
        }

        // DELETE: api/POPISNI_DOKUMENT/5
        [ResponseType(typeof(POPISNI_DOKUMENT))]
        public IHttpActionResult DeletePOPISNI_DOKUMENT(int id)
        {
            POPISNI_DOKUMENT pOPISNI_DOKUMENT = db.POPISNI_DOKUMENT.SingleOrDefault(m => m.ID_POP__DOK__ == id);
            if (pOPISNI_DOKUMENT == null)
            {
                return NotFound();
            }

            db.POPISNI_DOKUMENT.Remove(pOPISNI_DOKUMENT);
            db.SaveChanges();

            return Ok(pOPISNI_DOKUMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POPISNI_DOKUMENTExists(int id)
        {
            return db.POPISNI_DOKUMENT.Count(e => e.ID_POP__DOK__ == id) > 0;
        }

        // GET: api/POPISNI_DOKUMENTNext/POPISNA_KOMISIJA/1
        [Route("api/POPISNI_DOKUMENTNext/POPISNA_KOMISIJA/{id}")]
        [HttpGet]
        [ResponseType(typeof(POPISNA_KOMISIJA))]
        public HttpResponseMessage GetPOPISNI_DOKUMENTNext_PopisnaKomisija(int id)
        {

            IQueryable<POPISNA_KOMISIJA> promdom = db.POPISNA_KOMISIJA;
            var list = promdom.Where(p => p.ID_POP__DOK__ == id)
                .Select(p => new {
                    p.FUNKCIJA_U_KOMISIJI
                   
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }

        // GET: api/POPISNI_DOKUMENTNext/STAVKE_POPISA/1
        [Route("api/POPISNI_DOKUMENTNext/STAVKE_POPISA/{id}")]
        [HttpGet]
        [ResponseType(typeof(STAVKE_POPISA))]
        public HttpResponseMessage GePopisniDokumentNext_StavkePopisa(int id)
        {

            IQueryable<STAVKE_POPISA> promdom = db.STAVKE_POPISA;
            var list = promdom.Where(p => p.ID_POP__DOK__ == id)
                .Select(p => new {
                    p.KOLICINA_PO_POPISU,
                    p.KOLICINA_U_KARTICI
                 });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}