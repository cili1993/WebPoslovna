﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class GRUPA_ARTIKALAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/GRUPA_ARTIKALA
        public IQueryable<GRUPA_ARTIKALA> GetGRUPA_ARTIKALA()
        {
            return db.GRUPA_ARTIKALA;
        }

        // GET: api/GRUPA_ARTIKALA/5
        [ResponseType(typeof(GRUPA_ARTIKALA))]
        public IHttpActionResult GetGRUPA_ARTIKALA(int id)
        {
            GRUPA_ARTIKALA gRUPA_ARTIKALA = db.GRUPA_ARTIKALA.Find(id);
            if (gRUPA_ARTIKALA == null)
            {
                return NotFound();
            }

            return Ok(gRUPA_ARTIKALA);
        }

        // PUT: api/GRUPA_ARTIKALA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGRUPA_ARTIKALA(int id, GRUPA_ARTIKALA gRUPA_ARTIKALA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gRUPA_ARTIKALA.ID_GRUPE)
            {
                return BadRequest();
            }

            db.Entry(gRUPA_ARTIKALA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GRUPA_ARTIKALAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GRUPA_ARTIKALA
        [ResponseType(typeof(GRUPA_ARTIKALA))]
        public IHttpActionResult PostGRUPA_ARTIKALA(GRUPA_ARTIKALA gRUPA_ARTIKALA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GRUPA_ARTIKALA.Add(gRUPA_ARTIKALA);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = gRUPA_ARTIKALA.ID_GRUPE }, gRUPA_ARTIKALA);
        }

        // DELETE: api/GRUPA_ARTIKALA/5
        [ResponseType(typeof(GRUPA_ARTIKALA))]
        public IHttpActionResult DeleteGRUPA_ARTIKALA(int id)
        {
            GRUPA_ARTIKALA gRUPA_ARTIKALA = db.GRUPA_ARTIKALA.SingleOrDefault(m => m.ID_GRUPE == id);
            if (gRUPA_ARTIKALA == null)
            {
                return NotFound();
            }

            db.GRUPA_ARTIKALA.Remove(gRUPA_ARTIKALA);
            db.SaveChanges();

            return Ok(gRUPA_ARTIKALA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GRUPA_ARTIKALAExists(int id)
        {
            return db.GRUPA_ARTIKALA.Count(e => e.ID_GRUPE == id) > 0;
        }

        // GET: api/GRUPA_ARTIKALANext/1
        [Route("api/GRUPA_ARTIKALANext/{id}")]
        [HttpGet]
        [ResponseType(typeof(SIFRARNIK_ARTIKALA))]
        public HttpResponseMessage GetGRUPA_ASRTIKALANext(int id)
        {

            IQueryable<SIFRARNIK_ARTIKALA> sifart = db.SIFRARNIK_ARTIKALA;
            var list = sifart.Where(p => p.ID_GRUPE == id)
                .Select(p => new { p.NAZIV_ARTIKAL });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}