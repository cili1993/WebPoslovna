﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class MAGACINSKA_KARTICAController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/MAGACINSKA_KARTICA
        public IQueryable<MAGACINSKA_KARTICA> GetMAGACINSKA_KARTICA()
        {
            return db.MAGACINSKA_KARTICA;
        }

        // GET: api/MAGACINSKA_KARTICA/5
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public IHttpActionResult GetMAGACINSKA_KARTICA(int id)
        {
            MAGACINSKA_KARTICA mAGACINSKA_KARTICA = db.MAGACINSKA_KARTICA.Find(id);
            if (mAGACINSKA_KARTICA == null)
            {
                return NotFound();
            }

            return Ok(mAGACINSKA_KARTICA);
        }

        // PUT: api/MAGACINSKA_KARTICA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAGACINSKA_KARTICA(int id, MAGACINSKA_KARTICA mAGACINSKA_KARTICA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mAGACINSKA_KARTICA.ID_MAG__KART_)
            {
                return BadRequest();
            }

            db.Entry(mAGACINSKA_KARTICA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MAGACINSKA_KARTICAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MAGACINSKA_KARTICA
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public IHttpActionResult PostMAGACINSKA_KARTICA(MAGACINSKA_KARTICA mAGACINSKA_KARTICA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MAGACINSKA_KARTICA.Add(mAGACINSKA_KARTICA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MAGACINSKA_KARTICAExists(mAGACINSKA_KARTICA.ID_MAG__KART_))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mAGACINSKA_KARTICA.ID_MAG__KART_ }, mAGACINSKA_KARTICA);
        }

        // DELETE: api/MAGACINSKA_KARTICA/5
        [ResponseType(typeof(MAGACINSKA_KARTICA))]
        public IHttpActionResult DeleteMAGACINSKA_KARTICA(int id)
        {
            MAGACINSKA_KARTICA mAGACINSKA_KARTICA = db.MAGACINSKA_KARTICA.SingleOrDefault(m => m.ID_MAG__KART_ == id);
            if (mAGACINSKA_KARTICA == null)
            {
                return NotFound();
            }

            db.MAGACINSKA_KARTICA.Remove(mAGACINSKA_KARTICA);
            db.SaveChanges();

            return Ok(mAGACINSKA_KARTICA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAGACINSKA_KARTICAExists(int id)
        {
            return db.MAGACINSKA_KARTICA.Count(e => e.ID_MAG__KART_ == id) > 0;
        }

        // GET: api/MAGACINSKA_KARTICANext/1
        [Route("api/MAGACINSKA_KARTICANext/{id}")]
        [HttpGet]
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public HttpResponseMessage GetMagacinskakarticaNext(int id)
        {

            IQueryable<ANALITIKA_MAGACINSKE_KARTICE> promdom = db.ANALITIKA_MAGACINSKE_KARTICE;
            var list = promdom.Where(p => p.ID_MAG__KART_ == id)
                .Select(p => new {
                    p.SIFRA_DOKUMENTA,
                    p.VRSTA_PROMETA,
                    p.DATUM_PROMENE,
                    p.SMER
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}