﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class SEKTORController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/SEKTOR
        public IQueryable<SEKTOR> GetSEKTOR()
        {
            return db.SEKTOR;
        }

        // GET: api/SEKTOR/5
        [ResponseType(typeof(SEKTOR))]
        public IHttpActionResult GetSEKTOR(int id)
        {
            SEKTOR sEKTOR = db.SEKTOR.Find(id);
            if (sEKTOR == null)
            {
                return NotFound();
            }

            return Ok(sEKTOR);
        }

        // PUT: api/SEKTOR/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSEKTOR(int id, SEKTOR sEKTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sEKTOR.ID_PREDUZECA)
            {
                return BadRequest();
            }

            db.Entry(sEKTOR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SEKTORExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SEKTOR
        [ResponseType(typeof(SEKTOR))]
        public IHttpActionResult PostSEKTOR(SEKTOR sEKTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SEKTOR.Add(sEKTOR);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SEKTORExists(sEKTOR.ID_PREDUZECA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sEKTOR.ID_PREDUZECA }, sEKTOR);
        }

        // DELETE: api/SEKTOR/5
        [ResponseType(typeof(SEKTOR))]
        public IHttpActionResult DeleteSEKTOR(int id)
        {
            SEKTOR sEKTOR = db.SEKTOR.SingleOrDefault(m => m.ID_SEKTORA == id);
            if (sEKTOR == null)
            {
                return NotFound();
            }

            db.SEKTOR.Remove(sEKTOR);
            db.SaveChanges();

            return Ok(sEKTOR);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SEKTORExists(int id)
        {
            return db.SEKTOR.Count(e => e.ID_PREDUZECA == id) > 0;
        }

        // GET: api/SEKTORNext/1
        [Route("api/SEKTORNext/{id}")]
        [HttpGet]
        [ResponseType(typeof(MAGACIN))]
        public HttpResponseMessage GetSektorNext(int id)
        {

            IQueryable<MAGACIN> promdom = db.MAGACIN;
            var list = promdom.Where(p => p.ID_SEKTORA == id)
                .Select(p => new {
                    p.NAZIV_MAGACINA
                });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}