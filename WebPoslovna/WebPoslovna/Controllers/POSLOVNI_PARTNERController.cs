﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebPoslovna;

namespace WebPoslovna.Controllers
{
    public class POSLOVNI_PARTNERController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/POSLOVNI_PARTNER
        public IQueryable<POSLOVNI_PARTNER> GetPOSLOVNI_PARTNER()
        {
            return db.POSLOVNI_PARTNER;
        }

        // GET: api/POSLOVNI_PARTNER/5
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public IHttpActionResult GetPOSLOVNI_PARTNER(int id)
        {
            POSLOVNI_PARTNER pOSLOVNI_PARTNER = db.POSLOVNI_PARTNER.Find(id);
            if (pOSLOVNI_PARTNER == null)
            {
                return NotFound();
            }

            return Ok(pOSLOVNI_PARTNER);
        }

        // PUT: api/POSLOVNI_PARTNER/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPOSLOVNI_PARTNER(int id, POSLOVNI_PARTNER pOSLOVNI_PARTNER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOSLOVNI_PARTNER.ID_PARTNERA)
            {
                return BadRequest();
            }

            db.Entry(pOSLOVNI_PARTNER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POSLOVNI_PARTNERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POSLOVNI_PARTNER
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public IHttpActionResult PostPOSLOVNI_PARTNER(POSLOVNI_PARTNER pOSLOVNI_PARTNER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.POSLOVNI_PARTNER.Add(pOSLOVNI_PARTNER);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (POSLOVNI_PARTNERExists(pOSLOVNI_PARTNER.ID_PARTNERA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pOSLOVNI_PARTNER.ID_PARTNERA }, pOSLOVNI_PARTNER);
        }

        // DELETE: api/POSLOVNI_PARTNER/5
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public IHttpActionResult DeletePOSLOVNI_PARTNER(int id)
        {
            POSLOVNI_PARTNER pOSLOVNI_PARTNER = db.POSLOVNI_PARTNER.SingleOrDefault(m => m.ID_PARTNERA == id);
            if (pOSLOVNI_PARTNER == null)
            {
                return NotFound();
            }

            db.POSLOVNI_PARTNER.Remove(pOSLOVNI_PARTNER);
            db.SaveChanges();

            return Ok(pOSLOVNI_PARTNER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POSLOVNI_PARTNERExists(int id)
        {
            return db.POSLOVNI_PARTNER.Count(e => e.ID_PARTNERA == id) > 0;
        }

        // GET: api/POSLOVNI_PARTNERNext/1
        [Route("api/POSLOVNI_PARTNERNext/{id}")]
        [HttpGet]
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public HttpResponseMessage GetPoslovniPartnerNext_PrometniDokument(int id)
        {

            IQueryable<PROMETNI_DOKUMENT> promdom = db.PROMETNI_DOKUMENT;
            var list = promdom.Where(p => p.ID_PARTNERA == id)
                .Select(p => new { p.VRSTA_PROMETNOG_DOKUMENTA, p.BROJ, p.STATUS, p.DATUM_NASTANKA, p.DATUM_KNJIZENJA });

            HttpResponseMessage msg = new HttpResponseMessage();
            msg.Content = new ObjectContent<object>(list, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return msg;
        }
    }
}