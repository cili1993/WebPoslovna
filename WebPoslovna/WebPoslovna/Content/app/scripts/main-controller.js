
var myApp = angular.module('myApp', ['ngTouch', 'ui.grid', 'ui.grid.selection', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls']);
var myLogIn = angular.module('myLogIn', ['ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls']);

myApp.controller('MainController', function ($scope, $http, uiGridConstants ,$compile, $timeout, $uibModal, $log) {

    $scope.isEditable = true;
    $scope.modalDialogs = 'templates/modalDialogs.html';
    var aktivnaTabela = '';
    $scope.selektovaniRed = null;


    $scope.gridOptions = {
        enableRowSelection: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter: true,
        multiSelect: false,
        enableFullRowSelection: true,
        enableSelectAll: false,
        enableColumnResizing: true,
        enableSorting: true,
        enableFiltering: true
    };


    $scope.gridZoomOptions = {
        enableRowSelection: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter: true,
        multiSelect: false,
        enableFullRowSelection: true,
        enableSelectAll: false,
        enableColumnResizing: true,
        enableSorting: true
    };


    $http({
        url: 'http://localhost:60233/api/tables',
        method: "GET"
    })

    .then(function (result) {
        $scope.tableList = angular.copy(result.data);

    })


    $("#table_search").keyup(function () {
        var valin = document.getElementById('table_search').value;
        var result = [];
        for (x in $scope.tableList) {
            if ($scope.tableList[x].includes(valin.toUpperCase())) {
                result.push($scope.tableList[x]);
            }
        }
        document.getElementById('table_list').innerHTML = '';
        for (i in result) {
            var txt = "<a href='#' class='list-group-item' ng-click=\"loadContent('" + result[i] + "')\">" + result[i] + "</a>";
            angular.element(document.getElementById('table_list')).append($compile(
                      txt)($scope));

        }
    })

    $scope.editState = function () {
        $scope.isEditable = true;
    };

    $scope.addState = function () {
        $scope.isEditable = false;
    };

    $scope.isNext = false;

    $scope.logOut = function () {
        window.location = "http://localhost:60233/Content/app/logIn.html";
    }

    $scope.nextMoreTbName = "";


    $scope.storniraj = function(row) {
         var pROMETNI_DOKUMENT = {
            ID_GODINE: null,
            ID_PREDUZECA: null,
            ID_SEKTORA: null,
            ID_MAGACINA: null,
            MAG_ID_PREDUZECA: null,
            MAG_ID_SEKTORA: null,
            MAG_ID_MAGACINA: null,
            ID_PARTNERA: null,
            BROJ: null,
            DATUM_NASTANKA: null,
            DATUM_KNJIZENJA: null,
            STATUS: null,
            VRSTA_PROMETNOG_DOKUMENTA: null
        };

        $scope.promDokument.statusPdok = "STOR";
        $scope.promDokument.datumKPdok = newDate();
        
        pROMETNI_DOKUMENT.ID_GODINE = $scope.promDokument.idGodPdok;
        pROMETNI_DOKUMENT.ID_PARTNERA = $scope.promDokument.idPredPdok;
        pROMETNI_DOKUMENT.ID_SEKTORA = $scope.promDokument.idSekPdok;
        pROMETNI_DOKUMENT.ID_MAGACINA = $scope.promDokument.idMagPdok;
        pROMETNI_DOKUMENT.MAG_ID_PREDUZECA = $scope.promDokument.mIdpPdok;
        pROMETNI_DOKUMENT.MAG_ID_SEKTORA = $scope.promDokument.mIdsPdok;
        pROMETNI_DOKUMENT.MAG_ID_MAGACINA = $scope.promDokument.mIdmPdok;
        pROMETNI_DOKUMENT.ID_PARTNERA = $scope.promDokument.idPartPdok;
        pROMETNI_DOKUMENT.BROJ = $scope.promDokument.brDokPdok;
        pROMETNI_DOKUMENT.DATUM_NASTANKA = $scope.promDokument.datumNpdok;
        pROMETNI_DOKUMENT.DATUM_KNJIZENJA = $scope.promDokument.datumKPdok;
        pROMETNI_DOKUMENT.STATUS = $scope.promDokument.statusPdok;
        pROMETNI_DOKUMENT.VRSTA_PROMETNOG_DOKUMENTA = $scope.promDokument.vrstaPdok;

        updatePrometniDok(pROMETNI_DOKUMENT);
        updateSorniranja();
        event.stopPropagation();
    };
    
    var newDate = function () {
        var date = new Date();
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + '.' + monthIndex + '.' + year + '.';
    }

    var updatePrometniDok = function (pROMETNI_DOKUMENT) {
        pROMETNI_DOKUMENT['ID_PROM__DOK_'] = $scope.promDokument.idPdok;
        $http.put('http://localhost:60233/api/PROMETNI_DOKUMENT/' + $scope.promDokument.idPdok, pROMETNI_DOKUMENT).then(function () {
            $scope.loadContent('PROMETNI_DOKUMENT');
        });
    };

    var updateKnjizenja = function(){
         $http.post('http://localhost:60233/api/knjizenje/' + $scope.promDokument.idPdok);
    };

    var updateSorniranja = function () {
        $http.post('http://localhost:60233/api/storniranje/' + $scope.promDokument.idPdok);
    };

    $scope.proknjizi = function (row) {
        var pROMETNI_DOKUMENT = {
            ID_GODINE: null,
            ID_PREDUZECA: null,
            ID_SEKTORA: null,
            ID_MAGACINA: null,
            MAG_ID_PREDUZECA: null,
            MAG_ID_SEKTORA: null,
            MAG_ID_MAGACINA: null,
            ID_PARTNERA: null,
            BROJ: null,
            DATUM_NASTANKA: null,
            DATUM_KNJIZENJA: null,
            STATUS: null,
            VRSTA_PROMETNOG_DOKUMENTA: null
        };

        $scope.promDokument.statusPdok = "PROK";
        $scope.promDokument.datumKPdok = newDate();
        
        pROMETNI_DOKUMENT.ID_GODINE = $scope.promDokument.idGodPdok;
        pROMETNI_DOKUMENT.ID_PARTNERA = $scope.promDokument.idPredPdok;
        pROMETNI_DOKUMENT.ID_SEKTORA = $scope.promDokument.idSekPdok;
        pROMETNI_DOKUMENT.ID_MAGACINA = $scope.promDokument.idMagPdok;
        pROMETNI_DOKUMENT.MAG_ID_PREDUZECA = $scope.promDokument.mIdpPdok;
        pROMETNI_DOKUMENT.MAG_ID_SEKTORA = $scope.promDokument.mIdsPdok;
        pROMETNI_DOKUMENT.MAG_ID_MAGACINA = $scope.promDokument.mIdmPdok;
        pROMETNI_DOKUMENT.ID_PARTNERA = $scope.promDokument.idPartPdok;
        pROMETNI_DOKUMENT.BROJ = $scope.promDokument.brDokPdok;
        pROMETNI_DOKUMENT.DATUM_NASTANKA = $scope.promDokument.datumNpdok;
        pROMETNI_DOKUMENT.DATUM_KNJIZENJA = $scope.promDokument.datumKPdok;
        pROMETNI_DOKUMENT.STATUS = $scope.promDokument.statusPdok;
        pROMETNI_DOKUMENT.VRSTA_PROMETNOG_DOKUMENTA = $scope.promDokument.vrstaPdok;

        // $scope.savePrometni_dokument($scope.promDokument);
        updatePrometniDok(pROMETNI_DOKUMENT);
        updateKnjizenja();
        event.stopPropagation();
    };

    $scope.proknjiziKorekciju = function (row) {
        $http.post('http://localhost:60233/api/knjizenjePopisa/' + $scope.popDokument.idPdok);
        event.stopPropagation();
    };

    $scope.activeZoomTable = "";

    $scope.loadContentZoom = function (tableName) {
        $scope.activeZoomTable = tableName;
        $scope.pageToShowZoom = 'templates/partials/zoom/' + tableName + 'ZOOM-details.html';

        $http({
            url: 'http://localhost:60233/api/' + tableName,
            method: "GET"
        })
          .then(function (result) {
              $scope.gridZoomOptions.data = angular.copy(result.data);


          })



        //podesavanje kolona
        if ($scope.activeZoomTable == 'ANALITIKA_MAGACINSKE_KARTICE') {
            $scope.gridZoomOptions.columnDefs = [
       		    { name: 'SIFRA_DOKUMENTA', displayName: 'Šifra dokumenta', enableHiding: false },
       		    { name: 'VRSTA_PROMETA', displayName: 'Vrsta prometa', enableHiding: false },
       		    { name: 'DATUM_PROMENE', displayName: 'Datum promene', enableHiding: false },
       		    { name: 'SMER', displayName: 'Smer', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'DRZAVA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'OZNAKA_DRZAVA', displayName: 'Oznaka', enableHiding: false },
			    { name: 'NAZIV_DRZAVA', displayName: 'Drzava', enableHiding: false }
            ];

        } else if ($scope.activeZoomTable == 'GRUPA_ARTIKALA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_GRUPE', displayName: 'Grupa', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'JEDINICA_MERE') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_JEDINICE', displayName: 'Jedinica', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'MAGACIN') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_MAGACINA', displayName: 'Magacin', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'MAGACINSKA_KARTICA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'PROSECNA_CENA', displayName: 'Prosečna cena', enableHiding: false },
			    { name: 'KOLICINA__ULAZA', displayName: 'Količina ulaza', enableHiding: false },
			    { name: 'VREDNOST_ULAZA', displayName: 'Vrednost ulaza', enableHiding: false },
			    { name: 'KOLICINA_IZLAZA', displayName: 'Količina izlaza', enableHiding: false },
			    { name: 'VREDNOST_IZLAZA', displayName: 'Vrednost izlaza', enableHiding: false },
			    { name: 'POCETNO_STANJE_KOLICINA_MAG__KAR_', displayName: 'Početna količina', enableHiding: false },
			    { name: 'POCETNO_STANJE_VREDNOST_MAG__KAR', displayName: 'Početna vrednost', enableHiding: false },
			    { name: 'UK__KOL__MAG__KAR_', displayName: 'Uk. količina', enableHiding: false },
			    { name: 'UK_VR__MAG__KAR', displayName: 'Uk. vrednost', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'MESTO') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'OZNAKA_MESTA', displayName: 'Oznaka', enableHiding: false },
			    { name: 'NAZIV_MESTA', displayName: 'Mesto', enableHiding: false },
			    { name: 'POST_BR', displayName: 'Poštanski broj', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'POPISNA_KOMISIJA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'FUNKCIJA_U_KOMISIJI', displayName: 'Funkcija u komisiji', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'POPISNI_DOKUMENT') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'BROJ_POPISA', displayName: 'Broj popisa', enableHiding: false },
			    { name: 'DATUM_POPISA', displayName: 'Datum popisa', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'POSLOVNA_GODINA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'GODINA', displayName: 'Godina', enableHiding: false },
			    { name: 'ZAKLJUCENA_', displayName: 'Zakljucena', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'POSLOVNI_PARTNER') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_PARTNERA', displayName: 'Partner', enableHiding: false },
			    { name: 'TIP_PARTNERA', displayName: 'Tip', enableHiding: false },
			    { name: 'PIB_PARTNERA', displayName: 'PIB', enableHiding: false },
			    { name: 'ADRESA_PARTNERA', displayName: 'Adresa', enableHiding: false },
			    { name: 'TELEFON_PARTNERA', displayName: 'Telefon', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'PREDUZECE') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_PREDUZECA', displayName: 'Preduzeće', enableHiding: false },
			    { name: 'PIB_PREDUZECA', displayName: 'PIB', enableHiding: false },
			    { name: 'ADRESA_PREDUZECA', displayName: 'Adresa', enableHiding: false },
			    { name: 'TELEFON_PREDUZECA', displayName: 'Telefon', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'PROMETNI_DOKUMENT') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'VRSTA_PROMETNOG_DOKUMENTA', displayName: 'Vrsta prom. dokumenta', enableHiding: false },
			    { name: 'BROJ', displayName: 'Broj', enableHiding: false },
			    { name: 'STATUS', displayName: 'Status', enableHiding: false },
			    { name: 'DATUM_NASTANKA', displayName: 'Datum nastanka', enableHiding: false },
			    { name: 'DATUM_KNJIZENJA', displayName: 'Datum knjiženja', enableHiding: false },
            ];
        } else if ($scope.activeZoomTable == 'RADNIK') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'IME_RADNIKA', displayName: 'Ime', enableHiding: false },
			    { name: 'PREZIME_RADNIKA', displayName: 'Prezime', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'SEKTOR') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_SEKTORA', displayName: 'Sektor', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'SIFRARNIK_ARTIKALA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'NAZIV_ARTIKAL', displayName: 'Artikal', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'STAVKA_PROMETNOG_DOKUMENTA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'REDNI_BROJ_STAV__PROM__DOK', displayName: 'Redni broj', enableHiding: false },
			    { name: 'KOLICINA_STAV__PROM__DOK', displayName: 'Količina', enableHiding: false },
			    { name: 'CENA_STAVKA_PROM__DOK_', displayName: 'Cena', enableHiding: false },
			    { name: 'VREDNOST_STAV__PROM__DOK', displayName: 'Vrednost', enableHiding: false }
            ];
        } else if ($scope.activeZoomTable == 'STAVKE_POPISA') {
            $scope.gridZoomOptions.columnDefs = [
			    { name: 'KOLICINA_PO_POPISU', displayName: 'Količina po popisu', enableHiding: false },
			    { name: 'KOLICINA_U_KARTICI', displayName: 'Količina u kartici', enableHiding: false }
            ];
        }

    };

    $scope.loadContent = function (tableName) {
        aktivnaTabela = tableName;
        $scope.activeTable = tableName;
        $scope.pageToShow = 'templates/partials/' + tableName + '-details.html';
        $scope.nextTb = [];

        var urlNext;
        var id = 1;

        if ($scope.activeTable == "ANALITIKA_MAGACINSKE_KARTICE") {
            $scope.nextTb = [];

        } else if ($scope.activeTable == "DRZAVA") {
            $scope.nextTb = ["MESTO"];
            id = $scope.drzava.idDrzDr;
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;

        } else if ($scope.activeTable == "GRUPA_ARTIKALA") {
            id = $scope.grupa.idGrGA;
            $scope.nextTb = ["SIFRARNIK_ARTIKALA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "JEDINICA_MERE") {
            id = $scope.jedinica.idMJ;
            $scope.nextTb = ["SIFRARNIK_ARTIKALA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "MAGACIN") {
            id = $scope.magacin.idMagMag;
            $scope.nextTb = ["MAGACINSKA_KARTICA", "POPISNI_DOKUMENT", "PROMETNI_DOKUMENT"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "MAGACINSKA_KARTICA") {
            id = $scope.magKart.idMGMK;
            $scope.nextTb = ["ANALITIKA_MAGACINSKE_KARTICE"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "MESTO") {
            id = $scope.mesto.idMst;
            $scope.nextTb = ["POSLOVNI_PARTNER", "PREDUZECE"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.nextTb = [];
        } else if ($scope.activeTable == "POPISNI_DOKUMENT") {
            id = $scope.popDokument.idPDk;
            $scope.nextTb = ["POPISNA_KOMISIJA", "STAVKE_POPISA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "POSLOVNA_GODINA") {
            id = $scope.godina.idPgod;
            $scope.nextTb = ["MAGACINSKA_KARTICA", "POPISNI_DOKUMENT", "PROMETNI_DOKUMENT"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "POSLOVNI_PARTNER") {
            id = $scope.posPartner.idPP;
            $scope.nextTb = ["PROMETNI_DOKUMENT"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "PREDUZECE") {
            id = $scope.preduzece.idPred;
            $scope.nextTb = ["POSLOVNA_GODINA", "RADNIK", "SEKTOR"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "PROMETNI_DOKUMENT") {
            id = $scope.promDokument.idPdok;
            $scope.nextTb = ["STAVKA_PROMETNOG_DOKUMENTA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "RADNIK") {
            id = $scope.radnik.idRd;
            $scope.nextTb = ["POPISNA_KOMISIJA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "SEKTOR") {
            id = $scope.sektor.idS;
            $scope.nextTb = ["MAGACIN"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + id;
        } else if ($scope.activeTable == "SIFRARNIK_ARTIKALA") {
            id = $scope.sifrarnik.idSfr;
            $scope.nextTb = ["MAGACINSKA_KARTICA", "STAVKA_PROMETNOG_DOKUMENTA", "STAVKE_POPISA"];
            urlNext = 'http://localhost:60233/api/' + tableName + 'Next/' + $scope.nextMoreTbName + "/" + id;
        } else if ($scope.activeTable == "STAVKA_PROMETNOG_DOKUMENTA") {
            $scope.nextTb = [];
        } else if ($scope.activeTable == "STAVKE_POPISA") {
            $scope.nextTb = [];
        };



        if ($scope.isNext) {
            if ($scope.nextMoreTbName != "") {
                $scope.tableToPresent = $scope.nextMoreTbName;

            } else {
                $scope.tableToPresent = $scope.nextTb;
            }


            $scope.pageToShowNext = 'templates/partials/' + $scope.tableToPresent + '-details.html';
            $scope.isNext = false;
            $http({
                url: urlNext,
                method: "GET"
            })
          .then(function (result) {
              $scope.gridOptions.data = angular.copy(result.data);

          })

        } else {

            $http({
                url: 'http://localhost:60233/api/' + tableName,
                method: "GET"
            })
           .then(function (result) {
               $scope.gridOptions.data = angular.copy(result.data);

           })
            $scope.tableToPresent = tableName;

        }

        if ($scope.tableToPresent == 'ANALITIKA_MAGACINSKE_KARTICE') {
            $scope.gridOptions.columnDefs = [
       		    { name: 'SIFRA_DOKUMENTA', displayName: 'Šifra dokumenta', enableHiding: false },
       		    { name: 'VRSTA_PROMETA', displayName: 'Vrsta prometa', enableHiding: false },
       		    { name: 'DATUM_PROMENE', displayName: 'Datum promene', enableHiding: false },
       		    { name: 'SMER', displayName: 'Smer', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'DRZAVA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'OZNAKA_DRZAVA', displayName: 'Oznaka', enableHiding: false },
			    { name: 'NAZIV_DRZAVA', displayName: 'Drzava', enableHiding: false }
            ];

        } else if ($scope.tableToPresent == 'GRUPA_ARTIKALA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_GRUPE', displayName: 'Grupa', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'JEDINICA_MERE') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_JEDINICE', displayName: 'Jedinica', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'MAGACIN') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_MAGACINA', displayName: 'Magacin', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'MAGACINSKA_KARTICA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'PROSECNA_CENA', displayName: 'Prosečna cena', enableHiding: false },
			    { name: 'KOLICINA__ULAZA', displayName: 'Količina ulaza', enableHiding: false },
			    { name: 'VREDNOST_ULAZA', displayName: 'Vrednost ulaza', enableHiding: false },
			    { name: 'KOLICINA_IZLAZA', displayName: 'Količina izlaza', enableHiding: false },
			    { name: 'VREDNOST_IZLAZA', displayName: 'Vrednost izlaza', enableHiding: false },
			    { name: 'POCETNO_STANJE_KOLICINA_MAG__KAR_', displayName: 'Početna količina', enableHiding: false },
			    { name: 'POCETNO_STANJE_VREDNOST_MAG__KAR', displayName: 'Početna vrednost', enableHiding: false },
			    { name: 'UK__KOL__MAG__KAR_', displayName: 'Uk. količina', enableHiding: false },
			    { name: 'UK_VR__MAG__KAR', displayName: 'Uk. vrednost', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'MESTO') {
            $scope.gridOptions.columnDefs = [
			    { name: 'OZNAKA_MESTA', displayName: 'Oznaka', enableHiding: false },
			    { name: 'NAZIV_MESTA', displayName: 'Mesto', enableHiding: false },
			    { name: 'POST_BR', displayName: 'Poštanski broj', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'POPISNA_KOMISIJA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'FUNKCIJA_U_KOMISIJI', displayName: 'Funkcija u komisiji', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'POPISNI_DOKUMENT') {
            $scope.gridOptions.columnDefs = [
			    { name: 'BROJ_POPISA', displayName: 'Broj popisa', enableHiding: false },
			    { name: 'DATUM_POPISA', displayName: 'Datum popisa', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'POSLOVNA_GODINA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'GODINA', displayName: 'Godina', enableHiding: false },
			    { name: 'ZAKLJUCENA_', displayName: 'Zakljucena', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'POSLOVNI_PARTNER') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_PARTNERA', displayName: 'Partner', enableHiding: false },
			    { name: 'TIP_PARTNERA', displayName: 'Tip', enableHiding: false },
			    { name: 'PIB_PARTNERA', displayName: 'PIB', enableHiding: false },
			    { name: 'ADRESA_PARTNERA', displayName: 'Adresa', enableHiding: false },
			    { name: 'TELEFON_PARTNERA', displayName: 'Telefon', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'PREDUZECE') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_PREDUZECA', displayName: 'Preduzeće', enableHiding: false },
			    { name: 'PIB_PREDUZECA', displayName: 'PIB', enableHiding: false },
			    { name: 'ADRESA_PREDUZECA', displayName: 'Adresa', enableHiding: false },
			    { name: 'TELEFON_PREDUZECA', displayName: 'Telefon', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'PROMETNI_DOKUMENT') {
            $scope.gridOptions.columnDefs = [
			    { name: 'VRSTA_PROMETNOG_DOKUMENTA', displayName: 'Vrsta prom. dokumenta', enableHiding: false },
			    { name: 'BROJ', displayName: 'Broj', enableHiding: false },
			    { name: 'STATUS', displayName: 'Status', enableHiding: false },
			    { name: 'DATUM_NASTANKA', displayName: 'Datum nastanka', enableHiding: false },
                { name: 'DATUM_KNJIZENJA', displayName: 'Datum knjiženja', enableHiding: false },
			    { name: 'PROKNJIZI', displayName: 'Proknjiži/Storniraj', enableHiding: false, enableFiltering: false,
                cellTemplate: '<div><button class="btn btn-success storn" ng-click="grid.appScope.proknjizi(grid.appScope.selektovaniRed)">Proknjiži</button>&nbsp;&nbsp;&nbsp;<button class="btn btn-warning storn" ng-click="grid.appScope.storniraj(grid.appScope.selektovaniRed)">Storniraj</button></div>'}
            ];
        } else if ($scope.tableToPresent == 'RADNIK') {
            $scope.gridOptions.columnDefs = [
			    { name: 'IME_RADNIKA', displayName: 'Ime', enableHiding: false },
			    { name: 'PREZIME_RADNIKA', displayName: 'Prezime', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'SEKTOR') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_SEKTORA', displayName: 'Sektor', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'SIFRARNIK_ARTIKALA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'NAZIV_ARTIKAL', displayName: 'Artikal', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'STAVKA_PROMETNOG_DOKUMENTA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'REDNI_BROJ_STAV__PROM__DOK', displayName: 'Redni broj', enableHiding: false },
			    { name: 'KOLICINA_STAV__PROM__DOK', displayName: 'Količina', enableHiding: false },
			    { name: 'CENA_STAVKA_PROM__DOK_', displayName: 'Cena', enableHiding: false },
			    { name: 'VREDNOST_STAV__PROM__DOK', displayName: 'Vrednost', enableHiding: false }
            ];
        } else if ($scope.tableToPresent == 'STAVKE_POPISA') {
            $scope.gridOptions.columnDefs = [
			    { name: 'KOLICINA_PO_POPISU', displayName: 'Količina po popisu', enableHiding: false },
			    { name: 'KOLICINA_U_KARTICI', displayName: 'Količina u kartici', enableHiding: false }
            ];
        }
        $timeout(function() {
          $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
        });
    };


    $scope.drzava = {};
    $scope.analMag = {};
    $scope.grupa = {};
    $scope.jedinica = {};
    $scope.magacin = {};
    $scope.magKart = {};
    $scope.mesto = {};
    $scope.popKomisija = {};
    $scope.godina = {};
    $scope.posPartner = {};
    $scope.preduzece = {};
    $scope.promDokument = {};
    $scope.popDokument = {};
    $scope.radnik = {};
    $scope.sektor = {};
    $scope.sifrarnik = {};
    $scope.stavkaProm = {};
    $scope.stavkaPop = {};

    $scope.drzavaZoom = {};
    $scope.analMagZoom = {};
    $scope.grupaZoom = {};
    $scope.jedinicaZoom = {};
    $scope.magacinZoom = {};
    $scope.magKartZoom = {};
    $scope.mestoZoom = {};
    $scope.popKomisijaZoom = {};
    $scope.godinaZoom = {};
    $scope.posPartnerZoom = {};
    $scope.preduzeceZoom = {};
    $scope.promDokumentZoom = {};
    $scope.radnikZoom = {};
    $scope.sektorZoom = {};
    $scope.sifrarnikZoom = {};
    $scope.stavkaPromZoom = {};
    $scope.stavkaPopZoom = {};

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

    };



    $timeout(function () {
        $scope.gridApi.selection.setMultiSelect($scope.gridApi.grid.options.multiSelect);
        $scope.gridOptions.enableFullRowSelection = $scope.gridOptions.enableFullRowSelection;

        $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
            $scope.selektovaniRed = row;

            if (aktivnaTabela == "DRZAVA") {
                $scope.drzava.oznkaDr = row.entity.OZNAKA_DRZAVA;
                $scope.drzava.nazivDr = row.entity.NAZIV_DRZAVA;
                $scope.drzava.idDrzDr = row.entity.ID_DRZAVA;
            } else if (aktivnaTabela == "ANALITIKA_MAGACINSKE_KARTICE") {
                $scope.analMag.idMag = row.entity.ID_MAG__KART_;
                $scope.analMag.sifraAn = row.entity.SIFRA_DOKUMENTA;
                $scope.analMag.rbAn = row.entity.REDNI_BROJ_MAG__ANAL_;
                $scope.analMag.vrstaAn = row.entity.VRSTA_PROMETA;
                $scope.analMag.smerAn = row.entity.SMER;
                $scope.analMag.datumPrometaAn = row.entity.DATUM_PROMENE;
                $scope.analMag.vrednostAn = row.entity.VREDNOST_ANAL__MAG__KART_;
                $scope.analMag.cenaAn = row.entity.CENA_ANAL__MAG__KART_;
                $scope.analMag.kolicinaAn = row.entity.KOLICINA_ANAL__MAG__K_;
                $scope.analMag.idAnAn = row.entity.ID_ANAL__MAG__KART_;
            } else if (aktivnaTabela == "GRUPA_ARTIKALA") {
                $scope.grupa.nazivGA = row.entity.NAZIV_GRUPE
                $scope.grupa.idGrGA = row.entity.ID_GRUPE
            } else if (aktivnaTabela == "JEDINICA_MERE") {
                $scope.jedinica.jedMere = row.entity.NAZIV_JEDINICE;
                $scope.jedinica.idMJ = row.entity.ID_JEDINICE;
            } else if (aktivnaTabela == "MAGACIN") {
                $scope.magacin.idPredMag = row.entity.ID_PREDUZECA;
                $scope.magacin.idSektMag = row.entity.ID_SEKTORA;
                $scope.magacin.nazivMag = row.entity.NAZIV_MAGACINA;
                $scope.magacin.idMagMag = row.entity.ID_MAGACINA;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.magacin.idPredMag).then(function (response) {
                    var resp = response.data;
                    $scope.magacin.nazivPred = resp.NAZIV_PREDUZECA;

                });
                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.magacin.idSektMag).then(function (response) {
                    var resp = response.data;
                    $scope.magacin.nazivSekt = resp.NAZIV_SEKTORA;
                });
            } else if (aktivnaTabela == "MAGACINSKA_KARTICA") {
                $scope.magKart.idGMK = row.entity.ID_GODINE;
                $scope.magKart.idGrMK = row.entity.ID_GRUPE;
                $scope.magKart.idAMK = row.entity.ID_ARTIKAL;
                $scope.magKart.idPrMK = row.entity.ID_PREDUZECA;
                $scope.magKart.idSMK = row.entity.ID_SEKTORA;
                $scope.magKart.idMMK = row.entity.ID_MAGACINA;
                $scope.magKart.cenaMK = row.entity.PROSECNA_CENA;
                $scope.magKart.kolicinaUMK = row.entity.KOLICINA__ULAZA;
                $scope.magKart.vrednostUMK = row.entity.VREDNOST_ULAZA;
                $scope.magKart.kolicinaIMK = row.entity.KOLICINA_IZLAZA;
                $scope.magKart.vrednostIZMK = row.entity.VREDNOST_IZLAZA;
                $scope.magKart.pocKolMK = row.entity.POCETNO_STANJE_KOLICINA_MAG__KAR_;
                $scope.magKart.pocVrMK = row.entity.POCETNO_STANJE_VREDNOST_MAG__KAR;
                $scope.magKart.ukupnaKMK = row.entity.UK__KOL__MAG__KAR_;
                $scope.magKart.ukupnaVMK = row.entity.UK_VR__MAG__KAR;
                $scope.magKart.idMGMK = row.entity.ID_MAG__KART_;
            } else if (aktivnaTabela == "MESTO") {
                $scope.mesto.idDrzaveMst = row.entity.ID_DRZAVA;
                $scope.mesto.nazivMst = row.entity.NAZIV_MESTA;
                $scope.mesto.oznakaMst = row.entity.OZNAKA_MESTA;
                $scope.mesto.postBMst = row.entity.POST_BR;
                $scope.mesto.idMst = row.entity.ID_MESTO;
                $http.get('http://localhost:60233/api/DRZAVA/' + $scope.mesto.idDrzaveMst).then(function (response) {
                    var resp = response.data;
                    $scope.mesto.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.mesto.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if (aktivnaTabela == "POPISNA_KOMISIJA") {
                $scope.popKomisija.idPreduzecaPK = row.entity.ID_PREDUZECA;
                $scope.popKomisija.idSektoraPK = row.entity.ID_SEKTORA;
                $scope.popKomisija.idMagPK = row.entity.ID_MAGACINA;
                $scope.popKomisija.idGodPK = row.entity.ID_GODINE;
                $scope.popKomisija.idPopDokPK = row.entity.ID_POP__DOK__;
                $scope.popKomisija.idRadnikPK = row.entity.ID_RADNIKA;
                $scope.popKomisija.funkcijaPK = row.entity.FUNKCIJA_U_KOMISIJI;
                $scope.popKomisija.idPopK = row.entity.ID__POP__KOMISIJE;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.popKomisija.idPreduzecaPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisija.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.popKomisija.idSektoraPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisija.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.popKomisija.idMagPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisija.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.popKomisija.idGodPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisija.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/RADNIK/' + $scope.popKomisija.idRadnikPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisija.radnik = resp.IME_RADNIKA + " " + resp.PREZIME_RADNIKA;
                });

            } else if (aktivnaTabela == "POPISNI_DOKUMENT") {
                $scope.popDokument.idPredPDk = row.entity.ID_PREDUZECA;
                $scope.popDokument.idSektPDk = row.entity.ID_SEKTORA;
                $scope.popDokument.idMagPDk = row.entity.ID_MAGACINA;
                $scope.popDokument.idGodPDk = row.entity.ID_GODINE;
                $scope.popDokument.brPopPDk = row.entity.BROJ_POPISA;
                $scope.popDokument.datumPopPDk = row.entity.DATUM_POPISA;
                $scope.popDokument.idPDk = row.entity.ID_POP__DOK__;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.popDokument.idPredPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokument.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.popDokument.idSektPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokument.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.popDokument.idMagPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokument.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.popDokument.idGodPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokument.nazivGod = resp.GODINA;
                });
            } else if (aktivnaTabela == "POSLOVNA_GODINA") {
                $scope.godina.idPredPgod = row.entity.ID_PREDUZECA;
                $scope.godina.godPgod = row.entity.GODINA;
                $scope.godina.zakljPgod = row.entity.ZAKLJUCENA_;
                $scope.godina.idPgod = row.entity.ID_GODINE;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.godina.idPredPgod).then(function (response) {
                    var resp = response.data;
                    $scope.godina.nazivPred = resp.NAZIV_PREDUZECA;
                });

            } else if (aktivnaTabela == "POSLOVNI_PARTNER") {
                $scope.posPartner.idDrzPP = row.entity.ID_DRZAVA;
                $scope.posPartner.idMestaPP = row.entity.ID_MESTO;
                $scope.posPartner.nazivPP = row.entity.NAZIV_PARTNERA;
                $scope.posPartner.tipPP = row.entity.TIP_PARTNERA;
                $scope.posPartner.pibPP = row.entity.PIB_PARTNERA;
                $scope.posPartner.adresaPP = row.entity.ADRESA_PARTNERA;
                $scope.posPartner.telefonPP = row.entity.TELEFON_PARTNERA;
                $scope.posPartner.idPP = row.entity.ID_PARTNERA;

                $http.get('http://localhost:60233/api/MESTO/' + $scope.posPartner.idMestaPP).then(function (response) {
                    var resp = response.data;
                    $scope.posPartner.nazivMst = resp.NAZIV_MESTA;
                    $scope.posPartner.oznakaMst = resp.OZNAKA_MESTA;

                });

                $http.get('http://localhost:60233/api/MESTOFrom/' + $scope.posPartner.idMestaPP).then(function (response) {
                    var resp = response.data;
                    $scope.posPartner.idDrzP = resp.ID_DRZAVA;
                    $scope.posPartner.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.posPartner.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if (aktivnaTabela == "PREDUZECE") {
                $scope.preduzece.idMestoP = row.entity.ID_MESTO;
                $scope.preduzece.nazivP = row.entity.NAZIV_PREDUZECA;
                $scope.preduzece.pibP = row.entity.PIB_PREDUZECA;
                $scope.preduzece.adresaP = row.entity.ADRESA_PREDUZECA;
                $scope.preduzece.telefonP = row.entity.TELEFON_PREDUZECA;
                $scope.preduzece.idPred = row.entity.ID_PREDUZECA;
                $http.get('http://localhost:60233/api/MESTO/' + $scope.preduzece.idMestoP).then(function (response) {
                    var resp = response.data;
                    $scope.preduzece.nazivMst = resp.NAZIV_MESTA;
                    $scope.preduzece.oznakaMst = resp.OZNAKA_MESTA;

                });

                $http.get('http://localhost:60233/api/MESTOFrom/' + $scope.preduzece.idMestoP).then(function (response) {
                    var resp = response.data;
                    $scope.preduzece.idDrzP = resp.ID_DRZAVA;
                    $scope.preduzece.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.preduzece.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if (aktivnaTabela == "PROMETNI_DOKUMENT") {
                $scope.promDokument.idGodPdok = row.entity.ID_GODINE;
                $scope.promDokument.idPredPdok = row.entity.MAGACIN.ID_PREDUZECA;
                $scope.promDokument.idSekPdok = row.entity.MAGACIN.ID_SEKTORA;
                $scope.promDokument.idMagPdok = row.entity.ID_MAGACINA;
                $scope.promDokument.mIdpPdok = row.entity.MAGACIN.ID_PREDUZECA;
                $scope.promDokument.mIdsPdok = row.entity.MAGACIN.ID_SEKTORA;
                $scope.promDokument.mIdmPdok = row.entity.MAG_ID_MAGACINA;
                $scope.promDokument.idPartPdok = row.entity.ID_PARTNERA;
                $scope.promDokument.brDokPdok = row.entity.BROJ;
                $scope.promDokument.statusPdok = row.entity.STATUS;
                $scope.promDokument.datumNpdok = row.entity.DATUM_NASTANKA;
                $scope.promDokument.datumKPdok = row.entity.DATUM_KNJIZENJA;
                $scope.promDokument.vrstaPdok = row.entity.VRSTA_PROMETNOG_DOKUMENTA;
                $scope.promDokument.idPdok = row.entity.ID_PROM__DOK_;

                // $scope.promDokument.idPredPdok = row.entity.ID_PREDUZECA;
                // $scope.promDokument.idSekPdok = row.entity.ID_SEKTORA;
                // $scope.promDokument.mIdpPdok = row.entity.MAG_ID_PREDUZECA;
                // $scope.promDokument.mIdsPdok = row.entity.MAG_ID_SEKTORA;   OVAKO JE BILO, AKO NE BUDE RADILO VRATITI PA PROBATI


                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.promDokument.idPredPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokument.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.promDokument.idSekPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokument.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.promDokument.idMagPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokument.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.promDokument.idGodPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokument.nazivGod = resp.GODINA;
                });


            } else if (aktivnaTabela == "RADNIK") {
                $scope.radnik.idPredR = row.entity.ID_PREDUZECA;
                $scope.radnik.imeR = row.entity.IME_RADNIKA;
                $scope.radnik.prezimeR = row.entity.PREZIME_RADNIKA;
                $scope.radnik.idRd = row.entity.ID_RADNIKA;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.radnik.idPredR).then(function (response) {
                    var resp = response.data;
                    $scope.radnik.nazivPred = resp.NAZIV_PREDUZECA;
                });
            } else if (aktivnaTabela == "SEKTOR") {
                $scope.sektor.idPredS = row.entity.ID_PREDUZECA;
                $scope.sektor.nazivS = row.entity.NAZIV_SEKTORA;
                $scope.sektor.idS = row.entity.ID_SEKTORA;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.sektor.idPredS).then(function (response) {
                    var resp = response.data;
                    $scope.sektor.nazivPred = resp.NAZIV_PREDUZECA;
                });
            } else if (aktivnaTabela == "SIFRARNIK_ARTIKALA") {
                $scope.sifrarnik.idGSfr = row.entity.ID_GRUPE;
                $scope.sifrarnik.idMJSfr = row.entity.ID_JEDINICE;
                $scope.sifrarnik.nazivSfr = row.entity.NAZIV_ARTIKAL;
                $scope.sifrarnik.idSfr = row.entity.ID_ARTIKAL;
                $http.get('http://localhost:60233/api/GRUPA_ARTIKALA/' + $scope.sifrarnik.idGSfr).then(function (response) {
                    var resp = response.data;
                    $scope.sifrarnik.nazivGrupe = resp.NAZIV_GRUPE;
                });
                $http.get('http://localhost:60233/api/JEDINICA_MERE/' + $scope.sifrarnik.idMJSfr).then(function (response) {
                    var resp = response.data;
                    $scope.sifrarnik.nazivJedinice = resp.NAZIV_JEDINICE;
                });
            } else if (aktivnaTabela == "STAVKA_PROMETNOG_DOKUMENTA") {
                $scope.stavkaProm.idGodSP = row.entity.ID_GODINE;
                $scope.stavkaProm.idPDSP = row.entity.ID_PROM__DOK_;
                $scope.stavkaProm.idGSP = row.entity.ID_GRUPE;
                $scope.stavkaProm.idASP = row.entity.ID_ARTIKAL;
                $scope.stavkaProm.rbrSP = row.entity.REDNI_BROJ_STAV__PROM__DOK;
                $scope.stavkaProm.kolicinaSP = row.entity.KOLICINA_STAV__PROM__DOK;
                $scope.stavkaProm.cenaSP = row.entity.CENA_STAVKA_PROM__DOK_;
                $scope.stavkaProm.vrednostSP = row.entity.VREDNOST_STAV__PROM__DOK;
                $scope.stavkaProm.idstavkePD = row.entity.ID_STAV__PROM__DOK_;

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.stavkaProm.idGodSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaProm.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/SIFRARNIK_ARTIKALA/' + $scope.stavkaProm.idASP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaProm.nazivArtikal = resp.NAZIV_ARTIKLA;
                });

                $http.get('http://localhost:60233/api/GRUPA_ARTIKALA/' + $scope.stavkaProm.idGSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaProm.nazivGrupe = resp.NAZIV_GRUPE;
                });

            } else if (aktivnaTabela == "STAVKE_POPISA") {
                $scope.stavkaPop.idPredSP = row.entity.ID_PREDUZECA;
                $scope.stavkaPop.idSekSP = row.entity.ID_SEKTORA;
                $scope.stavkaPop.idMagSP = row.entity.ID_MAGACINA;
                $scope.stavkaPop.idGSP = row.entity.ID_GODINE;
                $scope.stavkaPop.idPopDSP = row.entity.ID_POP__DOK__;
                $scope.stavkaPop.idArtSP = row.entity.ID_ARTIKAL;
                $scope.stavkaPop.kolKSP = row.entity.KOLICINA_U_KARTICI;
                $scope.stavkaPop.kolPoPsP = row.entity.KOLICINA_PO_POPISU;
                $scope.stavkaPop.idStvP = row.entity.ID_STAVKE_POPISA;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.stavkaPop.idPredSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPop.nazivPred = resp.NAZIV_PREDUZECA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.stavkaPop.idGSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPop.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.stavkaPop.idSekSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPop.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.stavkaPop.idMagSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPop.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/SIFRARNIK_ARTIKALA/' + $scope.stavkaPop.idArtSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPop.nazivArtikal = resp.NAZIV_ARTIKLA;
                });
            }
        });


    });

    $scope.resetDrzava = function() {
        $scope.drzava = {};
    };

    $scope.resetAnalMag = function() {
        $scope.analMag = {};
    };

    $scope.resetGrupaArtikala = function() {
        $scope.grupa = {};
    };

    $scope.resetJedinica = function() {
        $scope.jedinica = {};
    };

    $scope.resetMagacin = function() {
        $scope.magacin = {};
    };

    $scope.resetMagKart = function() {
        $scope.magKart = {};
    };

    $scope.resetMesto = function() {
        $scope.mesto = {};
    };

    $scope.resetPopKomisija = function() {
        $scope.popKomisija = {};
    };

    $scope.resetPopDokument = function() {
        $scope.popDokument = {};
    };

    $scope.resetGodina = function() {
        $scope.godina = {};
    };

    $scope.resetPosPartner = function() {
        $scope.posPartner = {};
    };

    $scope.resetPreduzece = function() {
        $scope.preduzece = {};
    };

    $scope.resetPromDokument = function() {
        $scope.promDokument = {};
    };

    $scope.resetRadnik = function() {
        $scope.radnik = {};
    };

    $scope.resetSektor = function() {
        $scope.sektor = {};
    };

    $scope.resetSifrarnik = function() {
        $scope.sifrarnik = {};
    };

    $scope.resetStavkaProm = function() {
        $scope.stavkaProm = {};
    };

    $scope.resetStavkaPop = function() {
        $scope.stavkaPop = {};
    };

    $scope.loadContent('DRZAVA');

    $scope.saveDrzava = function (drzava) {
        var dRZAVA = {
            OZNAKA_DRZAVA : null,
            NAZIV_DRZAVA : null
	    };

	    dRZAVA.OZNAKA_DRZAVA = drzava.oznkaDr;
	    dRZAVA.NAZIV_DRZAVA = drzava.nazivDr;
	    if (drzava) {
	        if ($scope.isEditable) {
	            dRZAVA['ID_DRZAVA'] = drzava.idDrzDr;
	            $http.put('http://localhost:60233/api/DRZAVA/' + drzava.idDrzDr, dRZAVA).then(function() {
	                $scope.loadContent('DRZAVA');
	            });
	        } else
	            $http.post('http://localhost:60233/api/DRZAVA', dRZAVA).then(function () {
	                $scope.loadContent('DRZAVA');
	            });
	    }

	};

	$scope.saveAnalitika_mag_kartice = function (analMag) {
	    var aNALITIKA_MAGACINSKE_KARTICE = {
	        ID_MAG__KART_: null,
	        REDNI_BROJ_MAG__ANAL_: null,
	        DATUM_PROMENE: null,
	        VRSTA_PROMETA: null,
	        SIFRA_DOKUMENTA: null,
	        KOLICINA_ANAL__MAG__K_: null,
	        CENA_ANAL__MAG__KART_: null,
	        VREDNOST_ANAL__MAG__KART_: null,
	        SMER: null,
	    };

	    aNALITIKA_MAGACINSKE_KARTICE.ID_MAG__KART_ = analMag.idMag;
	    aNALITIKA_MAGACINSKE_KARTICE.REDNI_BROJ_MAG__ANAL_ = analMag.rbAn;
	    aNALITIKA_MAGACINSKE_KARTICE.DATUM_PROMENE = analMag.datumPrometaAn;
	    aNALITIKA_MAGACINSKE_KARTICE.VRSTA_PROMETA = analMag.vrstaAn;
	    aNALITIKA_MAGACINSKE_KARTICE.SIFRA_DOKUMENTA = analMag.sifraAn;
	    aNALITIKA_MAGACINSKE_KARTICE.KOLICINA_ANAL__MAG__K_ = analMag.kolicinaAn;
	    aNALITIKA_MAGACINSKE_KARTICE.CENA_ANAL__MAG__KART_ = analMag.cenaAn;
	    aNALITIKA_MAGACINSKE_KARTICE.VREDNOST_ANAL__MAG__KART_ = analMag.vrednostAn;
	    aNALITIKA_MAGACINSKE_KARTICE.SMER = analMag.smerAn;

	    if ($scope.isEditable) {
	        aNALITIKA_MAGACINSKE_KARTICE['ID_ANAL__MAG__KART_'] = analMag.idAnAn;
	        $http.put('http://localhost:60233/api/ANALITIKA_MAGACINSKE_KARTICE/' + analMag.idAnAn, aNALITIKA_MAGACINSKE_KARTICE).then(function() {
	        	$scope.loadContent('ANALITIKA_MAGACINSKE_KARTICE');
	        });
	    }else
	        $http.post('http://localhost:60233/api/ANALITIKA_MAGACINSKE_KARTICE', aNALITIKA_MAGACINSKE_KARTICE).then(function () {
	            $scope.loadContent('ANALITIKA_MAGACINSKE_KARTICE');
	        });
	};

	$scope.saveGrupa_artikala = function (grupa) {
	    var gRUPA_ARTIKALA = {
	        NAZIV_GRUPE: null
	    };

	    gRUPA_ARTIKALA.NAZIV_GRUPE = grupa.nazivGA;

	    if ($scope.isEditable) {
	        gRUPA_ARTIKALA['ID_GRUPE'] = grupa.idGrGA;
	        $http.put('http://localhost:60233/api/GRUPA_ARTIKALA/' + grupa.idGrGA, gRUPA_ARTIKALA).then(function() {
	        	$scope.loadContent('GRUPA_ARTIKALA');
	        });
	     }else
	        $http.post('http://localhost:60233/api/GRUPA_ARTIKALA', gRUPA_ARTIKALA).then(function () {
	            $scope.loadContent('GRUPA_ARTIKALA');
	        });
	};

	$scope.saveJedinica_mere = function (jedinica) {
	    var jEDINICA_MERE = {
	        NAZIV_JEDINICE: null
	    };

	    jEDINICA_MERE.NAZIV_JEDINICE = jedinica.jedMere;

	    if ($scope.isEditable) {
	        jEDINICA_MERE['ID_JEDINICE'] = jedinica.idMJ;
	        $http.put('http://localhost:60233/api/JEDINICA_MERE/' + jedinica.idMJ, jEDINICA_MERE).then(function() {
	        	$scope.loadContent('JEDINICA_MERE');
	        });
	    }else
	        $http.post('http://localhost:60233/api/JEDINICA_MERE', jEDINICA_MERE).then(function () {
	            $scope.loadContent('JEDINICA_MERE');
	        });
	};

	$scope.saveMagacin = function (magacin) {
	    var mAGACIN = {
	        ID_PREDUZECA: null,
	        ID_SEKTORA: null,
	        NAZIV_MAGACINA: null
	    };

	    mAGACIN.ID_PREDUZECA = magacin.idPredMag;
	    mAGACIN.ID_SEKTORA = magacin.idSektMag;
	    mAGACIN.NAZIV_MAGACINA = magacin.nazivMag;

	    if ($scope.isEditable) {
	        mAGACIN['ID_MAGACINA'] = magacin.idMagMag;
	        $http.put('http://localhost:60233/api/MAGACIN/' + magacin.idMagMag, mAGACIN).then(function() {
	        	$scope.loadContent('MAGACIN');
	        });
	    }else
	        $http.post('http://localhost:60233/api/MAGACIN', mAGACIN).then(function () {
	            $scope.loadContent('MAGACIN');
	        });
	};

	$scope.saveMagacinska_kartica = function (magKart) {
	    var mAGACINSKA_KARTICA = {
	        ID_GODINE : null,
	        ID_GRUPE : null,
	        ID_ARTIKAL : null,
	        ID_PREDUZECA: null,
	        ID_SEKTORA : null,
	        ID_MAGACINA : null,
	        PROSECNA_CENA : null,
	        KOLICINA__ULAZA : null,
	        VREDNOST_ULAZA : null,
	        KOLICINA_IZLAZA : null,
	        VREDNOST_IZLAZA : null,
	        POCETNO_STANJE_KOLICINA_MAG__KAR_: null,
	        POCETNO_STANJE_VREDNOST_MAG__KAR : null,
	        UK__KOL__MAG__KAR_: null,
	        UK_VR__MAG__KAR: null
	    };

	    mAGACINSKA_KARTICA.ID_GODINE = magKart.idGMK;
	    mAGACINSKA_KARTICA.ID_ARTIKAL = magKart.idAMK;
	    mAGACINSKA_KARTICA.ID_MAGACINA = magKart.idMMK;
	    mAGACINSKA_KARTICA.PROSECNA_CENA = magKart.cenaMK;
	    mAGACINSKA_KARTICA.KOLICINA__ULAZA = magKart.kolicinaUMK;
	    mAGACINSKA_KARTICA.VREDNOST_ULAZA = magKart.cenaMK * magKart.kolicinaUMK;
	    mAGACINSKA_KARTICA.KOLICINA_IZLAZA = magKart.kolicinaIMK;
	    mAGACINSKA_KARTICA.VREDNOST_IZLAZA = magKart.cenaMK * magKart.kolicinaIMK;
	    mAGACINSKA_KARTICA.POCETNO_STANJE_KOLICINA_MAG__KAR_ = magKart.pocKolMK;
	    mAGACINSKA_KARTICA.POCETNO_STANJE_VREDNOST_MAG__KAR = magKart.pocVrMK;
	    mAGACINSKA_KARTICA.UK__KOL__MAG__KAR_ = magKart.kolicinaUMK - magKart.kolicinaIMK;
	    mAGACINSKA_KARTICA.UK_VR__MAG__KAR = mAGACINSKA_KARTICA.VREDNOST_ULAZA - mAGACINSKA_KARTICA.VREDNOST_IZLAZA;

	    if ($scope.isEditable) {
	        mAGACINSKA_KARTICA['ID_MAG__KART_'] = magKart.idMGMK;
	        $http.put('http://localhost:60233/api/MAGACINSKA_KARTICA/' + magKart.idMGMK, mAGACINSKA_KARTICA).then(function() {
	        	$scope.loadContent('MAGACINSKA_KARTICA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/MAGACINSKA_KARTICA', mAGACINSKA_KARTICA).then(function () {
	            $scope.loadContent('MAGACINSKA_KARTICA');
	        });
	};

	$scope.saveMesto = function (mesto) {
	    var mESTO = {
	        ID_DRZAVA: null,
	        OZNAKA_MESTA: null,
	        NAZIV_MESTA: null,
            POST_BR: null
	    };

	    mESTO.ID_DRZAVA = mesto.idDrzaveMst;
	    mESTO.OZNAKA_MESTA = mesto.oznakaMst;
	    mESTO.NAZIV_MESTA = mesto.nazivMst;
	    mESTO.POST_BR = mesto.postBMst;

	    if ($scope.isEditable) {
	        mESTO['ID_MESTO'] = mesto.idMst;
	        $http.put('http://localhost:60233/api/MESTO/' + mesto.idMst, mESTO).then(function() {
	        	$scope.loadContent('MESTO');
	        });
	    }else
	        $http.post('http://localhost:60233/api/MESTO', mESTO).then(function () {
	            $scope.loadContent('MESTO');
	        });
	};

	$scope.savePopisna_komisija = function (popKomisija) {
	    var pOPISNA_KOMISIJA = {
	        ID_PREDUZECA: null,
	        ID_SEKTORA: null,
	        ID_MAGACINA: null,
	        ID_GODINE: null,
	        ID_POP__DOK__: null,
	        ID_RADNIKA: null,
	        FUNKCIJA_U_KOMISIJI : null
	    };

	    pOPISNA_KOMISIJA.ID_PREDUZECA = popKomisija.idPreduzecaPK;
	    pOPISNA_KOMISIJA.ID_SEKTORA = popKomisija.idSektoraPK;
	    pOPISNA_KOMISIJA.ID_MAGACINA = popKomisija.idMagPK;
	    pOPISNA_KOMISIJA.ID_GODINE = popKomisija.idGodPK;
	    pOPISNA_KOMISIJA.ID_POP__DOK__ = popKomisija.idPopDokPK
	    pOPISNA_KOMISIJA.ID_RADNIKA = popKomisija.idRadnikPK;
	    pOPISNA_KOMISIJA.FUNKCIJA_U_KOMISIJI = popKomisija.funkcijaPK;

	    if ($scope.isEditable) {
	        pOPISNA_KOMISIJA['ID__POP__KOMISIJE'] = popKomisija.idPopK;
	        $http.put('http://localhost:60233/api/POPISNA_KOMISIJA/' + popKomisija.idPopK, pOPISNA_KOMISIJA).then(function() {
	        	$scope.loadContent('POPISNA_KOMISIJA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/POPISNA_KOMISIJA', pOPISNA_KOMISIJA).then(function () {
	            $scope.loadContent('POPISNA_KOMISIJA');
	        });
	};

	$scope.savePopisni_dokument = function (popDokument) {
	    var pOPISNI_DOKUMENT = {
	        ID_PREDUZECA: null,
	        ID_SEKTORA: null,
	        ID_MAGACINA: null,
	        ID_GODINE: null,
	        BROJ_POPISA: null,
	        DATUM_POPISA: null
	    };

	    pOPISNI_DOKUMENT.ID_PREDUZECA = popDokument.idPredPDk;
	    pOPISNI_DOKUMENT.ID_SEKTORA = popDokument.idSektPDk;
	    pOPISNI_DOKUMENT.ID_MAGACINA = popDokument.idMagPDk;
	    pOPISNI_DOKUMENT.ID_GODINE = popDokument.idGodPDk;
	    pOPISNI_DOKUMENT.BROJ_POPISA = popDokument.brPopPDk;
	    pOPISNI_DOKUMENT.DATUM_POPISA = popDokument.datumPopPDk;

	    if ($scope.isEditable) {
	        pOPISNI_DOKUMENT['ID_POP__DOK__'] = popDokument.idPDk;
	        $http.put('http://localhost:60233/api/POPISNI_DOKUMENT/' + popDokument.idPDk, pOPISNI_DOKUMENT).then(function() {
	        	$scope.loadContent('POPISNI_DOKUMENT');
	        });
	    }else
	        $http.post('http://localhost:60233/api/POPISNI_DOKUMENT', pOPISNI_DOKUMENT).then(function () {
	            $scope.loadContent('POPISNI_DOKUMENT');
	        });
	};

	$scope.savePoslovna_godina = function (godina) {
	    var pOSLOVNA_GODINA = {
	        ID_PREDUZECA: null,
	        GODINA: null,
	        ZAKLJUCENA_: null
	    };

	    pOSLOVNA_GODINA.ID_PREDUZECA = godina.idPredPgod;
	    pOSLOVNA_GODINA.GODINA = godina.godPgod;
	    pOSLOVNA_GODINA.ZAKLJUCENA_ = godina.zakljPgod;

	    if ($scope.isEditable) {
	        pOSLOVNA_GODINA['ID_GODINE'] = godina.idPgod;
	        $http.put('http://localhost:60233/api/POSLOVNA_GODINA/' + godina.idPgod, pOSLOVNA_GODINA).then(function() {
	        	$scope.loadContent('POSLOVNA_GODINA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/POSLOVNA_GODINA', pOSLOVNA_GODINA).then(function () {
	            $scope.loadContent('POSLOVNA_GODINA');
	        });
	};

	$scope.savePoslovni_partner = function (posPartner) {
	    var pOSLOVNI_PARTNER = {
	        ID_DRZAVA: null,
	        ID_MESTO: null,
	        TIP_PARTNERA: null,
	        NAZIV_PARTNERA: null,
	        PIB_PARTNERA: null,
	        ADRESA_PARTNERA: null,
	        TELEFON_PARTNERA: null
	    };

	    pOSLOVNI_PARTNER.ID_DRZAVA = posPartner.idDrzPP;
	    pOSLOVNI_PARTNER.ID_MESTO = posPartner.idMestaPP;
	    pOSLOVNI_PARTNER.TIP_PARTNERA = posPartner.tipPP;
	    pOSLOVNI_PARTNER.NAZIV_PARTNERA = posPartner.nazivPP;
	    pOSLOVNI_PARTNER.PIB_PARTNERA = posPartner.pibPP;
	    pOSLOVNI_PARTNER.ADRESA_PARTNERA = posPartner.adresaPP;
	    pOSLOVNI_PARTNER.TELEFON_PARTNERA = posPartner.telefonPP;

	    if ($scope.isEditable) {
	        pOSLOVNI_PARTNER['ID_PARTNERA'] = posPartner.idPP;
	        $http.put('http://localhost:60233/api/POSLOVNI_PARTNER/' + posPartner.idPP, pOSLOVNI_PARTNER).then(function() {
	        	$scope.loadContent('POSLOVNI_PARTNER');
	        });
	    }else
	        $http.post('http://localhost:60233/api/POSLOVNI_PARTNER', pOSLOVNI_PARTNER).then(function () {
	            $scope.loadContent('POSLOVNI_PARTNER');
	        });
	};

	$scope.savePreduzece = function (preduzece) {
	    var pREDUZECE = {
	        ID_DRZAVA: null,
	        ID_MESTO: null,
	        NAZIV_PREDUZECA: null,
	        PIB_PREDUZECA: null,
	        ADRESA_PREDUZECA: null,
	        TELEFON_PREDUZECA: null
	    };

	    pREDUZECE.ID_DRZAVA = preduzece.idDrzP;
	    pREDUZECE.ID_MESTO = preduzece.idMestoP;
	    pREDUZECE.NAZIV_PREDUZECA = preduzece.nazivP;
	    pREDUZECE.PIB_PREDUZECA = preduzece.pibP;
	    pREDUZECE.ADRESA_PREDUZECA = preduzece.adresaP;
	    pREDUZECE.TELEFON_PREDUZECA = preduzece.telefonP;


	    if ($scope.isEditable) {
	        pREDUZECE['ID_PREDUZECA'] = preduzece.idPred;
	        $http.put('http://localhost:60233/api/PREDUZECE/' + preduzece.idPred, pREDUZECE).then(function() {
	        	$scope.loadContent('PREDUZECE');
	        });
	    }else
	        $http.post('http://localhost:60233/api/PREDUZECE', pREDUZECE).then(function () {
	            $scope.loadContent('PREDUZECE');
	        });
	};

	$scope.savePrometni_dokument = function (promDokument) {
	    var pROMETNI_DOKUMENT = {
	        ID_GODINE: null,
	        ID_PREDUZECA: null,
	        ID_SEKTORA: null,
	        ID_MAGACINA: null,
	        MAG_ID_PREDUZECA: null,
	        MAG_ID_SEKTORA: null,
	        MAG_ID_MAGACINA: null,
	        ID_PARTNERA: null,
	        BROJ: null,
	        DATUM_NASTANKA: null,
	        DATUM_KNJIZENJA: null,
	        STATUS: null,
	        VRSTA_PROMETNOG_DOKUMENTA: null
	    };

	    pROMETNI_DOKUMENT.ID_GODINE = promDokument.idGodPdok;
	    pROMETNI_DOKUMENT.ID_PARTNERA = promDokument.idPredPdok;
	    pROMETNI_DOKUMENT.ID_SEKTORA = promDokument.idSekPdok;
	    pROMETNI_DOKUMENT.ID_MAGACINA = promDokument.idMagPdok;
	    pROMETNI_DOKUMENT.MAG_ID_PREDUZECA = promDokument.mIdpPdok;
	    pROMETNI_DOKUMENT.MAG_ID_SEKTORA = promDokument.mIdsPdok;
	    pROMETNI_DOKUMENT.MAG_ID_MAGACINA = promDokument.mIdmPdok;
	    pROMETNI_DOKUMENT.ID_PARTNERA = promDokument.idPartPdok;
	    pROMETNI_DOKUMENT.BROJ = promDokument.brDokPdok;
	    pROMETNI_DOKUMENT.DATUM_NASTANKA = promDokument.datumNpdok; 
	    pROMETNI_DOKUMENT.DATUM_KNJIZENJA = promDokument.datumKPdok;
	    pROMETNI_DOKUMENT.STATUS = "FORM";
	    pROMETNI_DOKUMENT.VRSTA_PROMETNOG_DOKUMENTA = promDokument.vrstaPdok;

	    if ($scope.isEditable) {
	        pROMETNI_DOKUMENT['ID_PROM__DOK_'] = promDokument.idPdok;
	        $http.put('http://localhost:60233/api/PROMETNI_DOKUMENT/' + promDokument.idPdok, pROMETNI_DOKUMENT).then(function() {
	        	$scope.loadContent('PROMETNI_DOKUMENT');
	        });
	    }else
	        $http.post('http://localhost:60233/api/PROMETNI_DOKUMENT', pROMETNI_DOKUMENT).then(function () {
	            $scope.loadContent('PROMETNI_DOKUMENT');
	        });
	};

	$scope.saveRadnik = function (radnik) {
	    var rADNIK = {
	        ID_PREDUZECA: null,
	        IME_RADNIKA: null,
	        PREZIME_RADNIKA: null
	    };

	    rADNIK.ID_PREDUZECA = radnik.idPredR;
	    rADNIK.IME_RADNIKA = radnik.imeR;
	    rADNIK.PREZIME_RADNIKA = radnik.prezimeR;

	    if ($scope.isEditable) {
	        rADNIK['ID_RADNIKA'] = radnik.idRd;
	        $http.put('http://localhost:60233/api/RADNIK/' + radnik.idRd, rADNIK).then(function() {
	        	$scope.loadContent('RADNIK');
	        });
	    }else
	        $http.post('http://localhost:60233/api/RADNIK', rADNIK).then(function () {
	            $scope.loadContent('RADNIK');
	        });
	};

	$scope.saveSektor = function (sektor) {
	    var sEKTOR = {
	        ID_PREDUZECA: null,
	        NAZIV_SEKTORA: null
	    };

	    sEKTOR.ID_PREDUZECA = sektor.idPredS;
	    sEKTOR.NAZIV_SEKTORA = sektor.nazivS;

	    if ($scope.isEditable) {
	        sEKTOR['ID_SEKTORA'] = sektor.idS;
	        $http.put('http://localhost:60233/api/SEKTOR/' + sektor.idS, sEKTOR).then(function() {
	        	$scope.loadContent('SEKTOR');
	        });
	    }else
	        $http.post('http://localhost:60233/api/SEKTOR', sEKTOR).then(function () {
	            $scope.loadContent('SEKTOR');
	        });
	};

	$scope.saveSifrarnik_artikala = function (sifrarnik) {
	    var sIFRARNIK_ARTIKALA = {
	        ID_GRUPE: null,
	        ID_JEDINICE: null,
	        NAZIV_ARTIKAL: null
	    };

	    sIFRARNIK_ARTIKALA.ID_GRUPE = sifrarnik.idGSfr;
	    sIFRARNIK_ARTIKALA.ID_JEDINICE = sifrarnik.idMJSfr;
	    sIFRARNIK_ARTIKALA.NAZIV_ARTIKAL = sifrarnik.nazivSfr;

	    if ($scope.isEditable) {
	        sIFRARNIK_ARTIKALA['ID_ARTIKAL'] = sifrarnik.idSfr;
	        $http.put('http://localhost:60233/api/SIFRARNIK_ARTIKALA/' + sifrarnik.idSfr, sIFRARNIK_ARTIKALA).then(function() {
	        	$scope.loadContent('SIFRARNIK_ARTIKALA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/SIFRARNIK_ARTIKALA', sIFRARNIK_ARTIKALA).then(function () {
	            $scope.loadContent('SIFRARNIK_ARTIKALA');
	        });
	};

	$scope.saveStavka_prometnog_dok = function (stavkaProm) {
	    var sTAVKA_PROMETNOG_DOKUMENTA = {
	        ID_GODINE: null,
	        ID_PROM__DOK_: null,
	        ID_GRUPE: null,
	        ID_ARTIKAL: null,
	        REDNI_BROJ_STAV__PROM__DOK: null,
	        KOLICINA_STAV__PROM__DOK: null,
	        CENA_STAVKA_PROM__DOK_: null,
	        VREDNOST_STAV__PROM__DOK: null
	    };

	    sTAVKA_PROMETNOG_DOKUMENTA.ID_GODINE = stavkaProm.idGodSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.ID_PROM__DOK_ = stavkaProm.idPDSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.ID_GRUPE = stavkaProm.idGSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.ID_ARTIKAL = stavkaProm.idASP;
	    sTAVKA_PROMETNOG_DOKUMENTA.REDNI_BROJ_STAV__PROM__DOK = stavkaProm.rbrSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.KOLICINA_STAV__PROM__DOK = stavkaProm.kolicinaSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.CENA_STAVKA_PROM__DOK_ = stavkaProm.cenaSP;
	    sTAVKA_PROMETNOG_DOKUMENTA.VREDNOST_STAV__PROM__DOK = stavkaProm.kolicinaSP * stavkaProm.cenaSP;

	    if ($scope.isEditable) {
	        sTAVKA_PROMETNOG_DOKUMENTA['ID_STAV__PROM__DOK_'] = stavkaProm.idstavkePD;
	        $http.put('http://localhost:60233/api/STAVKA_PROMETNOG_DOKUMENTA/' + stavkaProm.idstavkePD, sTAVKA_PROMETNOG_DOKUMENTA).then(function() {
	        	$scope.loadContent('STAVKA_PROMETNOG_DOKUMENTA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/STAVKA_PROMETNOG_DOKUMENTA', sTAVKA_PROMETNOG_DOKUMENTA).then(function () {
	            $scope.loadContent('STAVKA_PROMETNOG_DOKUMENTA');
	        });
	};

	$scope.saveStavke_popisa = function (stavkaPop) {
	    var sTAVKE_POPISA = {
	        ID_PREDUZECA: null,
	        ID_SEKTORA: null,
	        ID_MAGACINA: null,
	        ID_GODINE: null,
	        ID_POP__DOK__: null,
	        ID_GRUPE: null,
	        ID_ARTIKAL: null,
	        KOLICINA_PO_POPISU: null,
	        KOLICINA_U_KARTICI: null
	    };

	    sTAVKE_POPISA.ID_PREDUZECA = stavkaPop.idPredSP;
	    sTAVKE_POPISA.ID_SEKTORA = stavkaPop.idSekSP;
	    sTAVKE_POPISA.ID_MAGACINA = stavkaPop.idMagSP;
	    sTAVKE_POPISA.ID_GODINE = stavkaPop.idGSP;
	    sTAVKE_POPISA.ID_POP__DOK__ = stavkaPop.idPopDSP;
	    sTAVKE_POPISA.ID_GRUPE = stavkaPop.idGSp;
	    sTAVKE_POPISA.ID_ARTIKAL = stavkaPop.idArtSP;
	    sTAVKE_POPISA.KOLICINA_PO_POPISU = stavkaPop.kolPoPsP;
	    sTAVKE_POPISA.KOLICINA_U_KARTICI = stavkaPop.kolKSP;

	    if ($scope.isEditable) {
	        sTAVKE_POPISA['ID_STAVKE_POPISA'] = stavkaPop.idStvP;
	        $http.put('http://localhost:60233/api/STAVKE_POPISA/' + stavkaPop.idStvP, sTAVKE_POPISA).then(function() {
	        	$scope.loadContent('STAVKE_POPISA');
	        });
	    }else
	        $http.post('http://localhost:60233/api/STAVKE_POPISA', sTAVKE_POPISA).then(function () {
	            $scope.loadContent('STAVKE_POPISA');
	        });
	};

	$scope.deleteData = function () {

	    $scope.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
	        if (row.entity != "undefined") {
	            if (aktivnaTabela == "DRZAVA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_DRZAVA).then(function () {
	                    $scope.loadContent('DRZAVA');
	                });
	            } else if (aktivnaTabela == "ANALITIKA_MAGACINSKE_KARTICE") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_ANAL__MAG__KART_).then(function () {
	            $scope.loadContent('ANALITIKA_MAGACINSKE_KARTICE');
	                 });
	            } else if (aktivnaTabela == "GRUPA_ARTIKALA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_GRUPE).then(function () {
	            $scope.loadContent('GRUPA_ARTIKALA');
	                 });
	            } else if (aktivnaTabela == "JEDINICA_MERE") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_JEDINICE).then(function () {
	            $scope.loadContent('JEDINICA_MERE');
	                 });
	            } else if (aktivnaTabela == "MAGACIN") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_MAGACINA).then(function () {
	            $scope.loadContent('MAGACIN');
	                });
	            } else if (aktivnaTabela == "MAGACINSKA_KARTICA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_MAG__KART_).then(function () {
	            $scope.loadContent('MAGACINSKA_KARTICA');
	                 });
	            } else if (aktivnaTabela == "MESTO") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_MESTO).then(function () {
	            $scope.loadContent('MESTO');
	                });
	            } else if (aktivnaTabela == "POPISNA_KOMISIJA") {
	                $http.delete('http://POPISNA_KOMISIJA:60233/api/' + aktivnaTabela + '/' + row.entity.ID__POP__KOMISIJE).then(function () {
	            $scope.loadContent('POPISNA_KOMISIJA');
	                 });
	            } else if (aktivnaTabela == "POPISNI_DOKUMENT") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_POP__DOK__).then(function () {
	            $scope.loadContent('POPISNI_DOKUMENT');
	                 });
	            } else if (aktivnaTabela == "POSLOVNA_GODINA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_GODINE).then(function () {
	                    $scope.loadContent('POSLOVNA_GODINA');
	                });
	            } else if (aktivnaTabela == "POSLOVNI_PARTNER") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_PARTNERA).then(function () {
	                    $scope.loadContent('POSLOVNI_PARTNER');
	                });
	            } else if (aktivnaTabela == "PREDUZECE") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_PREDUZECA).then(function () {
	                    $scope.loadContent('PREDUZECE');
	                });
	            } else if (aktivnaTabela == "PROMETNI_DOKUMENT") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_PROM__DOK_).then(function () {
	                    $scope.loadContent('PROMETNI_DOKUMENT');
	                });
	            } else if (aktivnaTabela == "RADNIK") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_RADNIKA).then(function () {
	                    $scope.loadContent('RADNIK');
	                });
	            } else if (aktivnaTabela == "SEKTOR") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_SEKTORA).then(function () {
	                    $scope.loadContent('SEKTOR');
	                });
	            } else if (aktivnaTabela == "SIFRARNIK_ARTIKALA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_ARTIKAL).then(function () {
	                    $scope.loadContent('SIFRARNIK_ARTIKALA');
	                });
	            } else if (aktivnaTabela == "STAVKA_PROMETNOG_DOKUMENTA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_STAV__PROM__DOK_).then(function () {
	                    $scope.loadContent('STAVKA_PROMETNOG_DOKUMENTA');
	                });
	            } else if (aktivnaTabela == "STAVKE_POPISA") {
	                $http.delete('http://localhost:60233/api/' + aktivnaTabela + '/' + row.entity.ID_STAVKE_POPISA).then(function () {
	                    $scope.loadContent('STAVKE_POPISA');
	                });
	            };
	        };
	    });
	};

   //modals

    $scope.openNext = function (name) {
        if (name != "") {
            $scope.nextMoreTbName = name;
        };

        $scope.isNext = true;

        $scope.loadContent($scope.activeTable);

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalNext.html',
            controller: 'ModalInstanceNextCtrl',
            windowClass: 'app-modal-window',
            scope: $scope,
            resolve: {
                name: function () {
                    return name;

                }

            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
            $scope.loadContent($scope.activeTable);
        });
    };

    $scope.isZoom = false;

    $scope.openZoom = function (name) {
        $scope.loadContentZoom(name);
        $scope.isZoom = true;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalZoom.html',
            controller: 'ModalInstanceZoomCtrl',
            windowClass: 'app-modal-window',
            scope: $scope,
            resolve: {
                name: function () {
                    return name;

                }

            }
        });

        modalInstance.result.then(function (selectedItem) {

        }, function () {



            $log.info('Modal dismissed at: ' + new Date());

        });
    };



    $scope.openReport1 = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'myModalReport1.html',
            controller: 'ModalInstanceReportCtrl',
            windowClass: 'app-modal-window',
            scope: $scope,
            resolve: {
                name: function () {
                    return name;
                }
            }
        });
    };

    $scope.openReport2 = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'myModalReport2.html',
            controller: 'ModalInstanceReportCtrl',
            windowClass: 'app-modal-window',
            scope: $scope,
            resolve: {
                name: function () {
                    return name;
                }
            }
        });
    };

});

myLogIn.controller('LogInController', ['$scope', '$http', function ($scope, $http) {

    $scope.checkLogIn = function () {
        var email = $scope.email;
        var pass = $scope.pass;

        if (email == 'admin@example.com' && pass == 'admin')
            window.location = 'http://localhost:60233/Content/app/index.html';
        else
            alert("Wrong email or password!");


        // $http.post('http://localhost:60233/api/login/' + $scope.email + '/' + $scope.pass)
    }
}]);


myApp.controller('ModalInstanceNextCtrl', function ($scope, $uibModalInstance, name) {


    $scope.pageToShow = 'templates/partials/' + $scope.activeTable + '-details.html';


    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.loadContent($scope.activeTable);
    };

});



myApp.controller('ModalInstanceZoomCtrl', function ($scope, $uibModalInstance, name, $timeout, $http) {
    $scope.gridZoomOptions.onRegisterApi = function (gridApi) {
        $scope.gridZoomApi = gridApi;

    };

    $scope.editState = function () {
        $scope.isEditable = true;
    };

    $scope.addState = function () {
        $scope.isEditable = false;
    };

    //  $scope.pageToShow = 'templates/partials/' + $scope.activeTable + '-details.html';

    $timeout(function () {
        $scope.gridZoomApi.selection.setMultiSelect($scope.gridZoomApi.grid.options.multiSelect);
        $scope.gridZoomOptions.enableFullRowSelection = $scope.gridZoomOptions.enableFullRowSelection;
        $scope.gridZoomApi.selection.on.rowSelectionChanged($scope, function (row) {
            if ($scope.activeZoomTable == "DRZAVA") {
                $scope.drzavaZoom.oznkaDr = row.entity.OZNAKA_DRZAVA;
                $scope.drzavaZoom.nazivDr = row.entity.NAZIV_DRZAVA;
                $scope.drzavaZoom.idDrzDr = row.entity.ID_DRZAVA;
            } else if ($scope.activeZoomTable == "ANALITIKA_MAGACINSKE_KARTICE") {
                $scope.analMagZoom.idMag = row.entity.ID_MAG__KART_;
                $scope.analMagZoom.sifraAn = row.entity.SIFRA_DOKUMENTA;
                $scope.analMagZoom.rbAn = row.entity.REDNI_BROJ_MAG__ANAL_;
                $scope.analMagZoom.vrstaAn = row.entity.VRSTA_PROMETA;
                $scope.analMagZoom.smerAn = row.entity.SMER;
                $scope.analMagZoom.datumPrometaAn = row.entity.DATUM_PROMENE;
                $scope.analMagZoom.vrednostAn = row.entity.VREDNOST_ANAL__MAG__KART_;
                $scope.analMagZoom.cenaAn = row.entity.CENA_ANAL__MAG__KART_;
                $scope.analMagZoom.kolicinaAn = row.entity.KOLICINA_ANAL__MAG__K_;
                $scope.analMagZoom.idAnAn = row.entity.ID_ANAL__MAG__KART_;
            } else if ($scope.activeZoomTable == "GRUPA_ARTIKALA") {
                $scope.grupaZoom.nazivGA = row.entity.NAZIV_GRUPE
                $scope.grupaZoom.idGrGA = row.entity.ID_GRUPE
            } else if ($scope.activeZoomTable == "JEDINICA_MERE") {
                $scope.jedinicaZoom.jedMere = row.entity.NAZIV_JEDINICE;
                $scope.jedinicaZoom.idMJ = row.entity.ID_JEDINICE;
            } else if ($scope.activeZoomTable == "MAGACIN") {
                $scope.magacinZoom.idPredMag = row.entity.ID_PREDUZECA;
                $scope.magacinZoom.idSektMag = row.entity.ID_SEKTORA;
                $scope.magacinZoom.nazivMag = row.entity.NAZIV_MAGACINA;
                $scope.magacinZoom.idMagMag = row.entity.ID_MAGACINA;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.magacinZoom.idPredMag).then(function (response) {
                    var resp = response.data;
                    $scope.magacinZoom.nazivPred = resp.NAZIV_PREDUZECA;

                });
                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.magacinZoom.idSektMag).then(function (response) {
                    var resp = response.data;
                    $scope.magacinZoom.nazivSekt = resp.NAZIV_SEKTORA;
                });
            } else if ($scope.activeZoomTable == "MAGACINSKA_KARTICA") {
                $scope.magKartZoom.idGMK = row.entity.ID_GODINE;
                $scope.magKartZoom.idGrMK = row.entity.ID_GRUPE;
                $scope.magKartZoom.idAMK = row.entity.ID_ARTIKAL;
                $scope.magKartZoom.idPrMK = row.entity.ID_PREDUZECA;
                $scope.magKartZoom.idSMK = row.entity.ID_SEKTORA;
                $scope.magKartZoom.idMMK = row.entity.ID_MAGACINA;
                $scope.magKartZoom.cenaMK = row.entity.PROSECNA_CENA;
                $scope.magKartZoom.kolicinaUMK = row.entity.KOLICINA__ULAZA;
                $scope.magKartZoom.vrednostUMK = row.entity.VREDNOST_ULAZA;
                $scope.magKartZoom.kolicinaIMK = row.entity.KOLICINA_IZLAZA;
                $scope.magKartZoom.vrednostIZMK = row.entity.VREDNOST_IZLAZA;
                $scope.magKartZoom.pocKolMK = row.entity.POCETNO_STANJE_KOLICINA_MAG__KAR_;
                $scope.magKartZoom.pocVrMK = row.entity.POCETNO_STANJE_VREDNOST_MAG__KAR;
                $scope.magKartZoom.ukupnaKMK = row.entity.UK__KOL__MAG__KAR_;
                $scope.magKartZoom.ukupnaVMK = row.entity.UK_VR__MAG__KAR;
                $scope.magKartZoom.idMGMK = row.entity.ID_MAG__KART_;
            } else if ($scope.activeZoomTable == "MESTO") {
                $scope.mestoZoom.idDrzaveMst = row.entity.ID_DRZAVA;
                $scope.mestoZoom.nazivMst = row.entity.NAZIV_MESTA;
                $scope.mestoZoom.oznakaMst = row.entity.OZNAKA_MESTA;
                $scope.mestoZoom.postBMst = row.entity.POST_BR;
                $scope.mestoZoom.idMst = row.entity.ID_MESTO;
                $http.get('http://localhost:60233/api/DRZAVA/' + $scope.mestoZoom.idDrzaveMst).then(function (response) {
                    var resp = response.data;
                    $scope.mestoZoom.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.mestoZoom.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if ($scope.activeZoomTable == "POPISNA_KOMISIJA") {
                $scope.popKomisijaZoom.idPreduzecaPK = row.entity.ID_PREDUZECA;
                $scope.popKomisijaZoom.idSektoraPK = row.entity.ID_SEKTORA;
                $scope.popKomisijaZoom.idMagPK = row.entity.ID_MAGACINA;
                $scope.popKomisijaZoom.idGodPK = row.entity.ID_GODINE;
                $scope.popKomisijaZoom.idPopDokPK = row.entity.ID_POP__DOK__;
                $scope.popKomisijaZoom.idRadnikPK = row.entity.ID_RADNIKA;
                $scope.popKomisijaZoom.funkcijaPK = row.entity.FUNKCIJA_U_KOMISIJI;
                $scope.popKomisijaZoom.idPopK = row.entity.ID__POP__KOMISIJE;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.popKomisijaZoom.idPreduzecaPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisijaZoom.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.popKomisijaZoom.idSektoraPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisijaZoom.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.popKomisijaZoom.idMagPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisijaZoom.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.popKomisijaZoom.idGodPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisijaZoom.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/RADNIK/' + $scope.popKomisijaZoom.idRadnikPK).then(function (response) {
                    var resp = response.data;
                    $scope.popKomisijaZoom.radnik = resp.IME_RADNIKA + " " + resp.PREZIME_RADNIKA;
                });

            } else if ($scope.activeZoomTable == "POPISNI_DOKUMENT") {
                $scope.popDokumentZoom.idPredPDk = row.entity.ID_PREDUZECA;
                $scope.popDokumentZoom.idSektPDk = row.entity.ID_SEKTORA;
                $scope.popDokumentZoom.idMagPDk = row.entity.ID_MAGACINA;
                $scope.popDokumentZoom.idGodPDk = row.entity.ID_GODINE;
                $scope.popDokumentZoom.brPopPDk = row.entity.BROJ_POPISA;
                $scope.popDokumentZoom.datumPopPDk = row.entity.DATUM_POPISA;
                $scope.popDokumentZoom.idPDk = row.entity.ID_POP__DOK__;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.popDokumentZoom.idPredPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokumentZoom.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.popDokumentZoom.idSektPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokumentZoom.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.popDokumentZoom.idMagPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokumentZoom.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.popDokumentZoom.idGodPDk).then(function (response) {
                    var resp = response.data;
                    $scope.popDokumentZoom.nazivGod = resp.GODINA;
                });
            } else if ($scope.activeZoomTable == "POSLOVNA_GODINA") {
                $scope.godinaZoom.idPredPgod = row.entity.ID_PREDUZECA;
                $scope.godinaZoom.godPgod = row.entity.GODINA;
                $scope.godinaZoom.zakljPgod = row.entity.ZAKLJUCENA_;
                $scope.godinaZoom.idPgod = row.entity.ID_GODINE;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.godinaZoom.idPredPgod).then(function (response) {
                    var resp = response.data;
                    $scope.godinaZoom.nazivPred = resp.NAZIV_PREDUZECA;
                });

            } else if ($scope.activeZoomTable == "POSLOVNI_PARTNER") {
                $scope.posPartnerZoom.idDrzPP = row.entity.ID_DRZAVA;
                $scope.posPartnerZoom.idMestaPP = row.entity.ID_MESTO;
                $scope.posPartnerZoom.nazivPP = row.entity.NAZIV_PARTNERA;
                $scope.posPartnerZoom.tipPP = row.entity.TIP_PARTNERA;
                $scope.posPartnerZoom.pibPP = row.entity.PIB_PARTNERA;
                $scope.posPartnerZoom.adresaPP = row.entity.ADRESA_PARTNERA;
                $scope.posPartnerZoom.telefonPP = row.entity.TELEFON_PARTNERA;
                $scope.posPartnerZoom.idPP = row.entity.ID_PARTNERA;

                $http.get('http://localhost:60233/api/MESTO/' + $scope.posPartnerZoom.idMestaPP).then(function (response) {
                    var resp = response.data;
                    $scope.posPartnerZoom.nazivMst = resp.NAZIV_MESTA;
                    $scope.posPartner.oznakaMst = resp.OZNAKA_MESTA;

                });

                $http.get('http://localhost:60233/api/MESTOFrom/' + $scope.posPartnerZoom.idMestaPP).then(function (response) {
                    var resp = response.data;
                    $scope.posPartnerZoom.idDrzP = resp.ID_DRZAVA;
                    $scope.posPartnerZoom.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.posPartnerZoom.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if ($scope.activeZoomTable == "PREDUZECE") {
                $scope.preduzeceZoom.idMestoP = row.entity.ID_MESTO;
                $scope.preduzeceZoom.nazivP = row.entity.NAZIV_PREDUZECA;
                $scope.preduzeceZoom.pibP = row.entity.PIB_PREDUZECA;
                $scope.preduzeceZoom.adresaP = row.entity.ADRESA_PREDUZECA;
                $scope.preduzeceZoom.telefonP = row.entity.TELEFON_PREDUZECA;
                $scope.preduzeceZoom.idPred = row.entity.ID_PREDUZECA;
                $http.get('http://localhost:60233/api/MESTO/' + $scope.preduzeceZoom.idMestoP).then(function (response) {
                    var resp = response.data;
                    $scope.preduzeceZoom.nazivMst = resp.NAZIV_MESTA;
                    $scope.preduzeceZoom.oznakaMst = resp.OZNAKA_MESTA;

                });

                $http.get('http://localhost:60233/api/MESTOFrom/' + $scope.preduzeceZoom.idMestoP).then(function (response) {
                    var resp = response.data;
                    $scope.preduzeceZoom.idDrzP = resp.ID_DRZAVA;
                    $scope.preduzeceZoom.nazivDrz = resp.NAZIV_DRZAVA;
                    $scope.preduzeceZoom.oznakaDrz = resp.OZNAKA_DRZAVA;

                });
            } else if ($scope.activeZoomTable == "PROMETNI_DOKUMENT") {
                $scope.promDokumentZoom.idGodPdok = row.entity.ID_GODINE;
                $scope.promDokumentZoom.idPredPdok = row.entity.ID_PREDUZECA;
                $scope.promDokumentZoom.idSekPdok = row.entity.ID_SEKTORA;
                $scope.promDokumentZoom.idMagPdok = row.entity.ID_MAGACINA;
                $scope.promDokumentZoom.mIdpPdok = row.entity.MAG_ID_PREDUZECA;
                $scope.promDokumentZoom.mIdsPdok = row.entity.MAG_ID_SEKTORA;
                $scope.promDokumentZoom.mIdmPdok = row.entity.MAG_ID_MAGACINA;
                $scope.promDokumentZoom.idPartPdok = row.entity.ID_PARTNERA;
                $scope.promDokumentZoom.brDokPdok = row.entity.BROJ;
                $scope.promDokumentZoom.statusPdok = row.entity.STATUS;
                $scope.promDokumentZoom.datumNpdok = row.entity.DATUM_NASTANKA;
                $scope.promDokumentZoom.datumKPdok = row.entity.DATUM_KNJIZENJA;
                $scope.promDokumentZoom.vrstaPdok = row.entity.VRSTA_PROMETNOG_DOKUMENTA;
                $scope.promDokumentZoom.idPdok = row.entity.ID_PROM__DOK_;


                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.promDokumentZoom.idPredPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokumentZoom.nazivPred = resp.NAZIV_PREDUZECA;

                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.promDokumentZoom.idSekPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokumentZoom.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.promDokumentZoom.idMagPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokumentZoom.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.promDokumentZoom.idGodPdok).then(function (response) {
                    var resp = response.data;
                    $scope.promDokumentZoom.nazivGod = resp.GODINA;
                });


            } else if ($scope.activeZoomTable == "RADNIK") {
                $scope.radnikZoom.idPredR = row.entity.ID_PREDUZECA;
                $scope.radnikZoom.imeR = row.entity.IME_RADNIKA;
                $scope.radnikZoom.prezimeR = row.entity.PREZIME_RADNIKA;
                $scope.radnikZoom.idRd = row.entity.ID_RADNIKA;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.radnikZoom.idPredR).then(function (response) {
                    var resp = response.data;
                    $scope.radnikZoom.nazivPred = resp.NAZIV_PREDUZECA;
                });
            } else if ($scope.activeZoomTable == "SEKTOR") {
                $scope.sektorZoom.idPredS = row.entity.ID_PREDUZECA;
                $scope.sektorZoom.nazivS = row.entity.NAZIV_SEKTORA;
                $scope.sektorZoom.idS = row.entity.ID_SEKTORA;
                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.sektorZoom.idPredS).then(function (response) {
                    var resp = response.data;
                    $scope.sektorZoom.nazivPred = resp.NAZIV_PREDUZECA;
                });
            } else if ($scope.activeZoomTable == "SIFRARNIK_ARTIKALA") {
                $scope.sifrarnikZoom.idGSfr = row.entity.ID_GRUPE;
                $scope.sifrarnikZoom.idMJSfr = row.entity.ID_JEDINICE;
                $scope.sifrarnikZoom.nazivSfr = row.entity.NAZIV_ARTIKAL;
                $scope.sifrarnikZoom.idSfr = row.entity.ID_ARTIKAL;
                $http.get('http://localhost:60233/api/GRUPA_ARTIKALA/' + $scope.sifrarnikZoom.idGSfr).then(function (response) {
                    var resp = response.data;
                    $scope.sifrarnikZoom.nazivGrupe = resp.NAZIV_GRUPE;
                });
                $http.get('http://localhost:60233/api/JEDINICA_MERE/' + $scope.sifrarnikZoom.idMJSfr).then(function (response) {
                    var resp = response.data;
                    $scope.sifrarnikZoom.nazivJedinice = resp.NAZIV_JEDINICE;
                });
            } else if ($scope.activeZoomTable == "STAVKA_PROMETNOG_DOKUMENTA") {
                $scope.stavkaPromZoom.idGodSP = row.entity.ID_GODINE;
                $scope.stavkaPromZoom.idPDSP = row.entity.ID_PROM__DOK_;
                $scope.stavkaPromZoom.idGSP = row.entity.ID_GRUPE;
                $scope.stavkaPromZoom.idASP = row.entity.ID_ARTIKAL;
                $scope.stavkaPromZoom.rbrSP = row.entity.REDNI_BROJ_STAV__PROM__DOK;
                $scope.stavkaPromZoom.kolicinaSP = row.entity.KOLICINA_STAV__PROM__DOK;
                $scope.stavkaPromZoom.cenaSP = row.entity.CENA_STAVKA_PROM__DOK_;
                $scope.stavkaPromZoom.vrednostSP = row.entity.VREDNOST_STAV__PROM__DOK;
                $scope.stavkaPromZoom.idstavkePD = row.entity.ID_STAV__PROM__DOK_;

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.stavkaPromZoom.idGodSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPromZoom.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/SIFRARNIK_ARTIKALA/' + $scope.stavkaPromZoom.idASP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPromZoom.nazivArtikal = resp.NAZIV_ARTIKLA;
                });

                $http.get('http://localhost:60233/api/GRUPA_ARTIKALA/' + $scope.stavkaPromZoom.idGSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPromZoom.nazivGrupe = resp.NAZIV_GRUPE;
                });

            } else if ($scope.activeZoomTable == "STAVKE_POPISA") {
                $scope.stavkaPopZoom.idPredSP = row.entity.ID_PREDUZECA;
                $scope.stavkaPopZoom.idSekSP = row.entity.ID_SEKTORA;
                $scope.stavkaPopZoom.idMagSP = row.entity.ID_MAGACINA;
                $scope.stavkaPopZoom.idGSP = row.entity.ID_GODINE;
                $scope.stavkaPopZoom.idPopDSP = row.entity.ID_POP__DOK__;
                $scope.stavkaPopZoom.idArtSP = row.entity.ID_ARTIKAL;
                $scope.stavkaPopZoom.kolKSP = row.entity.KOLICINA_U_KARTICI;
                $scope.stavkaPopZoom.kolPoPsP = row.entity.KOLICINA_PO_POPISU;
                $scope.stavkaPopZoom.idStvP = row.entity.ID_STAVKE_POPISA;

                $http.get('http://localhost:60233/api/PREDUZECE/' + $scope.stavkaPopZoom.idPredSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPopZoom.nazivPred = resp.NAZIV_PREDUZECA;
                });

                $http.get('http://localhost:60233/api/POSLOVNA_GODINA/' + $scope.stavkaPopZoom.idGSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPopZoom.nazivGod = resp.GODINA;
                });

                $http.get('http://localhost:60233/api/SEKTOR/' + $scope.stavkaPopZoom.idSekSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPopZoom.nazivSekt = resp.NAZIV_SEKTORA;
                });

                $http.get('http://localhost:60233/api/MAGACIN/' + $scope.stavkaPopZoom.idMagSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPopZoom.nazivMag = resp.NAZIV_MAGACINA;
                });

                $http.get('http://localhost:60233/api/SIFRARNIK_ARTIKALA/' + $scope.stavkaPopZoom.idArtSP).then(function (response) {
                    var resp = response.data;
                    $scope.stavkaPopZoom.nazivArtikal = resp.NAZIV_ARTIKLA;
                });
            }
        });
    });

    $scope.nesto = function () {
        console.log("usao u nesto");

        if ($scope.activeZoomTable == "DRZAVA" && $scope.activeTable == "MESTO") {
            $scope.mesto.idDrzaveMst = $scope.drzavaZoom.idDrzDr;
            $scope.mesto.oznakaDrz = $scope.drzavaZoom.oznkaDr;
            $scope.mesto.nazivDrz = $scope.drzavaZoom.nazivDr;
        } else if ($scope.activeZoomTable == "MAGACINSKA_KARTICA" && $scope.activeTable == "ANALITIKA_MAGACINSKE_KARTICE") {
            $scope.analMag.ifMag = $scope.magKartZoom.idMGMK;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "MAGACIN") {
            $scope.magacin.idPredMag = $scope.preduzeceZoom.idPred;
            $scope.magacin.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "SEKTOR" && $scope.activeTable == "MAGACIN") {
            $scope.magacin.idSektMag = $scope.sektorZoom.idS;
            $scope.magacin.nazivSekt = $scope.sektorZoom.nazivS;
            $scope.magacin.idPredMag = $scope.sektorZoom.idPredS;
            $scope.magacin.nazivPred = $scope.sektorZoom.nazivPred;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idPreduzecaPK = $scope.preduzeceZoom.idPred;
            $scope.popKomisija.nazivPred = $scope.preduzeceZoom.nazivP;

        } else if ($scope.activeZoomTable == "SEKTOR" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idSektoraPK = $scope.sektorZoom.idS;
            $scope.popKomisija.nazivSekt = $scope.sektorZoom.nazivS;
            $scope.popKomisija.idPreduzecaPK = $scope.sektorZoom.idPredS;
            $scope.popKomisija.nazivPred = $scope.sektorZoom.nazivPred;

        } else if ($scope.activeZoomTable == "MAGACIN" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idMagPK = $scope.magacinZoom.idMagMag;
            $scope.popKomisija.nazivMag = $scope.magacinZoom.nazivMag;
            $scope.popKomisija.idSektoraPK = $scope.magacinZoom.idSektMag;
            $scope.popKomisija.nazivSekt = $scope.magacinZoom.nazivSekt;
            $scope.popKomisija.idPreduzecaPK = $scope.magacinZoom.idPredMag;
            $scope.popKomisija.nazivPred = $scope.magacinZoom.nazivPred;
        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idGodPK = $scope.godinaZoom.idPgod;
            $scope.popKomisija.nazivGod = $scope.godinaZoom.godPgod;
            $scope.popKomisija.idPreduzecaPK = $scope.godinaZoom.idPredPgod;
            $scope.popKomisija.nazivPred = $scope.godinaZoom.nazivPred;
        } else if ($scope.activeZoomTable == "RADNIK" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idRadnikPK = $scope.radnikZoom.idRd;
            $scope.popKomisija.radnik = $scope.radnikZoom.imeR + " " + $scope.radnikZoom.prezimeR;
            $scope.popKomisija.idPreduzecaPK = $scope.radnikZoom.idPredR;
            $scope.popKomisija.nazivPred = $scope.radnikZoom.nazivPred;
        } else if ($scope.activeZoomTable == "POPISNI_DOKUMENT" && $scope.activeTable == "POPISNA_KOMISIJA") {
            $scope.popKomisija.idPopDokPK = $scope.popDokumentZoom.idPDk;
            $scope.popKomisija.idPreduzecaPK = $scope.popDokumentZoom.idPredPDk;
            $scope.popKomisija.nazivPred = $scope.popDokumentZoom.nazivPred;
            $scope.popKomisija.idMagPK = $scope.popDokumentZoom.idMagPDk;
            $scope.popKomisija.nazivMag = $scope.popDokumentZoom.nazivMag;
            $scope.popKomisija.idSektoraPK = $scope.popDokumentZoom.idSektPDk;
            $scope.popKomisija.nazivSekt = $scope.popDokumentZoom.nazivSekt;

        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "POPISNI_DOKUMENT") {
            $scope.popDokument.idPreduzecaPDk = $scope.preduzeceZoom.idPred;
            $scope.popDokument.nazivPred = $scope.preduzeceZoom.nazivP;

        } else if ($scope.activeZoomTable == "SEKTOR" && $scope.activeTable == "POPISNI_DOKUMENT") {
            $scope.popDokument.idSektPDk = $scope.sektorZoom.idS;
            $scope.popDokument.nazivSekt = $scope.sektorZoom.nazivS;
            $scope.popDokument.idPredPDk = $scope.sektorZoom.idPredS;
            $scope.popDokument.nazivPred = $scope.sektorZoom.nazivPred;

        } else if ($scope.activeZoomTable == "MAGACIN" && $scope.activeTable == "POPISNI_DOKUMENT") {
            $scope.popDokument.idMagPDk = $scope.magacinZoom.idMagMag;
            $scope.popDokument.nazivMag = $scope.magacinZoom.nazivMag;
            $scope.popDokument.idSektPDk = $scope.magacinZoom.idSektMag;
            $scope.popDokument.nazivSekt = $scope.magacinZoom.nazivSekt;
            $scope.popDokument.idPredPDk = $scope.magacinZoom.idPredMag;
            $scope.popDokument.nazivPred = $scope.magacinZoom.nazivPred;

        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "POPISNI_DOKUMENT") {
            $scope.popDokument.idGodPDk = $scope.godinaZoom.idPgod;
            $scope.popDokument.nazivGod = $scope.godinaZoom.godPgod;
            $scope.popDokument.idPredPDk = $scope.godinaZoom.idPredPgod;
            $scope.popDokument.nazivPred = $scope.godinaZoom.nazivPred;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "POSLOVNA_GODINA") {
            $scope.godina.idPredGod = $scope.preduzeceZoom.idPred;
            $scope.godina.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "DRZAVA" && $scope.activeTable == "POSLOVNI_PARTNER") {
            $scope.posPartner.idDrzP = $scope.drzavaZoom.idDrzDr;
            $scope.posPartner.oznakaDrz = $scope.drzavaZoom.oznkaDr;
            $scope.posPartner.nazivDrz = $scope.drzavaZoom.nazivDr;
        } else if ($scope.activeZoomTable = "MESTO" && $scope.activeTable == "POSLOVNI_PARTNER") {
            $scope.posPartner.idMestoP = $scope.mestoZoom.idMst;
            $scope.posPartner.oznakaMst = $scope.mestoZoom.oznakaMst;
            $scope.posPartner.nazivMst = $scope.mestoZoom.nazivMst;
        } else if ($scope.activeZoomTable == "DRZAVA" && $scope.activeTable == "PREDUZECE") {
            $scope.preduzece.idDrzP = $scope.drzavaZoom.idDrzDr;
            $scope.preduzece.oznakaDrz = $scope.drzavaZoom.oznkaDr;
            $scope.preduzece.nazivDrz = $scope.drzavaZoom.nazivDr;
        } else if ($scope.activeZoomTable = "MESTO" && $scope.activeTable == "PREDUZECE") {
            $scope.preduzece.idMestoP = $scope.mestoZoom.idMst;
            $scope.preduzece.oznakaMst = $scope.mestoZoom.oznakaMst;
            $scope.preduzece.nazivMst = $scope.mestoZoom.nazivMst;
            $scope.preduzece.idDrzP = $scope.mestoZoom.idDrzaveMst;
            $scope.preduzece.oznakaDrz = $scope.mestoZoom.oznakaDrz;
            $scope.preduzece.nazivDrz = $scope.mestoZoom.nazivDrz;
        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "PROMETNI_DOKUMENT") {
            $scope.promDokument.idGodPdok = $scope.godinaZoom.idPgod;
            $scope.promDokument.nazivGod = $scope.godinaZoom.godPgod;
            $scope.promDokument.idPredPdok = $scope.godinaZoom.idPredPgod;
            $scope.promDokument.nazivPred = $scope.godinaZoom.nazivPred;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "PROMETNI_DOKUMENT") {
            $scope.promDokument.idPredPdok = $scope.preduzeceZoom.idPred;
            $scope.promDokument.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "SEKTOR" && $scope.activeTable == "PROMETNI_DOKUMENT") {
            $scope.promDokument.idSekPdok = $scope.sektorZoom.idS;
            $scope.promDokument.nazivSekt = $scope.sektorZoom.nazivS;
            $scope.promDokument.idPredPdok = $scope.sektorZoom.idPredS;
            $scope.promDokument.nazivPred = $scope.sektorZoom.nazivPred;
        } else if ($scope.activeZoomTable == "MAGACIN" && $scope.activeTable == "PROMETNI_DOKUMENT") {
            $scope.promDokument.idMagPdok = $scope.magacinZoom.idMagMag;
            $scope.promDokument.nazivMag = $scope.magacinZoom.nazivMag;
            $scope.promDokument.idSektPdok = $scope.magacinZoom.idSektMag;
            $scope.promDokument.nazivSekt = $scope.magacinZoom.nazivSekt;
            $scope.promDokument.idPredPdok = $scope.magacinZoom.idPredMag;
            $scope.promDokument.nazivPred = $scope.magacinZoom.nazivPred;

        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "RADNIK") {
            $scope.radnik.idPredR = $scope.preduzeceZoom.idPred;
            $scope.radnik.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "SEKTOR") {
            $scope.sektor.idPredS = $scope.preduzeceZoom.idPred;
            $scope.sektor.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "GRUPA_ARTIKALA" && $scope.activeTable == "SIFRARNIK_ARTIKALA") {
            $scope.sifrarnik.idGSfr = $scope.grupaZoom.idGrGa;
            $scope.sifrarnik.nazivGrupe = $scope.grupaZoom.nazivGA;

        } else if ($scope.activeZoomTable == "JEDINICA_MERE" && $scope.activeTable == "SIFRARNIK_ARTIKALA") {
            $scope.sifrarnik.idMJSfr = $scope.jedinicaZoom.idMJ;
            $scope.sifrarnik.nazivJedinice = $scope.jedinicaZoom.jedMere;
        } else if ($scope.activeZoomTable == "PREDUZECE" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idPredSP = $scope.preduzeceZoom.idPred;
            $scope.stavkaPop.nazivPred = $scope.preduzeceZoom.nazivP;
        } else if ($scope.activeZoomTable == "SEKTOR" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idSekSP = $scope.sektorZoom.idS;
            $scope.stavkaPop.nazivSekt = $scope.sektorZoom.nazivS;

        } else if ($scope.activeZoomTable == "MAGACIN" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idMagSP = $scope.magacinZoom.idMagMag;
            $scope.stavkaPop.nazivMag = $scope.magacinZoom.nazivMag;
        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idGSP = $scope.godinaZoom.idPgod;
            $scope.stavkaPop.nazivGod = $scope.godinaZoom.godPgod;
        } else if ($scope.activeZoomTable == "POPISNI_DOKUMENT" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idPopDSP = $scope.popDokument.idPDk;
        } else if ($scope.activeZoomTable == "SIFRANIK_ARTIKALA" && $scope.activeTable == "STAVKE_POPISA") {
            $scope.stavkaPop.idArtSP = $scope.sifrarnikZoom.idSfr;
            $scope.stavkaPop.nazivArtikal = $scope.sifrarnikZoom.nazivSfr;
        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "STAVKA_PROMETNOG_DOKUMENTA") {
            $scope.stavkaProm.idGodSP = $scope.godinaZoom.idPgod;
            $scope.stavkaProm.nazivGod = $scope.godinaZoom.godPgod;

        } else if ($scope.activeZoomeTable = "PROMETNI_DOKUMENT" && $scope.activeTable == "STAVKA_PROMETNOG_DOKUMENTA") {
            $scope.stavkaProm.idPDSP = $scope.promDokumentZoom.idPdok;
            $scope.stavkaProm.idGodSP = $scope.promDokumentZoom.idGodPdok;
            $scope.stavkaProm.nazivGod = $scope.promDokumentZoom.nazivGod;
        } else if ($scope.activeZoomTable == "GRUPA_ARTIKALA" && $scope.activeTable == "STAVKA_PROMETNOG_DOKUMENTA") {
            $scope.stavkaProm.idGSP = $scope.grupaZoom.idGrGa;
            $scope.stavkaProm.nazivGrupe = $scope.grupaZoom.nazivGA;

        } else if ($scope.activeZoomTable == "SIFRANIK_ARTIKALA" && $scope.activeTable == "STAVKA_PROMETNOG_DOKUMENTA") {
            $scope.stavkaProm.idASP = $scope.sifrarnikZoom.idSfr;
            $scope.stavkaProm.nazivArtikal = $scope.sifrarnikZoom.nazivSfr;
            $scope.stavkaProm.idGSP = $scope.sifrarnikZoom.idGSfr;
            $scope.stavkaProm.nazivGrupe = $scope.sifrarnikZoom.nazivGrupe;
        } else if ($scope.activeZoomTable == "POSLOVNA_GODINA" && $scope.activeTable == "MAGACINSKA_KARTICA") {
            $scope.magKart.idGMK = $scope.godinaZoom.idPgod;
            $scope.magKart.nazivGod = $scope.godinaZoom.godPgod;;
        } else if ($scope.activeZoomTable == "SIFRARNIK_ARTIKALA" && $scope.activeTable == "MAGACINSKA_KARTICA") {
            $scope.magKart.idAMK = $scope.sifrarnikZoom.idSfr;
            $scope.magKart.nazivArtikal = $scope.sifrarnikZoom.nazivSfr;

        } else if ($scope.activeZoomTable == "MAGACIN" && $scope.activeTable == "MAGACINSKA_KARTICA") {
            $scope.magKart.idMMK = $scope.magacinZoom.idMagMag;
            $scope.magKart.nazivMag = $scope.magacinZoom.nazivMag;
        };

        $uibModalInstance.close();
    };


    $scope.ok = function () {

        $uibModalInstance.close();
        $scope.isZoom = false;
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isZoom = false;
    };

});



myApp.controller('ModalInstanceReportCtrl', function ($http, $scope, $uibModalInstance, name) {
    $scope.izvestajLagerLista = function () {
        $http.post('http://localhost:60233/api/pdf/LagerLista/' + $scope.godina + '/' + $scope.magacin, { responseType: 'arraybuffer' })
            .then(function (response) {
                var file = new Blob([response.data], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            });
    };

    $scope.izvestajAnalitika = function () {
         $http.post('http://localhost:60233/api/pdf/Analitika/' + $scope.magKart, { responseType: 'arraybuffer' })
            .then(function (response) {
                var file = new Blob([response.data], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});