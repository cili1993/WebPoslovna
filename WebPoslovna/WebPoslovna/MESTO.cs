namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MESTO")]
    public partial class MESTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MESTO()
        {
            POSLOVNI_PARTNER = new HashSet<POSLOVNI_PARTNER>();
            PREDUZECE = new HashSet<PREDUZECE>();
        }

        [Key]
        public int ID_MESTO { get; set; }

        public int ID_DRZAVA { get; set; }

        [Required]
        [StringLength(2)]
        public string OZNAKA_MESTA { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_MESTA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? POST_BR { get; set; }

        public virtual DRZAVA DRZAVA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POSLOVNI_PARTNER> POSLOVNI_PARTNER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PREDUZECE> PREDUZECE { get; set; }
    }
}
