namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class POSLOVNA_GODINA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public POSLOVNA_GODINA()
        {
            MAGACINSKA_KARTICA = new HashSet<MAGACINSKA_KARTICA>();
            POPISNI_DOKUMENT = new HashSet<POPISNI_DOKUMENT>();
            PROMETNI_DOKUMENT = new HashSet<PROMETNI_DOKUMENT>();
        }

        [Key]
        public int ID_GODINE { get; set; }

        public int ID_PREDUZECA { get; set; }

        [Required]
        [StringLength(4)]
        public string GODINA { get; set; }

        public bool? ZAKLJUCENA_ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAGACINSKA_KARTICA> MAGACINSKA_KARTICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POPISNI_DOKUMENT> POPISNI_DOKUMENT { get; set; }

        public virtual PREDUZECE PREDUZECE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROMETNI_DOKUMENT> PROMETNI_DOKUMENT { get; set; }
    }
}
