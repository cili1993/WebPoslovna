namespace WebPoslovna
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<ANALITIKA_MAGACINSKE_KARTICE> ANALITIKA_MAGACINSKE_KARTICE { get; set; }
        public virtual DbSet<DRZAVA> DRZAVA { get; set; }
        public virtual DbSet<GRUPA_ARTIKALA> GRUPA_ARTIKALA { get; set; }
        public virtual DbSet<JEDINICA_MERE> JEDINICA_MERE { get; set; }
        public virtual DbSet<MAGACIN> MAGACIN { get; set; }
        public virtual DbSet<MAGACINSKA_KARTICA> MAGACINSKA_KARTICA { get; set; }
        public virtual DbSet<MESTO> MESTO { get; set; }
        public virtual DbSet<POPISNA_KOMISIJA> POPISNA_KOMISIJA { get; set; }
        public virtual DbSet<POPISNI_DOKUMENT> POPISNI_DOKUMENT { get; set; }
        public virtual DbSet<POSLOVNA_GODINA> POSLOVNA_GODINA { get; set; }
        public virtual DbSet<POSLOVNI_PARTNER> POSLOVNI_PARTNER { get; set; }
        public virtual DbSet<PREDUZECE> PREDUZECE { get; set; }
        public virtual DbSet<PROMETNI_DOKUMENT> PROMETNI_DOKUMENT { get; set; }
        public virtual DbSet<RADNIK> RADNIK { get; set; }
        public virtual DbSet<SEKTOR> SEKTOR { get; set; }
        public virtual DbSet<SIFRARNIK_ARTIKALA> SIFRARNIK_ARTIKALA { get; set; }
        public virtual DbSet<STAVKA_PROMETNOG_DOKUMENTA> STAVKA_PROMETNOG_DOKUMENTA { get; set; }
        public virtual DbSet<STAVKE_POPISA> STAVKE_POPISA { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ANALITIKA_MAGACINSKE_KARTICE>()
                .Property(e => e.REDNI_BROJ_MAG__ANAL_)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ANALITIKA_MAGACINSKE_KARTICE>()
                .Property(e => e.DATUM_PROMENE)
                .IsUnicode(false);

            modelBuilder.Entity<ANALITIKA_MAGACINSKE_KARTICE>()
                .Property(e => e.VRSTA_PROMETA)
                .IsUnicode(false);

            modelBuilder.Entity<ANALITIKA_MAGACINSKE_KARTICE>()
                .Property(e => e.SIFRA_DOKUMENTA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ANALITIKA_MAGACINSKE_KARTICE>()
                .Property(e => e.SMER)
                .IsUnicode(false);

            modelBuilder.Entity<DRZAVA>()
                .Property(e => e.OZNAKA_DRZAVA)
                .IsUnicode(false);

            modelBuilder.Entity<DRZAVA>()
                .Property(e => e.NAZIV_DRZAVA)
                .IsUnicode(false);

            modelBuilder.Entity<DRZAVA>()
                .HasMany(e => e.MESTO)
                .WithRequired(e => e.DRZAVA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GRUPA_ARTIKALA>()
                .Property(e => e.NAZIV_GRUPE)
                .IsUnicode(false);

            modelBuilder.Entity<GRUPA_ARTIKALA>()
                .HasMany(e => e.SIFRARNIK_ARTIKALA)
                .WithRequired(e => e.GRUPA_ARTIKALA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JEDINICA_MERE>()
                .Property(e => e.NAZIV_JEDINICE)
                .IsUnicode(false);

            modelBuilder.Entity<JEDINICA_MERE>()
                .HasMany(e => e.SIFRARNIK_ARTIKALA)
                .WithRequired(e => e.JEDINICA_MERE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MAGACIN>()
                .Property(e => e.NAZIV_MAGACINA)
                .IsUnicode(false);

            modelBuilder.Entity<MAGACIN>()
                .HasMany(e => e.MAGACINSKA_KARTICA)
                .WithRequired(e => e.MAGACIN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MAGACIN>()
                .HasMany(e => e.POPISNI_DOKUMENT)
                .WithRequired(e => e.MAGACIN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MAGACIN>()
                .HasMany(e => e.PROMETNI_DOKUMENT)
                .WithRequired(e => e.MAGACIN)
                .HasForeignKey(e => e.ID_MAGACINA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MAGACIN>()
                .HasMany(e => e.PROMETNI_DOKUMENT1)
                .WithOptional(e => e.MAGACIN1)
                .HasForeignKey(e => e.MAG_ID_MAGACINA);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .Property(e => e.PROSECNA_CENA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .Property(e => e.VREDNOST_ULAZA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .Property(e => e.VREDNOST_IZLAZA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .Property(e => e.POCETNO_STANJE_VREDNOST_MAG__KAR)
                .HasPrecision(18, 0);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .Property(e => e.UK_VR__MAG__KAR)
                .HasPrecision(18, 0);

            modelBuilder.Entity<MAGACINSKA_KARTICA>()
                .HasMany(e => e.ANALITIKA_MAGACINSKE_KARTICE)
                .WithRequired(e => e.MAGACINSKA_KARTICA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MESTO>()
                .Property(e => e.OZNAKA_MESTA)
                .IsUnicode(false);

            modelBuilder.Entity<MESTO>()
                .Property(e => e.NAZIV_MESTA)
                .IsUnicode(false);

            modelBuilder.Entity<MESTO>()
                .Property(e => e.POST_BR)
                .HasPrecision(18, 0);

            modelBuilder.Entity<POPISNA_KOMISIJA>()
                .Property(e => e.FUNKCIJA_U_KOMISIJI)
                .IsUnicode(false);

            modelBuilder.Entity<POPISNI_DOKUMENT>()
                .Property(e => e.BROJ_POPISA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<POPISNI_DOKUMENT>()
                .Property(e => e.DATUM_POPISA)
                .IsUnicode(false);

            modelBuilder.Entity<POPISNI_DOKUMENT>()
                .HasMany(e => e.POPISNA_KOMISIJA)
                .WithRequired(e => e.POPISNI_DOKUMENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POPISNI_DOKUMENT>()
                .HasMany(e => e.STAVKE_POPISA)
                .WithRequired(e => e.POPISNI_DOKUMENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POSLOVNA_GODINA>()
                .Property(e => e.GODINA)
                .IsUnicode(false);

            modelBuilder.Entity<POSLOVNA_GODINA>()
                .HasMany(e => e.MAGACINSKA_KARTICA)
                .WithRequired(e => e.POSLOVNA_GODINA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POSLOVNA_GODINA>()
                .HasMany(e => e.POPISNI_DOKUMENT)
                .WithRequired(e => e.POSLOVNA_GODINA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POSLOVNA_GODINA>()
                .HasMany(e => e.PROMETNI_DOKUMENT)
                .WithRequired(e => e.POSLOVNA_GODINA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POSLOVNI_PARTNER>()
                .Property(e => e.TIP_PARTNERA)
                .IsUnicode(false);

            modelBuilder.Entity<POSLOVNI_PARTNER>()
                .Property(e => e.NAZIV_PARTNERA)
                .IsUnicode(false);

            modelBuilder.Entity<POSLOVNI_PARTNER>()
                .Property(e => e.ADRESA_PARTNERA)
                .IsUnicode(false);

            modelBuilder.Entity<POSLOVNI_PARTNER>()
                .Property(e => e.TELEFON_PARTNERA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<PREDUZECE>()
                .Property(e => e.NAZIV_PREDUZECA)
                .IsUnicode(false);

            modelBuilder.Entity<PREDUZECE>()
                .Property(e => e.PIB_PREDUZECA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<PREDUZECE>()
                .Property(e => e.ADRESA_PREDUZECA)
                .IsUnicode(false);

            modelBuilder.Entity<PREDUZECE>()
                .Property(e => e.TELEFON_PREDUZECA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<PREDUZECE>()
                .HasMany(e => e.POSLOVNA_GODINA)
                .WithRequired(e => e.PREDUZECE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PREDUZECE>()
                .HasMany(e => e.RADNIK)
                .WithRequired(e => e.PREDUZECE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PREDUZECE>()
                .HasMany(e => e.SEKTOR)
                .WithRequired(e => e.PREDUZECE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PROMETNI_DOKUMENT>()
                .Property(e => e.DATUM_NASTANKA)
                .IsUnicode(false);

            modelBuilder.Entity<PROMETNI_DOKUMENT>()
                .Property(e => e.DATUM_KNJIZENJA)
                .IsUnicode(false);

            modelBuilder.Entity<PROMETNI_DOKUMENT>()
                .Property(e => e.STATUS)
                .IsUnicode(false);

            modelBuilder.Entity<PROMETNI_DOKUMENT>()
                .Property(e => e.VRSTA_PROMETNOG_DOKUMENTA)
                .IsUnicode(false);

            modelBuilder.Entity<PROMETNI_DOKUMENT>()
                .HasMany(e => e.STAVKA_PROMETNOG_DOKUMENTA)
                .WithRequired(e => e.PROMETNI_DOKUMENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RADNIK>()
                .Property(e => e.IME_RADNIKA)
                .IsUnicode(false);

            modelBuilder.Entity<RADNIK>()
                .Property(e => e.PREZIME_RADNIKA)
                .IsUnicode(false);

            modelBuilder.Entity<RADNIK>()
                .HasMany(e => e.POPISNA_KOMISIJA)
                .WithRequired(e => e.RADNIK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SEKTOR>()
                .Property(e => e.NAZIV_SEKTORA)
                .IsUnicode(false);

            modelBuilder.Entity<SEKTOR>()
                .HasMany(e => e.MAGACIN)
                .WithRequired(e => e.SEKTOR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIFRARNIK_ARTIKALA>()
                .Property(e => e.NAZIV_ARTIKAL)
                .IsUnicode(false);

            modelBuilder.Entity<SIFRARNIK_ARTIKALA>()
                .HasMany(e => e.MAGACINSKA_KARTICA)
                .WithRequired(e => e.SIFRARNIK_ARTIKALA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIFRARNIK_ARTIKALA>()
                .HasMany(e => e.STAVKA_PROMETNOG_DOKUMENTA)
                .WithRequired(e => e.SIFRARNIK_ARTIKALA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIFRARNIK_ARTIKALA>()
                .HasMany(e => e.STAVKE_POPISA)
                .WithRequired(e => e.SIFRARNIK_ARTIKALA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<STAVKA_PROMETNOG_DOKUMENTA>()
                .Property(e => e.REDNI_BROJ_STAV__PROM__DOK)
                .HasPrecision(18, 0);
        }
    }
}
