namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PREDUZECE")]
    public partial class PREDUZECE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PREDUZECE()
        {
            POSLOVNA_GODINA = new HashSet<POSLOVNA_GODINA>();
            RADNIK = new HashSet<RADNIK>();
            SEKTOR = new HashSet<SEKTOR>();
        }

        [Key]
        public int ID_PREDUZECA { get; set; }

        public int? ID_MESTO { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_PREDUZECA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PIB_PREDUZECA { get; set; }

        [StringLength(30)]
        public string ADRESA_PREDUZECA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TELEFON_PREDUZECA { get; set; }

        public virtual MESTO MESTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POSLOVNA_GODINA> POSLOVNA_GODINA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RADNIK> RADNIK { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEKTOR> SEKTOR { get; set; }
    }
}
