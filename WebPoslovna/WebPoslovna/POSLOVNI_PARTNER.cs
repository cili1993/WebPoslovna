namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class POSLOVNI_PARTNER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public POSLOVNI_PARTNER()
        {
            PROMETNI_DOKUMENT = new HashSet<PROMETNI_DOKUMENT>();
        }

        [Key]
        public int ID_PARTNERA { get; set; }

        public int? ID_MESTO { get; set; }

        [Required]
        [StringLength(15)]
        public string TIP_PARTNERA { get; set; }

        [Required]
        [StringLength(50)]
        public string NAZIV_PARTNERA { get; set; }

        public int PIB_PARTNERA { get; set; }

        [Required]
        [StringLength(50)]
        public string ADRESA_PARTNERA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TELEFON_PARTNERA { get; set; }

        public virtual MESTO MESTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROMETNI_DOKUMENT> PROMETNI_DOKUMENT { get; set; }
    }
}
