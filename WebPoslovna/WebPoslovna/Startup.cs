﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebPoslovna.Startup))]
namespace WebPoslovna
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
