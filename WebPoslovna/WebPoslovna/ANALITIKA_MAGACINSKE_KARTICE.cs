namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ANALITIKA_MAGACINSKE_KARTICE
    {
        [Key]
        public int ID_ANAL__MAG__KART_ { get; set; }

        public int ID_MAG__KART_ { get; set; }

        [Column(TypeName = "numeric")]
        public decimal REDNI_BROJ_MAG__ANAL_ { get; set; }

        [Required]
        [StringLength(11)]
        public string DATUM_PROMENE { get; set; }

        [StringLength(5)]
        public string VRSTA_PROMETA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SIFRA_DOKUMENTA { get; set; }

        public int? KOLICINA_ANAL__MAG__K_ { get; set; }

        public double? CENA_ANAL__MAG__KART_ { get; set; }

        public double? VREDNOST_ANAL__MAG__KART_ { get; set; }

        [Required]
        [StringLength(3)]
        public string SMER { get; set; }

        public virtual MAGACINSKA_KARTICA MAGACINSKA_KARTICA { get; set; }
    }
}
