namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SEKTOR")]
    public partial class SEKTOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SEKTOR()
        {
            MAGACIN = new HashSet<MAGACIN>();
        }

        [Key]
        public int ID_SEKTORA { get; set; }

        public int ID_PREDUZECA { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_SEKTORA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAGACIN> MAGACIN { get; set; }

        public virtual PREDUZECE PREDUZECE { get; set; }
    }
}
