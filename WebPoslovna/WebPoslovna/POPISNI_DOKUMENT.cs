namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class POPISNI_DOKUMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public POPISNI_DOKUMENT()
        {
            POPISNA_KOMISIJA = new HashSet<POPISNA_KOMISIJA>();
            STAVKE_POPISA = new HashSet<STAVKE_POPISA>();
        }

        [Key]
        public int ID_POP__DOK__ { get; set; }

        public int ID_MAGACINA { get; set; }

        public int ID_GODINE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BROJ_POPISA { get; set; }

        [StringLength(11)]
        public string DATUM_POPISA { get; set; }

        public virtual MAGACIN MAGACIN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POPISNA_KOMISIJA> POPISNA_KOMISIJA { get; set; }

        public virtual POSLOVNA_GODINA POSLOVNA_GODINA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STAVKE_POPISA> STAVKE_POPISA { get; set; }
    }
}
