namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class POPISNA_KOMISIJA
    {
        [Key]
        public int ID__POP__KOMISIJE { get; set; }

        public int ID_POP__DOK__ { get; set; }

        public int ID_RADNIKA { get; set; }

        [Required]
        [StringLength(50)]
        public string FUNKCIJA_U_KOMISIJI { get; set; }

        public virtual POPISNI_DOKUMENT POPISNI_DOKUMENT { get; set; }

        public virtual RADNIK RADNIK { get; set; }
    }
}
