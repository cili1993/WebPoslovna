namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MAGACINSKA_KARTICA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MAGACINSKA_KARTICA()
        {
            ANALITIKA_MAGACINSKE_KARTICE = new HashSet<ANALITIKA_MAGACINSKE_KARTICE>();
        }

        [Key]
        public int ID_MAG__KART_ { get; set; }

        public int ID_GODINE { get; set; }

        public int ID_ARTIKAL { get; set; }

        public int ID_MAGACINA { get; set; }

        public decimal? PROSECNA_CENA { get; set; }

        public int? KOLICINA__ULAZA { get; set; }

        public decimal? VREDNOST_ULAZA { get; set; }

        public int? KOLICINA_IZLAZA { get; set; }

        public decimal? VREDNOST_IZLAZA { get; set; }

        public int POCETNO_STANJE_KOLICINA_MAG__KAR_ { get; set; }

        public decimal? POCETNO_STANJE_VREDNOST_MAG__KAR { get; set; }

        public int? UK__KOL__MAG__KAR_ { get; set; }

        public decimal? UK_VR__MAG__KAR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANALITIKA_MAGACINSKE_KARTICE> ANALITIKA_MAGACINSKE_KARTICE { get; set; }

        public virtual MAGACIN MAGACIN { get; set; }

        public virtual SIFRARNIK_ARTIKALA SIFRARNIK_ARTIKALA { get; set; }

        public virtual POSLOVNA_GODINA POSLOVNA_GODINA { get; set; }
    }
}
