namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DRZAVA")]
    public partial class DRZAVA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DRZAVA()
        {
            MESTO = new HashSet<MESTO>();
        }

        [Key]
        public int ID_DRZAVA { get; set; }

        [Required]
        [StringLength(3)]
        public string OZNAKA_DRZAVA { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_DRZAVA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MESTO> MESTO { get; set; }
    }
}
