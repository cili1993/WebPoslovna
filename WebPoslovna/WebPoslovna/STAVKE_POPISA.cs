namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class STAVKE_POPISA
    {
        [Key]
        public int ID_STAVKE_POPISA { get; set; }

        public int ID_PREDUZECA { get; set; }

        public int ID_SEKTORA { get; set; }

        public int ID_MAGACINA { get; set; }

        public int ID_GODINE { get; set; }

        public int ID_POP__DOK__ { get; set; }

        public int ID_ARTIKAL { get; set; }

        public double? KOLICINA_PO_POPISU { get; set; }

        public double? KOLICINA_U_KARTICI { get; set; }

        public virtual POPISNI_DOKUMENT POPISNI_DOKUMENT { get; set; }

        public virtual SIFRARNIK_ARTIKALA SIFRARNIK_ARTIKALA { get; set; }
    }
}
