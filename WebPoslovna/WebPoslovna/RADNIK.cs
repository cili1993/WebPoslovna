namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RADNIK")]
    public partial class RADNIK
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RADNIK()
        {
            POPISNA_KOMISIJA = new HashSet<POPISNA_KOMISIJA>();
        }

        [Key]
        public int ID_RADNIKA { get; set; }

        public int ID_PREDUZECA { get; set; }

        [Required]
        [StringLength(30)]
        public string IME_RADNIKA { get; set; }

        [Required]
        [StringLength(30)]
        public string PREZIME_RADNIKA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POPISNA_KOMISIJA> POPISNA_KOMISIJA { get; set; }

        public virtual PREDUZECE PREDUZECE { get; set; }
    }
}
