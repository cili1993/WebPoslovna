namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PROMETNI_DOKUMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROMETNI_DOKUMENT()
        {
            STAVKA_PROMETNOG_DOKUMENTA = new HashSet<STAVKA_PROMETNOG_DOKUMENTA>();
        }

        [Key]
        public int ID_PROM__DOK_ { get; set; }

        public int ID_GODINE { get; set; }

        public int ID_MAGACINA { get; set; }

        public int? MAG_ID_MAGACINA { get; set; }

        public int? ID_PARTNERA { get; set; }

        public int BROJ { get; set; }

        [Required]
        [StringLength(11)]
        public string DATUM_NASTANKA { get; set; }

        [StringLength(11)]
        public string DATUM_KNJIZENJA { get; set; }

        [Required]
        [StringLength(5)]
        public string STATUS { get; set; }

        [StringLength(5)]
        public string VRSTA_PROMETNOG_DOKUMENTA { get; set; }

        public virtual MAGACIN MAGACIN { get; set; }

        public virtual MAGACIN MAGACIN1 { get; set; }

        public virtual POSLOVNA_GODINA POSLOVNA_GODINA { get; set; }

        public virtual POSLOVNI_PARTNER POSLOVNI_PARTNER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STAVKA_PROMETNOG_DOKUMENTA> STAVKA_PROMETNOG_DOKUMENTA { get; set; }
    }
}
