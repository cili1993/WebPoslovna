namespace WebPoslovna
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SIFRARNIK_ARTIKALA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SIFRARNIK_ARTIKALA()
        {
            MAGACINSKA_KARTICA = new HashSet<MAGACINSKA_KARTICA>();
            STAVKA_PROMETNOG_DOKUMENTA = new HashSet<STAVKA_PROMETNOG_DOKUMENTA>();
            STAVKE_POPISA = new HashSet<STAVKE_POPISA>();
        }

        [Key]
        public int ID_ARTIKAL { get; set; }

        public int ID_GRUPE { get; set; }

        public int ID_JEDINICE { get; set; }

        [Required]
        [StringLength(30)]
        public string NAZIV_ARTIKAL { get; set; }

        public virtual GRUPA_ARTIKALA GRUPA_ARTIKALA { get; set; }

        public virtual JEDINICA_MERE JEDINICA_MERE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAGACINSKA_KARTICA> MAGACINSKA_KARTICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STAVKA_PROMETNOG_DOKUMENTA> STAVKA_PROMETNOG_DOKUMENTA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STAVKE_POPISA> STAVKE_POPISA { get; set; }
    }
}
