/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     6/29/2016 02:13:13                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ANALITIKA_MAGACINSKE_KARTICE') and o.name = 'FK_ANALITIK_MAGACINSK_MAGACINS')
alter table ANALITIKA_MAGACINSKE_KARTICE
   drop constraint FK_ANALITIK_MAGACINSK_MAGACINS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MAGACIN') and o.name = 'FK_MAGACIN_MAGACIN_P_SEKTOR')
alter table MAGACIN
   drop constraint FK_MAGACIN_MAGACIN_P_SEKTOR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MAGACINSKA_KARTICA') and o.name = 'FK_MAGACINS_MAGACINSK_SIFRARNI')
alter table MAGACINSKA_KARTICA
   drop constraint FK_MAGACINS_MAGACINSK_SIFRARNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MAGACINSKA_KARTICA') and o.name = 'FK_MAGACINS_MAG_KARTI_MAGACIN')
alter table MAGACINSKA_KARTICA
   drop constraint FK_MAGACINS_MAG_KARTI_MAGACIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MAGACINSKA_KARTICA') and o.name = 'FK_MAGACINS_POSLOVNA__POSLOVNA')
alter table MAGACINSKA_KARTICA
   drop constraint FK_MAGACINS_POSLOVNA__POSLOVNA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MESTO') and o.name = 'FK_MESTO_PRIPADA_D_DRZAVA')
alter table MESTO
   drop constraint FK_MESTO_PRIPADA_D_DRZAVA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POPISNA_KOMISIJA') and o.name = 'FK_POPISNA__KOMISIJA__POPISNI_')
alter table POPISNA_KOMISIJA
   drop constraint FK_POPISNA__KOMISIJA__POPISNI_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POPISNA_KOMISIJA') and o.name = 'FK_POPISNA__RADNIK_U__RADNIK')
alter table POPISNA_KOMISIJA
   drop constraint FK_POPISNA__RADNIK_U__RADNIK
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POPISNI_DOKUMENT') and o.name = 'FK_POPISNI__POPISNI_D_MAGACIN')
alter table POPISNI_DOKUMENT
   drop constraint FK_POPISNI__POPISNI_D_MAGACIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POPISNI_DOKUMENT') and o.name = 'FK_POPISNI__POSLOVNA__POSLOVNA')
alter table POPISNI_DOKUMENT
   drop constraint FK_POPISNI__POSLOVNA__POSLOVNA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POSLOVNA_GODINA') and o.name = 'FK_POSLOVNA_POSLOVNA__PREDUZEC')
alter table POSLOVNA_GODINA
   drop constraint FK_POSLOVNA_POSLOVNA__PREDUZEC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POSLOVNI_PARTNER') and o.name = 'FK_POSLOVNI_RELATIONS_MESTO')
alter table POSLOVNI_PARTNER
   drop constraint FK_POSLOVNI_RELATIONS_MESTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PREDUZECE') and o.name = 'FK_PREDUZEC_PREDUZECE_MESTO')
alter table PREDUZECE
   drop constraint FK_PREDUZEC_PREDUZECE_MESTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROMETNI_DOKUMENT') and o.name = 'FK_PROMETNI_PROMETNI__POSLOVNA')
alter table PROMETNI_DOKUMENT
   drop constraint FK_PROMETNI_PROMETNI__POSLOVNA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROMETNI_DOKUMENT') and o.name = 'FK_PROMETNI_PROMET_ZA_MAGACIN')
alter table PROMETNI_DOKUMENT
   drop constraint FK_PROMETNI_PROMET_ZA_MAGACIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROMETNI_DOKUMENT') and o.name = 'FK_PROMETNI_STRANKA_U_MAGACIN')
alter table PROMETNI_DOKUMENT
   drop constraint FK_PROMETNI_STRANKA_U_MAGACIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROMETNI_DOKUMENT') and o.name = 'FK_PROMETNI_STRANKA_U_POSLOVNI')
alter table PROMETNI_DOKUMENT
   drop constraint FK_PROMETNI_STRANKA_U_POSLOVNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('RADNIK') and o.name = 'FK_RADNIK_RADI_U_PREDUZEC')
alter table RADNIK
   drop constraint FK_RADNIK_RADI_U_PREDUZEC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SEKTOR') and o.name = 'FK_SEKTOR_SASTOJI_S_PREDUZEC')
alter table SEKTOR
   drop constraint FK_SEKTOR_SASTOJI_S_PREDUZEC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SIFRARNIK_ARTIKALA') and o.name = 'FK_SIFRARNI_ARTIKAL_U_JEDINICA')
alter table SIFRARNIK_ARTIKALA
   drop constraint FK_SIFRARNI_ARTIKAL_U_JEDINICA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SIFRARNIK_ARTIKALA') and o.name = 'FK_SIFRARNI_PRIAPDA_G_GRUPA_AR')
alter table SIFRARNIK_ARTIKALA
   drop constraint FK_SIFRARNI_PRIAPDA_G_GRUPA_AR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STAVKA_PROMETNOG_DOKUMENTA') and o.name = 'FK_STAVKA_P_ARTIKAL_P_SIFRARNI')
alter table STAVKA_PROMETNOG_DOKUMENTA
   drop constraint FK_STAVKA_P_ARTIKAL_P_SIFRARNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STAVKA_PROMETNOG_DOKUMENTA') and o.name = 'FK_STAVKA_P_SADRZI_ST_PROMETNI')
alter table STAVKA_PROMETNOG_DOKUMENTA
   drop constraint FK_STAVKA_P_SADRZI_ST_PROMETNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STAVKE_POPISA') and o.name = 'FK_STAVKE_P_POPIS_ZA__SIFRARNI')
alter table STAVKE_POPISA
   drop constraint FK_STAVKE_P_POPIS_ZA__SIFRARNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STAVKE_POPISA') and o.name = 'FK_STAVKE_P_SADRZI_ST_POPISNI_')
alter table STAVKE_POPISA
   drop constraint FK_STAVKE_P_SADRZI_ST_POPISNI_
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ANALITIKA_MAGACINSKE_KARTICE')
            and   name  = 'MAGACINSKA_KARTICA_IMA_ANALITIKU_FK'
            and   indid > 0
            and   indid < 255)
   drop index ANALITIKA_MAGACINSKE_KARTICE.MAGACINSKA_KARTICA_IMA_ANALITIKU_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ANALITIKA_MAGACINSKE_KARTICE')
            and   type = 'U')
   drop table ANALITIKA_MAGACINSKE_KARTICE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DRZAVA')
            and   type = 'U')
   drop table DRZAVA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GRUPA_ARTIKALA')
            and   type = 'U')
   drop table GRUPA_ARTIKALA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('JEDINICA_MERE')
            and   type = 'U')
   drop table JEDINICA_MERE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACIN')
            and   name  = 'MAGACIN_PRIPADA_SEKTORU_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACIN.MAGACIN_PRIPADA_SEKTORU_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MAGACIN')
            and   type = 'U')
   drop table MAGACIN
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'MAG_KARTICA_MAGACINA_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.MAG_KARTICA_MAGACINA_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'MAGACINSKA_KARTICA_ZA_ARTIKAL_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.MAGACINSKA_KARTICA_ZA_ARTIKAL_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'POSLOVNA_GODINA_MAGACINSKE_KARTICE_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.POSLOVNA_GODINA_MAGACINSKE_KARTICE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MAGACINSKA_KARTICA')
            and   type = 'U')
   drop table MAGACINSKA_KARTICA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MESTO')
            and   name  = 'PRIPADA_DRZAVI_FK'
            and   indid > 0
            and   indid < 255)
   drop index MESTO.PRIPADA_DRZAVI_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MESTO')
            and   type = 'U')
   drop table MESTO
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNA_KOMISIJA')
            and   name  = 'RADNIK_U_POPISNOJ_KOMISIJI_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNA_KOMISIJA.RADNIK_U_POPISNOJ_KOMISIJI_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNA_KOMISIJA')
            and   name  = 'KOMISIJA_ZADUZENA_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNA_KOMISIJA.KOMISIJA_ZADUZENA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POPISNA_KOMISIJA')
            and   type = 'U')
   drop table POPISNA_KOMISIJA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNI_DOKUMENT')
            and   name  = 'POSLOVNA_GOD_U_POP_DOK_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNI_DOKUMENT.POSLOVNA_GOD_U_POP_DOK_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNI_DOKUMENT')
            and   name  = 'POPISNI_DOKUMENT_MAGACINA_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNI_DOKUMENT.POPISNI_DOKUMENT_MAGACINA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POPISNI_DOKUMENT')
            and   type = 'U')
   drop table POPISNI_DOKUMENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POSLOVNA_GODINA')
            and   name  = 'POSLOVNA_GODINA_PREDUZECA_FK'
            and   indid > 0
            and   indid < 255)
   drop index POSLOVNA_GODINA.POSLOVNA_GODINA_PREDUZECA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POSLOVNA_GODINA')
            and   type = 'U')
   drop table POSLOVNA_GODINA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POSLOVNI_PARTNER')
            and   name  = 'RELATIONSHIP_4_FK'
            and   indid > 0
            and   indid < 255)
   drop index POSLOVNI_PARTNER.RELATIONSHIP_4_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POSLOVNI_PARTNER')
            and   type = 'U')
   drop table POSLOVNI_PARTNER
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PREDUZECE')
            and   name  = 'PREDUZECE_JE_IZ_FK'
            and   indid > 0
            and   indid < 255)
   drop index PREDUZECE.PREDUZECE_JE_IZ_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PREDUZECE')
            and   type = 'U')
   drop table PREDUZECE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'STRANKA_U_PROMETU_MAGACIN_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.STRANKA_U_PROMETU_MAGACIN_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'PROMET_ZA_MAGACIN_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.PROMET_ZA_MAGACIN_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'PROMETNI_DOKUMENT_PRIPADA_GODINI_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.PROMETNI_DOKUMENT_PRIPADA_GODINI_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'STRANKA_U_PROMETU_PARTNER_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.STRANKA_U_PROMETU_PARTNER_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PROMETNI_DOKUMENT')
            and   type = 'U')
   drop table PROMETNI_DOKUMENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('RADNIK')
            and   name  = 'RADI_U_FK'
            and   indid > 0
            and   indid < 255)
   drop index RADNIK.RADI_U_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RADNIK')
            and   type = 'U')
   drop table RADNIK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SEKTOR')
            and   name  = 'SASTOJI_SE_OD_SEKTORA_FK'
            and   indid > 0
            and   indid < 255)
   drop index SEKTOR.SASTOJI_SE_OD_SEKTORA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SEKTOR')
            and   type = 'U')
   drop table SEKTOR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SIFRARNIK_ARTIKALA')
            and   name  = 'ARTIKAL_U_JEDINICI_MERE_FK'
            and   indid > 0
            and   indid < 255)
   drop index SIFRARNIK_ARTIKALA.ARTIKAL_U_JEDINICI_MERE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SIFRARNIK_ARTIKALA')
            and   name  = 'PRIAPDA_GRUPI_ARTIKALA_FK'
            and   indid > 0
            and   indid < 255)
   drop index SIFRARNIK_ARTIKALA.PRIAPDA_GRUPI_ARTIKALA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SIFRARNIK_ARTIKALA')
            and   type = 'U')
   drop table SIFRARNIK_ARTIKALA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   name  = 'ARTIKAL_PROMETA_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKA_PROMETNOG_DOKUMENTA.ARTIKAL_PROMETA_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   name  = 'SADRZI_STAVKE_PROM_DOK_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKA_PROMETNOG_DOKUMENTA.SADRZI_STAVKE_PROM_DOK_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   type = 'U')
   drop table STAVKA_PROMETNOG_DOKUMENTA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKE_POPISA')
            and   name  = 'POPIS_ZA_ARTIKAL_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKE_POPISA.POPIS_ZA_ARTIKAL_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKE_POPISA')
            and   name  = 'SADRZI_STAVKE_POPISA_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKE_POPISA.SADRZI_STAVKE_POPISA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STAVKE_POPISA')
            and   type = 'U')
   drop table STAVKE_POPISA
go

/*==============================================================*/
/* Table: ANALITIKA_MAGACINSKE_KARTICE                          */
/*==============================================================*/
create table ANALITIKA_MAGACINSKE_KARTICE (
   ID_ANAL__MAG__KART_  int                  not null,
   ID_MAG__KART_        int                  not null,
   REDNI_BROJ_MAG__ANAL_ numeric              not null,
   DATUM_PROMENE        varchar(11)          not null,
   VRSTA_PROMETA        varchar(5)           null
      constraint CKC_VRSTA_PROMETA_ANALITIK check (VRSTA_PROMETA is null or (VRSTA_PROMETA in ('PR','OT','MM','NI','PS','KO'))),
   SIFRA_DOKUMENTA      numeric              null,
   KOLICINA_ANAL__MAG__K_ int                  null,
   CENA_ANAL__MAG__KART_ double precision     null,
   VREDNOST_ANAL__MAG__KART_ double precision     null,
   SMER                 varchar(3)           not null
      constraint CKC_SMER_ANALITIK check (SMER in ('U','I')),
   constraint PK_ANALITIKA_MAGACINSKE_KARTIC primary key (ID_ANAL__MAG__KART_)
)
go

/*==============================================================*/
/* Index: MAGACINSKA_KARTICA_IMA_ANALITIKU_FK                   */
/*==============================================================*/
create index MAGACINSKA_KARTICA_IMA_ANALITIKU_FK on ANALITIKA_MAGACINSKE_KARTICE (
ID_MAG__KART_ ASC
)
go

/*==============================================================*/
/* Table: DRZAVA                                                */
/*==============================================================*/
create table DRZAVA (
   ID_DRZAVA            int                  not null,
   OZNAKA_DRZAVA        varchar(3)           not null,
   NAZIV_DRZAVA         varchar(30)          not null,
   constraint PK_DRZAVA primary key (ID_DRZAVA)
)
go

/*==============================================================*/
/* Table: GRUPA_ARTIKALA                                        */
/*==============================================================*/
create table GRUPA_ARTIKALA (
   ID_GRUPE             int                  not null,
   NAZIV_GRUPE          varchar(30)          not null,
   constraint PK_GRUPA_ARTIKALA primary key (ID_GRUPE)
)
go

/*==============================================================*/
/* Table: JEDINICA_MERE                                         */
/*==============================================================*/
create table JEDINICA_MERE (
   ID_JEDINICE          int                  not null,
   NAZIV_JEDINICE       varchar(10)          null,
   constraint PK_JEDINICA_MERE primary key (ID_JEDINICE)
)
go

/*==============================================================*/
/* Table: MAGACIN                                               */
/*==============================================================*/
create table MAGACIN (
   ID_MAGACINA          int                  not null,
   ID_PREDUZECA         int                  not null,
   ID_SEKTORA           int                  not null,
   NAZIV_MAGACINA       varchar(30)          not null,
   constraint PK_MAGACIN primary key (ID_MAGACINA)
)
go

/*==============================================================*/
/* Index: MAGACIN_PRIPADA_SEKTORU_FK                            */
/*==============================================================*/
create index MAGACIN_PRIPADA_SEKTORU_FK on MAGACIN (
ID_SEKTORA ASC
)
go

/*==============================================================*/
/* Table: MAGACINSKA_KARTICA                                    */
/*==============================================================*/
create table MAGACINSKA_KARTICA (
   ID_MAG__KART_        int                  not null,
   ID_GODINE            int                  not null,
   ID_ARTIKAL           int                  not null,
   ID_MAGACINA          int                  not null,
   PROSECNA_CENA        decimal              null,
   KOLICINA__ULAZA      int                  null,
   VREDNOST_ULAZA       decimal              null,
   KOLICINA_IZLAZA      int                  null,
   VREDNOST_IZLAZA      decimal              null,
   POCETNO_STANJE_KOLICINA_MAG__KAR_ int                  not null,
   POCETNO_STANJE_VREDNOST_MAG__KAR decimal              null,
   UK__KOL__MAG__KAR_   int                  null,
   UK_VR__MAG__KAR      decimal              null,
   constraint PK_MAGACINSKA_KARTICA primary key (ID_MAG__KART_)
)
go

/*==============================================================*/
/* Index: POSLOVNA_GODINA_MAGACINSKE_KARTICE_FK                 */
/*==============================================================*/
create index POSLOVNA_GODINA_MAGACINSKE_KARTICE_FK on MAGACINSKA_KARTICA (
ID_GODINE ASC
)
go

/*==============================================================*/
/* Index: MAGACINSKA_KARTICA_ZA_ARTIKAL_FK                      */
/*==============================================================*/
create index MAGACINSKA_KARTICA_ZA_ARTIKAL_FK on MAGACINSKA_KARTICA (
ID_ARTIKAL ASC
)
go

/*==============================================================*/
/* Index: MAG_KARTICA_MAGACINA_FK                               */
/*==============================================================*/
create index MAG_KARTICA_MAGACINA_FK on MAGACINSKA_KARTICA (
ID_MAGACINA ASC
)
go

/*==============================================================*/
/* Table: MESTO                                                 */
/*==============================================================*/
create table MESTO (
   ID_MESTO             int                  not null,
   ID_DRZAVA            int                  not null,
   OZNAKA_MESTA         varchar(2)           not null,
   NAZIV_MESTA          varchar(30)          not null,
   POST_BR              numeric              null,
   constraint PK_MESTO primary key (ID_MESTO)
)
go

/*==============================================================*/
/* Index: PRIPADA_DRZAVI_FK                                     */
/*==============================================================*/
create index PRIPADA_DRZAVI_FK on MESTO (
ID_DRZAVA ASC
)
go

/*==============================================================*/
/* Table: POPISNA_KOMISIJA                                      */
/*==============================================================*/
create table POPISNA_KOMISIJA (
   ID__POP__KOMISIJE    int                  not null,
   ID_POP__DOK__        int                  not null,
   ID_RADNIKA           int                  not null,
   FUNKCIJA_U_KOMISIJI  varchar(50)          not null
      constraint CKC_FUNKCIJA_U_KOMISI_POPISNA_ check (FUNKCIJA_U_KOMISIJI in ('PR','CL')),
   constraint PK_POPISNA_KOMISIJA primary key (ID__POP__KOMISIJE)
)
go

/*==============================================================*/
/* Index: KOMISIJA_ZADUZENA_FK                                  */
/*==============================================================*/
create index KOMISIJA_ZADUZENA_FK on POPISNA_KOMISIJA (
ID_POP__DOK__ ASC
)
go

/*==============================================================*/
/* Index: RADNIK_U_POPISNOJ_KOMISIJI_FK                         */
/*==============================================================*/
create index RADNIK_U_POPISNOJ_KOMISIJI_FK on POPISNA_KOMISIJA (
ID_RADNIKA ASC
)
go

/*==============================================================*/
/* Table: POPISNI_DOKUMENT                                      */
/*==============================================================*/
create table POPISNI_DOKUMENT (
   ID_POP__DOK__        int                  not null,
   ID_MAGACINA          int                  not null,
   ID_GODINE            int                  not null,
   BROJ_POPISA          numeric              null,
   DATUM_POPISA         varchar(11)          null,
   constraint PK_POPISNI_DOKUMENT primary key (ID_POP__DOK__)
)
go

/*==============================================================*/
/* Index: POPISNI_DOKUMENT_MAGACINA_FK                          */
/*==============================================================*/
create index POPISNI_DOKUMENT_MAGACINA_FK on POPISNI_DOKUMENT (
ID_MAGACINA ASC
)
go

/*==============================================================*/
/* Index: POSLOVNA_GOD_U_POP_DOK_FK                             */
/*==============================================================*/
create index POSLOVNA_GOD_U_POP_DOK_FK on POPISNI_DOKUMENT (
ID_GODINE ASC
)
go

/*==============================================================*/
/* Table: POSLOVNA_GODINA                                       */
/*==============================================================*/
create table POSLOVNA_GODINA (
   ID_GODINE            int                  not null,
   ID_PREDUZECA         int                  not null,
   GODINA               varchar(4)           not null,
   ZAKLJUCENA_          bit                  null,
   constraint PK_POSLOVNA_GODINA primary key (ID_GODINE)
)
go

/*==============================================================*/
/* Index: POSLOVNA_GODINA_PREDUZECA_FK                          */
/*==============================================================*/
create index POSLOVNA_GODINA_PREDUZECA_FK on POSLOVNA_GODINA (
ID_PREDUZECA ASC
)
go

/*==============================================================*/
/* Table: POSLOVNI_PARTNER                                      */
/*==============================================================*/
create table POSLOVNI_PARTNER (
   ID_PARTNERA          int                  not null,
   ID_MESTO             int                  null,
   TIP_PARTNERA         varchar(15)          not null
      constraint CKC_TIP_PARTNERA_POSLOVNI check (TIP_PARTNERA in ('KU','DO','KD')),
   NAZIV_PARTNERA       varchar(50)          not null,
   PIB_PARTNERA         int                  not null,
   ADRESA_PARTNERA      varchar(50)          not null,
   TELEFON_PARTNERA     numeric              not null,
   constraint PK_POSLOVNI_PARTNER primary key (ID_PARTNERA)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_4_FK on POSLOVNI_PARTNER (
ID_MESTO ASC
)
go

/*==============================================================*/
/* Table: PREDUZECE                                             */
/*==============================================================*/
create table PREDUZECE (
   ID_PREDUZECA         int                  not null,
   ID_MESTO             int                  null,
   NAZIV_PREDUZECA      varchar(30)          not null,
   PIB_PREDUZECA        numeric              not null,
   ADRESA_PREDUZECA     varchar(30)          null,
   TELEFON_PREDUZECA    numeric              null,
   constraint PK_PREDUZECE primary key (ID_PREDUZECA)
)
go

/*==============================================================*/
/* Index: PREDUZECE_JE_IZ_FK                                    */
/*==============================================================*/
create index PREDUZECE_JE_IZ_FK on PREDUZECE (
ID_MESTO ASC
)
go

/*==============================================================*/
/* Table: PROMETNI_DOKUMENT                                     */
/*==============================================================*/
create table PROMETNI_DOKUMENT (
   ID_PROM__DOK_        int                  not null,
   ID_GODINE            int                  not null,
   ID_MAGACINA          int                  not null,
   MAG_ID_MAGACINA      int                  null,
   ID_PARTNERA          int                  null,
   BROJ                 int                  not null,
   DATUM_NASTANKA       varchar(11)          not null,
   DATUM_KNJIZENJA      varchar(11)          null,
   STATUS               varchar(5)           not null
      constraint CKC_STATUS_PROMETNI check (STATUS in ('FORM','PROK','STOR')),
   VRSTA_PROMETNOG_DOKUMENTA varchar(5)           null
      constraint CKC_VRSTA_PROMETNOG_D_PROMETNI check (VRSTA_PROMETNOG_DOKUMENTA is null or (VRSTA_PROMETNOG_DOKUMENTA in ('OT','PR','MM'))),
   constraint PK_PROMETNI_DOKUMENT primary key (ID_PROM__DOK_)
)
go

/*==============================================================*/
/* Index: STRANKA_U_PROMETU_PARTNER_FK                          */
/*==============================================================*/
create index STRANKA_U_PROMETU_PARTNER_FK on PROMETNI_DOKUMENT (
ID_PARTNERA ASC
)
go

/*==============================================================*/
/* Index: PROMETNI_DOKUMENT_PRIPADA_GODINI_FK                   */
/*==============================================================*/
create index PROMETNI_DOKUMENT_PRIPADA_GODINI_FK on PROMETNI_DOKUMENT (
ID_GODINE ASC
)
go

/*==============================================================*/
/* Index: PROMET_ZA_MAGACIN_FK                                  */
/*==============================================================*/
create index PROMET_ZA_MAGACIN_FK on PROMETNI_DOKUMENT (
ID_MAGACINA ASC
)
go

/*==============================================================*/
/* Index: STRANKA_U_PROMETU_MAGACIN_FK                          */
/*==============================================================*/
create index STRANKA_U_PROMETU_MAGACIN_FK on PROMETNI_DOKUMENT (
MAG_ID_MAGACINA ASC
)
go

/*==============================================================*/
/* Table: RADNIK                                                */
/*==============================================================*/
create table RADNIK (
   ID_RADNIKA           int                  not null,
   ID_PREDUZECA         int                  not null,
   IME_RADNIKA          varchar(30)          not null,
   PREZIME_RADNIKA      varchar(30)          not null,
   constraint PK_RADNIK primary key (ID_RADNIKA)
)
go

/*==============================================================*/
/* Index: RADI_U_FK                                             */
/*==============================================================*/
create index RADI_U_FK on RADNIK (
ID_PREDUZECA ASC
)
go

/*==============================================================*/
/* Table: SEKTOR                                                */
/*==============================================================*/
create table SEKTOR (
   ID_SEKTORA           int                  not null,
   ID_PREDUZECA         int                  not null,
   NAZIV_SEKTORA        varchar(30)          not null,
   constraint PK_SEKTOR primary key (ID_SEKTORA)
)
go

/*==============================================================*/
/* Index: SASTOJI_SE_OD_SEKTORA_FK                              */
/*==============================================================*/
create index SASTOJI_SE_OD_SEKTORA_FK on SEKTOR (
ID_PREDUZECA ASC
)
go

/*==============================================================*/
/* Table: SIFRARNIK_ARTIKALA                                    */
/*==============================================================*/
create table SIFRARNIK_ARTIKALA (
   ID_ARTIKAL           int                  not null,
   ID_GRUPE             int                  not null,
   ID_JEDINICE          int                  not null,
   NAZIV_ARTIKAL        varchar(30)          not null,
   constraint PK_SIFRARNIK_ARTIKALA primary key (ID_ARTIKAL)
)
go

/*==============================================================*/
/* Index: PRIAPDA_GRUPI_ARTIKALA_FK                             */
/*==============================================================*/
create index PRIAPDA_GRUPI_ARTIKALA_FK on SIFRARNIK_ARTIKALA (
ID_GRUPE ASC
)
go

/*==============================================================*/
/* Index: ARTIKAL_U_JEDINICI_MERE_FK                            */
/*==============================================================*/
create index ARTIKAL_U_JEDINICI_MERE_FK on SIFRARNIK_ARTIKALA (
ID_JEDINICE ASC
)
go

/*==============================================================*/
/* Table: STAVKA_PROMETNOG_DOKUMENTA                            */
/*==============================================================*/
create table STAVKA_PROMETNOG_DOKUMENTA (
   ID_STAV__PROM__DOK_  int                  not null,
   ID_PROM__DOK_        int                  not null,
   ID_ARTIKAL           int                  not null,
   REDNI_BROJ_STAV__PROM__DOK numeric              not null,
   KOLICINA_STAV__PROM__DOK int                  not null,
   CENA_STAVKA_PROM__DOK_ double precision     not null,
   VREDNOST_STAV__PROM__DOK double precision     null,
   constraint PK_STAVKA_PROMETNOG_DOKUMENTA primary key (ID_STAV__PROM__DOK_)
)
go

/*==============================================================*/
/* Index: SADRZI_STAVKE_PROM_DOK_FK                             */
/*==============================================================*/
create index SADRZI_STAVKE_PROM_DOK_FK on STAVKA_PROMETNOG_DOKUMENTA (
ID_PROM__DOK_ ASC
)
go

/*==============================================================*/
/* Index: ARTIKAL_PROMETA_FK                                    */
/*==============================================================*/
create index ARTIKAL_PROMETA_FK on STAVKA_PROMETNOG_DOKUMENTA (
ID_ARTIKAL ASC
)
go

/*==============================================================*/
/* Table: STAVKE_POPISA                                         */
/*==============================================================*/
create table STAVKE_POPISA (
   ID_STAVKE_POPISA     int                  not null,
   ID_PREDUZECA         int                  not null,
   ID_SEKTORA           int                  not null,
   ID_MAGACINA          int                  not null,
   ID_GODINE            int                  not null,
   ID_POP__DOK__        int                  not null,
   ID_ARTIKAL           int                  not null,
   KOLICINA_PO_POPISU   float                null,
   KOLICINA_U_KARTICI   float                null,
   constraint PK_STAVKE_POPISA primary key (ID_STAVKE_POPISA)
)
go

/*==============================================================*/
/* Index: SADRZI_STAVKE_POPISA_FK                               */
/*==============================================================*/
create index SADRZI_STAVKE_POPISA_FK on STAVKE_POPISA (
ID_POP__DOK__ ASC
)
go

/*==============================================================*/
/* Index: POPIS_ZA_ARTIKAL_FK                                   */
/*==============================================================*/
create index POPIS_ZA_ARTIKAL_FK on STAVKE_POPISA (
ID_ARTIKAL ASC
)
go

alter table ANALITIKA_MAGACINSKE_KARTICE
   add constraint FK_ANALITIK_MAGACINSK_MAGACINS foreign key (ID_MAG__KART_)
      references MAGACINSKA_KARTICA (ID_MAG__KART_)
go

alter table MAGACIN
   add constraint FK_MAGACIN_MAGACIN_P_SEKTOR foreign key (ID_SEKTORA)
      references SEKTOR (ID_SEKTORA)
go

alter table MAGACINSKA_KARTICA
   add constraint FK_MAGACINS_MAGACINSK_SIFRARNI foreign key (ID_ARTIKAL)
      references SIFRARNIK_ARTIKALA (ID_ARTIKAL)
go

alter table MAGACINSKA_KARTICA
   add constraint FK_MAGACINS_MAG_KARTI_MAGACIN foreign key (ID_MAGACINA)
      references MAGACIN (ID_MAGACINA)
go

alter table MAGACINSKA_KARTICA
   add constraint FK_MAGACINS_POSLOVNA__POSLOVNA foreign key (ID_GODINE)
      references POSLOVNA_GODINA (ID_GODINE)
go

alter table MESTO
   add constraint FK_MESTO_PRIPADA_D_DRZAVA foreign key (ID_DRZAVA)
      references DRZAVA (ID_DRZAVA)
go

alter table POPISNA_KOMISIJA
   add constraint FK_POPISNA__KOMISIJA__POPISNI_ foreign key (ID_POP__DOK__)
      references POPISNI_DOKUMENT (ID_POP__DOK__)
go

alter table POPISNA_KOMISIJA
   add constraint FK_POPISNA__RADNIK_U__RADNIK foreign key (ID_RADNIKA)
      references RADNIK (ID_RADNIKA)
go

alter table POPISNI_DOKUMENT
   add constraint FK_POPISNI__POPISNI_D_MAGACIN foreign key (ID_MAGACINA)
      references MAGACIN (ID_MAGACINA)
go

alter table POPISNI_DOKUMENT
   add constraint FK_POPISNI__POSLOVNA__POSLOVNA foreign key (ID_GODINE)
      references POSLOVNA_GODINA (ID_GODINE)
go

alter table POSLOVNA_GODINA
   add constraint FK_POSLOVNA_POSLOVNA__PREDUZEC foreign key (ID_PREDUZECA)
      references PREDUZECE (ID_PREDUZECA)
go

alter table POSLOVNI_PARTNER
   add constraint FK_POSLOVNI_RELATIONS_MESTO foreign key (ID_MESTO)
      references MESTO (ID_MESTO)
go

alter table PREDUZECE
   add constraint FK_PREDUZEC_PREDUZECE_MESTO foreign key (ID_MESTO)
      references MESTO (ID_MESTO)
go

alter table PROMETNI_DOKUMENT
   add constraint FK_PROMETNI_PROMETNI__POSLOVNA foreign key (ID_GODINE)
      references POSLOVNA_GODINA (ID_GODINE)
go

alter table PROMETNI_DOKUMENT
   add constraint FK_PROMETNI_PROMET_ZA_MAGACIN foreign key (ID_MAGACINA)
      references MAGACIN (ID_MAGACINA)
go

alter table PROMETNI_DOKUMENT
   add constraint FK_PROMETNI_STRANKA_U_MAGACIN foreign key (MAG_ID_MAGACINA)
      references MAGACIN (ID_MAGACINA)
go

alter table PROMETNI_DOKUMENT
   add constraint FK_PROMETNI_STRANKA_U_POSLOVNI foreign key (ID_PARTNERA)
      references POSLOVNI_PARTNER (ID_PARTNERA)
go

alter table RADNIK
   add constraint FK_RADNIK_RADI_U_PREDUZEC foreign key (ID_PREDUZECA)
      references PREDUZECE (ID_PREDUZECA)
go

alter table SEKTOR
   add constraint FK_SEKTOR_SASTOJI_S_PREDUZEC foreign key (ID_PREDUZECA)
      references PREDUZECE (ID_PREDUZECA)
go

alter table SIFRARNIK_ARTIKALA
   add constraint FK_SIFRARNI_ARTIKAL_U_JEDINICA foreign key (ID_JEDINICE)
      references JEDINICA_MERE (ID_JEDINICE)
go

alter table SIFRARNIK_ARTIKALA
   add constraint FK_SIFRARNI_PRIAPDA_G_GRUPA_AR foreign key (ID_GRUPE)
      references GRUPA_ARTIKALA (ID_GRUPE)
go

alter table STAVKA_PROMETNOG_DOKUMENTA
   add constraint FK_STAVKA_P_ARTIKAL_P_SIFRARNI foreign key (ID_ARTIKAL)
      references SIFRARNIK_ARTIKALA (ID_ARTIKAL)
go

alter table STAVKA_PROMETNOG_DOKUMENTA
   add constraint FK_STAVKA_P_SADRZI_ST_PROMETNI foreign key (ID_PROM__DOK_)
      references PROMETNI_DOKUMENT (ID_PROM__DOK_)
go

alter table STAVKE_POPISA
   add constraint FK_STAVKE_P_POPIS_ZA__SIFRARNI foreign key (ID_ARTIKAL)
      references SIFRARNIK_ARTIKALA (ID_ARTIKAL)
go

alter table STAVKE_POPISA
   add constraint FK_STAVKE_P_SADRZI_ST_POPISNI_ foreign key (ID_POP__DOK__)
      references POPISNI_DOKUMENT (ID_POP__DOK__)
go

